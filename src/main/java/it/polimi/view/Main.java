package it.polimi.view;

import it.polimi.view.rmi.RMIMain;

import it.polimi.view.socket.SocketMain;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Asks to choose the type of connection to the server and instance the right
 * view.
 *
 */
public class Main {

    static Scanner in = new Scanner(System.in);
    private static boolean imPlaying = true;
    private static RMIMain clientRMI;
    private static SocketMain clientSkt;
    private static Object viewToWaitSkt;
    private static Object viewToWaitRMI;
    private static String ipAdd;
    private static final int SOCKETPORT = 1337;
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    private Main() {
    }

    public static void main(String[] args) {

        try {
            makeChoice();
        } catch (InterruptedException err) {
            LOGGER.log(Level.WARNING, "Game Crash", err);
            return;
        }

    }

    private static void makeChoice() throws InterruptedException {

        while (imPlaying) {
            System.out
                    .println("\nS to connect with Socket, R to connect with RMI, E to quit");
            char connectionChoice = in.nextLine().charAt(0);
            connectionChoice = Character.toUpperCase(connectionChoice);

            if (connectionChoice == 'S') {
                startSkt();
                try {
                    viewToWaitSkt = clientSkt.getWriter();
                    synchronized (viewToWaitSkt) {
                        viewToWaitSkt.wait();
                    }
                } catch (InterruptedException err) {
                    throw new InterruptedException();
                }
                System.out.println("if you would like to play again: ");
            }
            if (connectionChoice == 'R') {
                startRMI();
                try {
                    viewToWaitRMI = clientRMI.getCommandReader();
                    synchronized (viewToWaitRMI) {
                        viewToWaitRMI.wait();
                    }
                } catch (InterruptedException err) {
                    throw new InterruptedException();
                }
                System.out.println("if you would like to play again: ");
            }
            if (connectionChoice == 'E')
                imPlaying = false;
        }
    }

    private static void startRMI() {
        clientRMI = new RMIMain();
        clientRMI.rmiStart();
    }

    private static void startSkt() {

        System.out.println("ip address to connect ?");
        ipAdd = in.nextLine();

        clientSkt = new SocketMain(ipAdd, SOCKETPORT);
        try {
            clientSkt.startClient();
        } catch (IOException err) {
            LOGGER.log(Level.FINE, "server has close connecton", err);
        }

    }
}
