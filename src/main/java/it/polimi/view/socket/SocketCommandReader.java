package it.polimi.view.socket;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Read commands and sends they to the controller
 *
 *
 */
public class SocketCommandReader implements Runnable {

    private PrintWriter toController;
    private static final Logger LOGGER = Logger
            .getLogger(SocketCommandReader.class.getName());
    private Socket socket;
    private Scanner sc = new Scanner(System.in);
    private String cmd = "";
    private boolean conUp = false;

    /**
     * Instantiates a new socket command reader.
     *
     * @param socket
     *            the socket
     */
    public SocketCommandReader(Socket socket) {
        this.socket = socket;
        try {
            toController = new PrintWriter(socket.getOutputStream());
            conUp = true;
        } catch (IOException serverDown) {
            LOGGER.log(Level.SEVERE, "cannot connect to server socket!!",
                    serverDown);
            close();
        }
    }

    @Override
    public void run() {
        while (conUp) {
            cmd = sc.nextLine();
            toController.println(cmd);
            toController.flush();
            if ("exit".equals(cmd))
                break;
        }
        close();
    }

    protected void close() {
        conUp = false;
        try {
            socket.close();
            toController.close();
        } catch (Exception closingErr) {
            LOGGER.log(Level.FINE, "cannot close socket!!", closingErr);
            socket = null;
            toController = null;
        }
    }
}
