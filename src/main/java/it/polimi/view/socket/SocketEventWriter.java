package it.polimi.view.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.observer.Observer;
import it.polimi.common.observer.event.Event;
import it.polimi.model.player.Player;

public class SocketEventWriter extends Thread implements Observer {

    protected ObjectInputStream eventReceiver;
    private final Object viewToWait = new Object();
    private boolean conUp = false;
    protected Socket socket;
    private static final String NOTRECEIVINGSIG = "not receiving signal from server";
    private static final Logger LOGGER = Logger
            .getLogger(SocketEventWriter.class.getName());

    /**
     * Instantiates a new socket event writer.
     *
     * @param socket
     *            the socket
     */
    public SocketEventWriter(Socket socket) {
        this.socket = socket;
        try {
            eventReceiver = new ObjectInputStream(socket.getInputStream());
            conUp = true;
        } catch (IOException err) {
            LOGGER.log(Level.INFO, "no server found", err);
            close();
        }
    }

    /**
     * This method listens the server and checks if the server's connection goes
     * down .
     */
    @Override
    public void run() {
        try {
            while (conUp) {
                receive();
                cpuRest();
            }
        } catch (NoSuchElementException err) {
            LOGGER.log(Level.FINE, "server has close connecton", err);
            close();
        }
    }

    private void cpuRest() {
        try {
            Thread.sleep(5);
        } catch (InterruptedException err) {
            LOGGER.log(Level.SEVERE, err.getMessage(), err);
            close();
        }
    }

    /**
     * This method listens the server and checks if the server's connection goes
     * down It try to receive an event object and return it, this gave you the
     * opportunity to handle the event received.
     */

    protected Event receive() {
        Event event = null;
        try {
            event = (Event) eventReceiver.readObject();
            if (event != null)
                notifyAll(event);

        } catch (IOException serverDown) {
            LOGGER.log(Level.FINE, NOTRECEIVINGSIG, serverDown);
            close();
        } catch (ClassNotFoundException serverDown) {
            LOGGER.log(Level.FINE, NOTRECEIVINGSIG, serverDown);
            close();
        } catch (NullPointerException serverDown) {
            LOGGER.log(Level.FINE, NOTRECEIVINGSIG, serverDown);
            close();
        } catch (ClassCastException serverDown) {
            LOGGER.log(Level.FINE, NOTRECEIVINGSIG, serverDown);
            close();
        }
        return event;
    }

    /**
     * Close.
     */
    protected synchronized void close() {
        conUp = false;
        try {
            socket.close();
            eventReceiver.close();
        } catch (Exception err) {
            LOGGER.log(Level.FINE, "cannot close socket!", err);
            eventReceiver = null;
            socket = null;
        } finally {
            newGame();
        }
    }

    private void newGame() {
        System.out.println("Server closed the connection,the game is over");
        synchronized (viewToWait) {
            viewToWait.notify();
        }
        this.interrupt();
    }

    @Override
    public void notifyAll(Event event) {
        System.out.println(event.getMsg());
    }

    @Override
    public void notifyPlayer(Event event, Player player) {
        System.out.println(event.getMsg());
    }

    /**
     * Gets the view to wait.
     *
     * @return the view to wait
     */
    public Object getViewToWait() {
        return viewToWait;
    }
}
