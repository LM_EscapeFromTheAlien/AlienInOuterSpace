package it.polimi.view.socket;

import java.io.IOException;
import java.net.Socket;

public class SocketMain {

    private String ip;
    private int port;
    private SocketEventWriter writer;
    private SocketCommandReader reader;

    /**
     * Instantiates a new socket main.
     * 
     * @param ip
     * @param port
     */
    public SocketMain(String ip, int port) {

        this.ip = ip;
        this.port = port;

    }

    /**
     * Start client.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void startClient() throws IOException {
        Socket socket = new Socket(ip, port);
        System.out.println("Connection established");
        writer = new SocketEventWriter(socket);
        reader = new SocketCommandReader(socket);
        Thread writerThr = new Thread(writer);
        Thread readerThr = new Thread(reader);
        writerThr.start();
        readerThr.start();
    }

    /**
     * Gets the writer.
     *
     * @return the writer
     */
    public Object getWriter() {
        return writer.getViewToWait();
    }
}
