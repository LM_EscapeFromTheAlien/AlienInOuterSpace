package it.polimi.view.gui;

import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

public class MyGuiTerminal extends JPanel implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JTextArea textArea = new JTextArea(15, 50);
    private JTextField textField = new JTextField(50);
    private transient MyTerminalOutputStream guiOutputStream;
    private final StringBuilder sb = new StringBuilder();
    private String cmd;
    private transient SocketGUICommandReader commandReader;

    public MyGuiTerminal(SocketGUICommandReader commandReader, String title) {

        sb.append("EFTAIOS" + "> ");

        guiOutputStream = new MyTerminalOutputStream(textArea, title);
        this.commandReader = commandReader;

        textField.addActionListener(this);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        DefaultCaret caret = (DefaultCaret) textArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        setLayout(new BorderLayout());
        add(new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
        add(textField, BorderLayout.AFTER_LAST_LINE);
        System.setOut(new PrintStream(guiOutputStream));
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        cmd = textField.getText();
        textField.setText("");

        commandReader.readTerminal(cmd);
    }

}