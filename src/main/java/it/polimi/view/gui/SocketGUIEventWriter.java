package it.polimi.view.gui;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.EventType;
import it.polimi.view.socket.SocketEventWriter;

public class SocketGUIEventWriter extends SocketEventWriter {

    private StartMenu startMenu;
    private String playerName;
    private static final String NOTRECEIVINGSIG = "not receiving signal from server";
    private static final Logger LOGGER = Logger
            .getLogger(SocketGUIEventWriter.class.getName());

    /**
     * Instantiates a new socket event writer.
     *
     * @param socket
     *            the socket
     * @param startMenu
     */
    public SocketGUIEventWriter(Socket socket, StartMenu startMenu) {
        super(socket);
        this.startMenu = startMenu;
    }

    @Override
    protected Event receive() {
        Event event = super.receive();
        if (event != null) {
            if (EventType.INITEVENT.equals(event.getWhoIam())
                    && event.getMap() != null)
                startNewGameGui(event);
            if (EventType.INITEVENT.equals(event.getWhoIam())
                    && event.getPlayerName() != null)
                playerName = event.getPlayerName();
        }
        return null;
    }

    private void startNewGameGui(Event event) {

        startMenu.closeGUI();

        InGameView gui;

        try {
            gui = new InGameView(super.socket, event.getMap(), playerName);
            gui.createAndShowGUI();
        } catch (IOException guiErr) {
            LOGGER.log(Level.FINE, NOTRECEIVINGSIG, guiErr);
            close();
        }

    }
}
