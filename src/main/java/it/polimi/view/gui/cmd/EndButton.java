package it.polimi.view.gui.cmd;

import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;

public class EndButton extends CommandButton {

    private static final String CMD = "end";
    private static ImageIcon cardIcon = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/SkipEnd.png");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public EndButton(SocketGUICommandReader commandReader) {
        super(commandReader, "END", CMD, cardIcon);

        Image img = cardIcon.getImage();
        Image newimg = img.getScaledInstance(120, 120,
                java.awt.Image.SCALE_SMOOTH);
        cardIcon = new ImageIcon(newimg);
        this.setIcon(cardIcon);
        this.setSize(120, 120);
        this.setBackground(Color.BLACK);
        this.setForeground(Color.BLACK);
    }

}