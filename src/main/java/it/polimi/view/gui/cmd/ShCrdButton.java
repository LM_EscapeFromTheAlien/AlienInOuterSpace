package it.polimi.view.gui.cmd;

import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;

public class ShCrdButton extends CommandButton {

    private static final String CMD = "show cards";
    private static ImageIcon cardIcon = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/ShowCards.png");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ShCrdButton(SocketGUICommandReader commandReader) {
        super(commandReader, "HAND", CMD, cardIcon);

        Image img = cardIcon.getImage();
        Image newimg = img.getScaledInstance(120, 120,
                java.awt.Image.SCALE_SMOOTH);
        cardIcon = new ImageIcon(newimg);
        this.setIcon(cardIcon);
        this.setBackground(Color.BLACK);
        this.setForeground(Color.BLACK);
        this.setSize(120, 120);
    }

}
