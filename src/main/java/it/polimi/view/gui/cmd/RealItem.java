package it.polimi.view.gui.cmd;

import java.awt.event.ActionEvent;

public class RealItem extends ItemButtonGraphics {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    ItemButtonGraphics realItemBtn;
    ItemDiscard discItemBtn;
    ItemUse useItemBtn;

    public RealItem(ItemButtonGraphics realItemBtn) {
        this.realItemBtn = realItemBtn;
    }

    public RealItem(int itemIndex, SocketGUICommandReader commandReader) {
        super(itemIndex);
        useItemBtn = new ItemUse(itemIndex, commandReader);
        discItemBtn = new ItemDiscard(itemIndex, commandReader);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        realItemBtn.actionPerformed(e);
    }

    public void setDiscard() {
        realItemBtn = discItemBtn;
    }

    public void setUse() {
        realItemBtn = useItemBtn;
    }

}
