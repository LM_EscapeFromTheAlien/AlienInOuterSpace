package it.polimi.view.gui.cmd;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public abstract class CommandButton extends JButton implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    protected String cmd = "";
    private transient SocketGUICommandReader commandReader;

    public CommandButton(SocketGUICommandReader commandReader, String textShow,
            String cmd, ImageIcon cardIcon) {
        super(textShow);

        this.cmd = cmd;
        this.commandReader = commandReader;

        Image img = cardIcon.getImage();
        Image newimg = img.getScaledInstance(120, 200,
                java.awt.Image.SCALE_SMOOTH);
        ImageIcon realCardIcon = new ImageIcon(newimg);

        this.setIcon(realCardIcon);
        this.setSize(120, 200);
        this.setBorderPainted(false);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setHorizontalTextPosition(JButton.CENTER);
        this.setVerticalTextPosition(JButton.CENTER);

        this.setBackground(Color.BLACK);
        Font f = new Font("Impact", Font.BOLD, 18);

        this.setFont(f);

        super.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        commandReader.read(cmd);
    }
}
