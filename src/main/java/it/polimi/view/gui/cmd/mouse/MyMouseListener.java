package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.HexGridGUI;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public abstract class MyMouseListener extends MouseAdapter {

    private int sideLength;
    private int xOffsetToSquare;
    private int radiusInscribingHex;
    private int hexHeight;

    public MyMouseListener(HexGridGUI drawGrid) {
        this.sideLength = drawGrid.getHexagonDrawer().getSideLength();
        this.xOffsetToSquare = drawGrid.getHexagonDrawer().getxOffsetToSquare();
        this.radiusInscribingHex = drawGrid.getHexagonDrawer()
                .getRadiusInscribingHex();
        this.hexHeight = drawGrid.getHexagonDrawer().getHexHeight();

    }

    public Point pxtoHex(int mouseX, int mouseY) {
        Point p = new Point(-1, -1);
        int realx = (mouseX - HexGridGUI.GRIDBORDER)
                / (sideLength + xOffsetToSquare);
        int realy = (mouseY - HexGridGUI.GRIDBORDER - (realx % 2)
                * radiusInscribingHex)
                / hexHeight;

        int dx = mouseX - realx * (sideLength + xOffsetToSquare);
        int dy = mouseY - realy * hexHeight;

        if (mouseY - (realx % 2) * radiusInscribingHex < 0)
            return p;

        if (realx % 2 == 0) {
            if (upOddNeighbor(dy, dx)) {
                realx--;
            }
            if (downOddNeighbor(dy, dx)) {
                realx--;
                realy--;
            }
        } else {
            if (upEvenNeighbor(dx, dy)) {
                realx--;
                realy++;
            }
            if (downEvenNeighbor(dx, dy)) {
                realx--;
            }
        }

        p.x = realx;
        p.y = realy;

        return p;
    }

    private boolean downEvenNeighbor(int dx, int dy) {
        return dy < hexHeight
                && (xOffsetToSquare - dx) * radiusInscribingHex
                        / xOffsetToSquare > dy - radiusInscribingHex;
    }

    private boolean upEvenNeighbor(int dx, int dy) {
        return dy > hexHeight
                && dx * radiusInscribingHex / xOffsetToSquare < dy - hexHeight;
    }

    private boolean downOddNeighbor(int dy, int dx) {
        return dy < radiusInscribingHex
                && (xOffsetToSquare - dx) * radiusInscribingHex
                        / xOffsetToSquare > dy;
    }

    private boolean upOddNeighbor(int dy, int dx) {
        return dy > radiusInscribingHex
                && dx * radiusInscribingHex / xOffsetToSquare < dy
                        - radiusInscribingHex;
    }

    public abstract void mouseClicked(MouseEvent e, Point p);

}
