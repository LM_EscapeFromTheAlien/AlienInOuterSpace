package it.polimi.view.gui.cmd;

import javax.swing.ImageIcon;

public class AttackButton extends CommandButton {

    private static final String CMD = "attack";
    private static final ImageIcon CARDICON = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/Attack.png");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public AttackButton(SocketGUICommandReader commandReader) {
        super(commandReader, "ATTACK", CMD, CARDICON);
    }

}
