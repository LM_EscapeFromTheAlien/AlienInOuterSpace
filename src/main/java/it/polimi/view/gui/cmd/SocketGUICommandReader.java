package it.polimi.view.gui.cmd;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Read commands and sends they to the controller
 *
 *
 */
public class SocketGUICommandReader {

    private PrintWriter toController;
    private static final Logger LOGGER = Logger
            .getLogger(SocketGUICommandReader.class.getName());
    private Socket socket;

    /**
     * Instantiates a new socket command reader.
     *
     * @param socket
     *            the socket
     */
    public SocketGUICommandReader(Socket socket) {
        this.socket = socket;
        try {
            toController = new PrintWriter(socket.getOutputStream());
        } catch (IOException serverDown) {
            LOGGER.log(Level.SEVERE, "cannot connect to server socket!!",
                    serverDown);
            close();
        }
    }

    public void read(String cmd) {
        System.out.println(cmd);
        toController.println(cmd);
        toController.flush();
        if ("exit".equals(cmd))
            close();
    }

    protected void close() {
        try {
            socket.close();
            toController.close();
        } catch (Exception closingErr) {
            LOGGER.log(Level.FINE, "cannot close socket!!", closingErr);
            socket = null;
            toController = null;
        }
    }

    public void readTerminal(String cmd) {
        toController.println(cmd);
        toController.flush();
        if ("exit".equals(cmd))
            close();
    }
}
