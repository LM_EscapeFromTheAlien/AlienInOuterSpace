package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.CommandButton;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

public class LightButton extends CommandButton {

    private static final String CMD = "point the mouse to light sector";
    private static final ImageIcon CARDICON = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/Light.png");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private transient RealMouseClicked myMouse;

    public LightButton(SocketGUICommandReader commandReader,
            RealMouseClicked myMouse) {
        super(commandReader, "LIGHT", CMD, CARDICON);
        this.myMouse = myMouse;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        myMouse.setLightMode();
        System.out.println(CMD);
    }
}