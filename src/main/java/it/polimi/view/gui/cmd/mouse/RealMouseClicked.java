package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.HexGridGUI;
import it.polimi.view.gui.cmd.SectType;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RealMouseClicked extends MouseAdapter {

    private MyMouseListener moveMouseClicked;
    private MyMouseListener noiseMouseClicked;
    private MyMouseListener lightMouseClicked;
    private MyMouseListener realMouse;
    private HexGridGUI drawGrid;
    private static final Logger LOGGER = Logger
            .getLogger(RealMouseClicked.class.getName());

    public RealMouseClicked(HexGridGUI drawGrid,
            SocketGUICommandReader commandReader) {
        this.drawGrid = drawGrid;
        moveMouseClicked = new MoveMouseClicked(drawGrid, commandReader);
        noiseMouseClicked = new NoiseMouseClicked(drawGrid, commandReader);
        lightMouseClicked = new LightMouseClicked(drawGrid, commandReader);
        realMouse = moveMouseClicked;

    }

    @Override
    public void mouseClicked(MouseEvent e) {

        Point p = new Point(realMouse.pxtoHex(e.getX(), e.getY()));

        try {
            drawGrid.createAndShowGrid();
        } catch (FileNotFoundException err) {
            LOGGER.log(Level.WARNING, "file not found", err);
        }

        if (p.x < 0 || p.y < 0 || p.x >= HexGridGUI.COL
                || p.y >= HexGridGUI.ROW)
            return;

        realMouse.mouseClicked(e, p);
        drawGrid.getSectTypeColor()[p.x][p.y] = SectType.POINTED;
        drawGrid.repaint();
    }

    public void setLightMode() {
        realMouse = lightMouseClicked;
    }

    public void setNoiseMode() {
        realMouse = noiseMouseClicked;
    }

    public void setMoveMode() {
        realMouse = moveMouseClicked;
    }
}
