package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.HexGridGUI;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.Point;
import java.awt.event.MouseEvent;

public class MoveMouseClicked extends MyMouseListener {
    private SocketGUICommandReader commandReader;

    public MoveMouseClicked(HexGridGUI drawGrid,
            SocketGUICommandReader commandReader) {
        super(drawGrid);
        this.commandReader = commandReader;
    }

    @Override
    public void mouseClicked(MouseEvent e, Point p) {

        String cmd;

        if ((p.y + 1) < 10)
            cmd = "move " + Character.toString((char) (p.x + 65)) + ";0"
                    + (p.y + 1);
        else
            cmd = "move " + Character.toString((char) (p.x + 65)) + ";"
                    + (p.y + 1);

        commandReader.read(cmd);
    }

}
