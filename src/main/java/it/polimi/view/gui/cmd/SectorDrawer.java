package it.polimi.view.gui.cmd;

import java.awt.*;
import java.io.IOException;

import javax.swing.JComponent;

public class SectorDrawer extends JComponent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected int gridBorder;

    private int sideLength = 0;
    private int xOffsetToSquare = 0;
    private int radiusInscribingHex = 0;
    private int hexHeight = 0;

    public void setBorders(int b) throws IOException {
        gridBorder = b;
    }

    public void setHeight(int height) {
        hexHeight = height;
        radiusInscribingHex = getHexHeight() / 2;

        sideLength = (int) (hexHeight / Math.sqrt(3));

        xOffsetToSquare = (int) (getRadiusInscribingHex() / Math.sqrt(3));
    }

    public Polygon hex(int x0, int y0) {

        int y = y0 + gridBorder;
        int x = x0 + gridBorder;

        int[] xVertexCoord, yVertexCoord;

        xVertexCoord = new int[] {
                x + getxOffsetToSquare(),
                x + getSideLength() + getxOffsetToSquare(),
                x + getSideLength() + getxOffsetToSquare()
                        + getxOffsetToSquare(),
                x + getSideLength() + getxOffsetToSquare(),
                x + getxOffsetToSquare(), x };
        yVertexCoord = new int[] { y, y, y + getRadiusInscribingHex(),
                y + getRadiusInscribingHex() + getRadiusInscribingHex(),
                y + getRadiusInscribingHex() + getRadiusInscribingHex(),
                y + getRadiusInscribingHex() };

        return new Polygon(xVertexCoord, yVertexCoord, 6);

    }

    public void fillHex(int col, int row, SectType gridColorPerSector,
            Graphics2D g2) {
        int x = col * (getSideLength() + getxOffsetToSquare());
        int y = row * getHexHeight() + (col % 2) * getHexHeight() / 2;
        String colToWrite = Character.toString((char) (col + 65));

        float alpha = 1f;
        AlphaComposite alcom = AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER, alpha);
        g2.setComposite(alcom);

        if (gridColorPerSector.equals(SectType.ALIEN_SECTOR)) {
            g2.setColor(HexGridGUI.COLOURALHUM);
            g2.fillPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURCELL);
            g2.drawPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURTXT);
            g2.drawString("AS", x + gridBorder + getRadiusInscribingHex() - 8,
                    y + gridBorder + getRadiusInscribingHex() + 4);

        }
        if (gridColorPerSector.equals(SectType.DANGEROUS)) {
            g2.setColor(HexGridGUI.COLOURDANGEROUS);
            g2.fillPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURCELL);
            g2.drawPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURTXT);
            g2.drawString(colToWrite + "" + (row + 1), x + gridBorder
                    + getRadiusInscribingHex() - 8, y + gridBorder
                    + getRadiusInscribingHex() + 4);

        }
        if (gridColorPerSector.equals(SectType.SECURE)) {
            g2.setColor(HexGridGUI.COLOURSAFE);
            g2.fillPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURCELL);
            g2.drawPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURTXT);
            g2.drawString(colToWrite + "" + (row + 1), x + gridBorder
                    + getRadiusInscribingHex() - 8, y + gridBorder
                    + getRadiusInscribingHex() + 4);

        }
        if (gridColorPerSector.equals(SectType.HUMAN_SECTOR)) {
            g2.setColor(HexGridGUI.COLOURALHUM);
            g2.fillPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURCELL);
            g2.drawPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURTXT);
            g2.drawString("HS", x + gridBorder + getRadiusInscribingHex() - 8,
                    y + gridBorder + getRadiusInscribingHex() + 4);
        }
        if (gridColorPerSector.equals(SectType.ESCAPE)) {
            g2.setColor(HexGridGUI.COLOURALHUM);
            g2.fillPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURCELL);
            g2.drawPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURTXT);
            g2.drawString("HTC",
                    x + gridBorder + getRadiusInscribingHex() - 12, y
                            + gridBorder + getRadiusInscribingHex() + 4);
        }

        if (gridColorPerSector.equals(SectType.POINTED)) {
            alpha = 0.1f;
            alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2.setComposite(alcom);
            g2.setColor(HexGridGUI.COLOURPOSSIBLE);
            g2.fillPolygon(hex(x, y));
            g2.setColor(HexGridGUI.COLOURCELL);
            g2.drawPolygon(hex(x, y));
            g2.drawString("P", x + gridBorder + getRadiusInscribingHex() - 3, y
                    + gridBorder + getRadiusInscribingHex() + 4);
        }

    }

    public int getSideLength() {
        return sideLength;
    }

    public int getxOffsetToSquare() {
        return xOffsetToSquare;
    }

    public int getRadiusInscribingHex() {
        return radiusInscribingHex;
    }

    public int getHexHeight() {
        return hexHeight;
    }

}