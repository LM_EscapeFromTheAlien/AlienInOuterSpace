package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.HexGridGUI;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.Point;
import java.awt.event.MouseEvent;

public class NoiseMouseClicked extends MyMouseListener {

    private SocketGUICommandReader commandReader;

    public NoiseMouseClicked(HexGridGUI drawGrid,
            SocketGUICommandReader commandReader) {
        super(drawGrid);
        this.commandReader = commandReader;
    }

    @Override
    public void mouseClicked(MouseEvent e, Point p) {

        String noisePos;

        if ((p.y + 1) < 10)
            noisePos = "noise in " + Character.toString((char) (p.x + 65))
                    + ";0" + (p.y + 1);
        else
            noisePos = "noise in " + Character.toString((char) (p.x + 65))
                    + ";" + (p.y + 1);
        commandReader.read(noisePos);

    }

}
