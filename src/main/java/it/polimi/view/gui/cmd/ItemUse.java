package it.polimi.view.gui.cmd;

import java.awt.event.ActionEvent;

public class ItemUse extends ItemButtonGraphics {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private transient SocketGUICommandReader commandReader;
    private int itemChoosed;

    public ItemUse(int itemChoosed) {
        this.itemChoosed = itemChoosed;
    }

    public ItemUse(int itemIndex, SocketGUICommandReader commandReader) {
        this.commandReader = commandReader;
        this.itemChoosed = itemIndex;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String cmd;

        cmd = "use item " + itemChoosed;
        commandReader.read(cmd);
    }
}
