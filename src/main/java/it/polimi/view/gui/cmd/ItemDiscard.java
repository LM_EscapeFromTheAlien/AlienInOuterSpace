package it.polimi.view.gui.cmd;

import java.awt.event.ActionEvent;

public class ItemDiscard extends ItemButtonGraphics {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int itemChoosed;
    private transient SocketGUICommandReader commandReader;

    public ItemDiscard(int itemChoosed) {
        super(itemChoosed);
        this.itemChoosed = itemChoosed;
    }

    public ItemDiscard(int itemIndex, SocketGUICommandReader commandReader) {
        this.commandReader = commandReader;
        this.itemChoosed = itemIndex;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String cmd;

        cmd = "discard " + itemChoosed;
        commandReader.read(cmd);
    }
}
