package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.CommandButton;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

public class MoveButton extends CommandButton {

    private static final String CMD = "point the mouse to move in sector";
    private static final ImageIcon CARDICON = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/Move.png");

    /**
         * 
         */
    private static final long serialVersionUID = 1L;
    private transient RealMouseClicked myMouse;

    public MoveButton(SocketGUICommandReader commandReader,
            RealMouseClicked myMouse) {
        super(commandReader, "MOVE", CMD, CARDICON);
        this.myMouse = myMouse;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        myMouse.setMoveMode();
        System.out.println(CMD);
    }

}
