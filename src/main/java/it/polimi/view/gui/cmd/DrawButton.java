package it.polimi.view.gui.cmd;

import java.awt.Image;

import javax.swing.ImageIcon;

public class DrawButton extends CommandButton {

    private static final String CMD = "draw";
    private static final ImageIcon CARDICON = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/Draw.png");

    /**
         * 
         */
    private static final long serialVersionUID = 1L;

    public DrawButton(SocketGUICommandReader commandReader) {
        super(commandReader, "DRAW", CMD, CARDICON);

        Image img = CARDICON.getImage();
        Image newimg = img.getScaledInstance(96, 200,
                java.awt.Image.SCALE_SMOOTH);
        ImageIcon realCardIcon = new ImageIcon(newimg);

        this.setIcon(realCardIcon);
        this.setSize(96, 200);
    }

}
