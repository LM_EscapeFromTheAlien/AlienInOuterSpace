package it.polimi.view.gui.cmd;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class DiscardButton extends JButton implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private RealItem item1;
    private RealItem item2;
    private RealItem item3;
    private RealItem item4;

    public DiscardButton(RealItem item1, RealItem item2, RealItem item3,
            RealItem item4) {

        super("DISCARD ITEM");

        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;

        this.setBackground(Color.BLACK);
        this.setForeground(Color.CYAN);

        this.setPreferredSize(new Dimension(190, 30));

        this.setBorderPainted(false);
        this.setHorizontalTextPosition(JButton.CENTER);
        this.setVerticalTextPosition(JButton.CENTER);

        super.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        item1.setDiscard();
        item2.setDiscard();
        item3.setDiscard();
        item4.setDiscard();

        System.out.println("chose item to discard");
    }
}
