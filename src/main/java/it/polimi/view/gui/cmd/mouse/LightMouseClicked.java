package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.HexGridGUI;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.Point;
import java.awt.event.MouseEvent;

public class LightMouseClicked extends MyMouseListener {

    private SocketGUICommandReader commandReader;

    public LightMouseClicked(HexGridGUI drawGrid,
            SocketGUICommandReader commandReader) {
        super(drawGrid);
        this.commandReader = commandReader;
    }

    @Override
    public void mouseClicked(MouseEvent e, Point p) {
        String lightPos;

        if ((p.y + 1) < 10)
            lightPos = "light in " + Character.toString((char) (p.x + 65))
                    + ";0" + (p.y + 1);
        else
            lightPos = "light in " + Character.toString((char) (p.x + 65))
                    + ";" + (p.y + 1);

        commandReader.read(lightPos);

    }
}
