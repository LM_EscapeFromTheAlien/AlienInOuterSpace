package it.polimi.view.gui.cmd;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.util.Scanner;

import javax.swing.*;

public class HexGridGUI extends JPanel {

    public static final Color COLOURCELL = Color.BLACK;
    public static final Color COLOURTXT = Color.cyan;
    public static final Color COLOURDANGEROUS = Color.DARK_GRAY;
    public static final Color COLOURSAFE = Color.GRAY;
    public static final Color COLOURHATCH = Color.BLACK;
    public static final Color COLOURALHUM = Color.BLACK;
    public static final Color COLOURPOSSIBLE = Color.GREEN;
    public static final int COL = 23;
    public static final int ROW = 14;
    public static final int GRIDBORDER = 10;
    private static final int HEXAGONSIZEINPX = 39;
    private static final int SCREEENSIZE = HEXAGONSIZEINPX * (COL + 1) + GRIDBORDER * 3;
    private final SectType[][] sectTypeColor = new SectType[COL][ROW];
    private transient Scanner readMap;
    private SectorDrawer hexagonDrawer;
    private String mapChosed;

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public HexGridGUI(String mapChosed) throws IOException {

        this.mapChosed = mapChosed;
        hexagonDrawer = new SectorDrawer();
        initGame();
        createAndShowGrid();
    }

    private void initGame() throws IOException {

        getHexagonDrawer().setHeight(HEXAGONSIZEINPX);
        getHexagonDrawer().setBorders(GRIDBORDER);

        for (int i = 0; i < COL; i++) {
            for (int j = 0; j < ROW; j++) {
                sectTypeColor[i][j] = SectType.EMPTY;
            }
        }
    }

    private Scanner mapFileOpener() throws FileNotFoundException {

        try {
            File map = new File("src/main/resources/it/polimi/file/map/"
                    + mapChosed + ".txt");
            readMap = new Scanner(new File(map.getPath()));
        } catch (InvalidPathException er) {
            throw er;
        } catch (FileNotFoundException er) {
            throw er;
        }
        return readMap;
    }

    public void createAndShowGrid() throws FileNotFoundException {

        readMap = mapFileOpener();

        String type = new String();
        int colIndex;
        int rowIndex;
        while (readMap.hasNext()) {
            colIndex = readMap.next().charAt(0) - 65;
            rowIndex = readMap.nextInt() - 1;
            type = readMap.nextLine();
            type = type.trim();

            sectTypeColor[colIndex][rowIndex] = SectType.valueOf(type);
        }

        this.setSize((int) (SCREEENSIZE / 1.25), (int) (SCREEENSIZE / 1.65));
        this.setVisible(true);
        readMap.close();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.setFont(new Font("TimesRoman", Font.BOLD, 12));

        super.paintComponent(g2);
        for (int colIndex = 0; colIndex < COL; colIndex++) {
            for (int rowIndex = 0; rowIndex < ROW; rowIndex++) {
                getHexagonDrawer().fillHex(colIndex, rowIndex,
                        sectTypeColor[colIndex][rowIndex], g2);
            }
        }

    }

    public SectorDrawer getHexagonDrawer() {
        return hexagonDrawer;
    }

    public SectType[][] getSectTypeColor() {
        return sectTypeColor;
    }
}