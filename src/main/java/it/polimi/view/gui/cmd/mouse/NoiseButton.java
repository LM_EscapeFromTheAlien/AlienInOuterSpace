package it.polimi.view.gui.cmd.mouse;

import it.polimi.view.gui.cmd.CommandButton;
import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

public class NoiseButton extends CommandButton {

    private static final String CMD = "point the mouse to make noise in that sector";
    private static final ImageIcon CARDICON = new ImageIcon(
            "src/main/resources/it/polimi/file/EFTAIOStexture/Noise.png");

    /**
         * 
         */
    private static final long serialVersionUID = 1L;
    private transient RealMouseClicked myMouse;

    public NoiseButton(SocketGUICommandReader commandReader,
            RealMouseClicked myMouse) {
        super(commandReader, "NOISE", CMD, CARDICON);
        this.myMouse = myMouse;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        myMouse.setNoiseMode();
        System.out.println(CMD);
    }
}
