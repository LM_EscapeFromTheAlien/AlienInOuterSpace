package it.polimi.view.gui.cmd;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public abstract class ItemButtonGraphics extends JButton implements
        ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ItemButtonGraphics() {

    }

    public ItemButtonGraphics(int itemChoosed) {

        super(String.valueOf(itemChoosed));

        ImageIcon cardIcon = new ImageIcon(
                "src/main/resources/it/polimi/file/EFTAIOStexture/Item.png");
        Image img = cardIcon.getImage();
        Image newimg = img.getScaledInstance(96, 165,
                java.awt.Image.SCALE_SMOOTH);

        cardIcon = new ImageIcon(newimg);

        this.setIcon(cardIcon);
        this.setSize(96, 165);
        this.setBorderPainted(false);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setHorizontalTextPosition(JButton.CENTER);
        this.setVerticalTextPosition(JButton.CENTER);

        this.setBackground(Color.BLACK);
        Font f = new Font("Impact", Font.BOLD, 18);

        this.setFont(f);

        super.addActionListener(this);
    }

}