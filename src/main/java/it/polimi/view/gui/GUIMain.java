package it.polimi.view.gui;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

public class GUIMain {

    static Scanner in = new Scanner(System.in);
    private static String ipAdd;
    private static final int SOCKETPORT = 1337;
    private static final Logger LOGGER = Logger.getLogger(GUIMain.class
            .getName());

    private GUIMain() {

    }

    public static void main(String[] args) {
        startSkt();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                StartMenu startM = new StartMenu(ipAdd, SOCKETPORT);
                startM.createAndShowMenuGUI();
                try {
                    startM.startClient();
                } catch (IOException err) {
                    LOGGER.log(Level.WARNING, "Game Crash", err);
                }
            }
        });
    }

    private static void startSkt() {

        System.out.println("Ip address to connect ?");
        ipAdd = in.nextLine();

    }
}