package it.polimi.view.gui;

import it.polimi.view.gui.cmd.AttackButton;
import it.polimi.view.gui.cmd.DiscardButton;
import it.polimi.view.gui.cmd.DrawButton;
import it.polimi.view.gui.cmd.EndButton;
import it.polimi.view.gui.cmd.HexGridGUI;
import it.polimi.view.gui.cmd.RealItem;
import it.polimi.view.gui.cmd.ShCrdButton;
import it.polimi.view.gui.cmd.SkipButton;
import it.polimi.view.gui.cmd.SocketGUICommandReader;
import it.polimi.view.gui.cmd.UseButton;
import it.polimi.view.gui.cmd.mouse.LightButton;
import it.polimi.view.gui.cmd.mouse.MoveButton;
import it.polimi.view.gui.cmd.mouse.NoiseButton;
import it.polimi.view.gui.cmd.mouse.RealMouseClicked;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.io.IOException;
import java.net.Socket;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class InGameView {

    private final HexGridGUI myGrid;
    private RealMouseClicked myMouse;
    private SocketGUICommandReader commandReader;
    private String playerName;

    public InGameView(Socket socket, String map, String playerName)
            throws IOException {

        this.playerName = playerName;
        commandReader = new SocketGUICommandReader(socket);
        myGrid = new HexGridGUI(map);
        myMouse = new RealMouseClicked(myGrid, commandReader);
        myGrid.addMouseListener(myMouse);

    }

    public void createAndShowGUI() {

        JFrame main = new JFrame("escape from alien");

        JPanel command = setCommandPanel();
        JPanel leftPanel = setLeftPanel();

        main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = main.getContentPane();
        content.setLayout(new BorderLayout());
        content.add(leftPanel);
        content.add(command, BorderLayout.LINE_END);

        main.setExtendedState(JFrame.MAXIMIZED_BOTH);
        command.setSize(main.getWidth() / 4, main.getHeight());
        main.setVisible(true);

    }

    private JPanel setLeftPanel() {

        JPanel leftPanel = new JPanel();
        JPanel secondRow = new JPanel();
        leftPanel.setLayout(new BorderLayout());

        JButton skip = new SkipButton(commandReader);
        JButton end = new EndButton(commandReader);
        JButton show = new ShCrdButton(commandReader);
        secondRow.add(skip);
        secondRow.add(end);
        secondRow.add(show);

        leftPanel.add(myGrid, BorderLayout.CENTER);
        leftPanel.add(secondRow, BorderLayout.SOUTH);

        return leftPanel;
    }

    private JPanel setCommandPanel() {

        JPanel command = new JPanel();

        BoxLayout boxLayout = new BoxLayout(command, BoxLayout.Y_AXIS);

        command.setLayout(boxLayout);

        JPanel firstRow = new JPanel();
        JPanel secondRow = new JPanel();
        secondRow.setLayout(new FlowLayout());
        JPanel thirdRow = new JPanel();

        firstRow.add(new MyGuiTerminal(commandReader, playerName));

        JButton draw = new DrawButton(commandReader);
        secondRow.add(draw);
        secondRow.add(setItemPanel());

        JButton move = new MoveButton(commandReader, myMouse);
        JButton attack = new AttackButton(commandReader);
        JButton light = new LightButton(commandReader, myMouse);
        JButton noise = new NoiseButton(commandReader, myMouse);
        thirdRow.add(move);
        thirdRow.add(attack);
        thirdRow.add(noise);
        thirdRow.add(light);

        command.add(firstRow);
        command.add(secondRow);
        command.add(thirdRow);

        return command;
    }

    private Component setItemPanel() {

        JPanel itemPanel = new JPanel();
        itemPanel.setLayout(new BorderLayout());
        JPanel lineOne = new JPanel();
        JPanel lineTwo = new JPanel();

        RealItem item1 = new RealItem(1, commandReader);
        RealItem item2 = new RealItem(2, commandReader);
        RealItem item3 = new RealItem(3, commandReader);
        RealItem item4 = new RealItem(4, commandReader);
        JButton discard = new DiscardButton(item1, item2, item3, item4);
        JButton use = new UseButton(item1, item2, item3, item4);

        lineOne.add(item1);
        lineOne.add(item2);
        lineOne.add(item3);
        lineOne.add(item4);

        lineTwo.add(use);
        lineTwo.add(discard);

        itemPanel.add(lineOne, BorderLayout.NORTH);
        itemPanel.add(lineTwo, BorderLayout.SOUTH);

        return itemPanel;
    }
}