package it.polimi.view.gui;

import it.polimi.view.gui.cmd.SocketGUICommandReader;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

public class StartMenu {

    private SocketGUIEventWriter writer;
    private Socket socket;
    private JFrame main;
    private SocketGUICommandReader commandReader;
    private String gameName = "EFTAIOS";
    private static final Logger LOGGER = Logger.getLogger(StartMenu.class
            .getName());

    public StartMenu(String ip, int port) {

        try {
            socket = new Socket(ip, port);
        } catch (IOException err) {
            LOGGER.log(Level.WARNING, "Game Crash", err);
        }
        commandReader = new SocketGUICommandReader(socket);
    }

    public void createAndShowMenuGUI() {

        main = new JFrame("escape from alien");

        main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = main.getContentPane();
        content.setLayout(new BorderLayout());

        content.add(new MyGuiTerminal(commandReader, gameName));

        main.setPreferredSize(new Dimension(720, 540));
        main.pack();
        main.setLocationRelativeTo(null);
        main.setVisible(true);

    }

    public void startClient() throws IOException {

        System.out.println("Connection established");
        writer = new SocketGUIEventWriter(socket, this);
        Thread writerThr = new Thread(writer);
        writerThr.start();

    }

    public Object getWriter() {
        return writer.getViewToWait();
    }

    public void closeGUI() {
        main.setVisible(false);
        main.dispose();

        writer = null;
        socket = null;
        main = null;
        commandReader = null;
    }

}
