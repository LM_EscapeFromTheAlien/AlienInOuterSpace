package it.polimi.view.rmi;

import it.polimi.common.rmi.RMIReaderInterface;

import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RMICommandReader extends Thread {

    private Scanner in = new Scanner(System.in);
    private final Object viewToWait = new Object();
    private RMIReaderInterface reader;
    private String myName;
    private boolean conUp = true;
    private static final Logger LOGGER = Logger
            .getLogger(RMICommandReader.class.getName());

    /**
     * Instantiates a new RMI command reader.
     *
     * @param reader
     * @param myName
     */
    public RMICommandReader(RMIReaderInterface reader, String myName) {
        this.reader = reader;
        this.myName = myName;

    }

    @Override
    public void run() {
        String cmd = " ";
        while (conUp) {
            cmd = in.nextLine();
            try {
                reader.readCmd(cmd, myName);
            } catch (RemoteException e) {
                LOGGER.log(Level.FINE, e.getMessage(), e);
                System.out
                        .println("Server have close the connection, Game End");
                conUp = false;
            } finally {
                if ("exit".equals(cmd))
                    conUp = false;
            }
        }
        newGame();
    }

    private void newGame() {
        synchronized (viewToWait) {
            viewToWait.notify();
        }
        this.interrupt();
    }

    /**
     * Gets the view to wait.
     *
     * @return the view to wait
     */
    public Object getViewToWait() {
        return viewToWait;
    }
}
