package it.polimi.view.rmi;

import it.polimi.common.observer.Observer;
import it.polimi.common.observer.event.Event;
import it.polimi.common.rmi.EventWriterInterface;
import it.polimi.model.player.Player;

public class RMIEventWriter extends Thread implements Observer,
        EventWriterInterface {

    private String myName;

    /**
     * Instantiates a new RMI event writer.
     * 
     * @param myName
     */
    public RMIEventWriter(String myName) {
        this.myName = myName;
    }

    @Override
    public void notifyAll(Event event) {
        System.out.println(event.getMsg());
    }

    @Override
    public void notifyPlayer(Event event, Player player) {
        System.out.println(event.getMsg());
    }

    @Override
    public void dispatchMessage(String msg) {
        System.out.println(msg);
    }

    @Override
    public String getPlayerName() {
        return myName;
    }

}
