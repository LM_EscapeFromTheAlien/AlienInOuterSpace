package it.polimi.view.rmi;

import it.polimi.common.rmi.EventWriterInterface;
import it.polimi.common.rmi.RMIConnHandlerInterface;
import it.polimi.common.rmi.RMIReaderInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to instantiate the event writer and the command reader
 *
 */
public class RMIMain {

    private final Scanner in;
    private static final Logger LOGGER = Logger.getLogger(RMIMain.class
            .getName());
    private RMICommandReader commandReader;
    private RMIEventWriter eventWriter;

    /**
     * Instantiates a new RMI main.
     */
    public RMIMain() {
        in = new Scanner(System.in);
    }

    /**
     * Rmi start.
     */
    public void rmiStart() {

        System.out.println("\n Type your name: ");
        final String myName = in.nextLine();

        try {

            Registry registry = LocateRegistry.getRegistry(7777);
            /**
             * take the reference to server interface for making a connection.
             */
            RMIConnHandlerInterface server = (RMIConnHandlerInterface) registry
                    .lookup("Server");
            RMIReaderInterface reader = (RMIReaderInterface) registry
                    .lookup("Reader");
            commandReader = new RMICommandReader(reader, myName);
            eventWriter = new RMIEventWriter(myName);
            server.newRMIPlayer((EventWriterInterface) UnicastRemoteObject
                    .exportObject(eventWriter, 0));

            commandReader.start();
        } catch (NotBoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (RemoteException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Gets the command reader.
     *
     * @return the command reader
     */
    public Object getCommandReader() {
        return commandReader.getViewToWait();
    }

}
