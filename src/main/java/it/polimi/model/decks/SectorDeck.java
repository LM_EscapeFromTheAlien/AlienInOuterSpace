package it.polimi.model.decks;

import it.polimi.model.cards.Card;
import it.polimi.model.cards.SectorCard;
import it.polimi.model.cards.sectorcards.NoiseInAnySector;
import it.polimi.model.cards.sectorcards.NoiseInYourSector;
import it.polimi.model.cards.sectorcards.Silence;

import java.util.Collections;
import java.util.LinkedList;

public class SectorDeck extends Deck {

    /** The Constant NUM_OF_NOISE_IN_YOUR_SECTOR. */
    static final int NUM_OF_NOISE_IN_YOUR_SECTOR = 6;

    /** The Constant NUM_OF_NOISE_IN_ANY_SECTOR. */
    static final int NUM_OF_NOISE_IN_ANY_SECTOR = 6;

    /** The Constant NUM_OF_SILENCE. */
    static final int NUM_OF_SILENCE = 5;

    /** The Constant NUM_OF_NOISE_IN_YOUR_SECTOR_WITH_OBJECT. */
    static final int NUM_OF_NOISE_IN_YOUR_SECTOR_WITH_OBJECT = 4;

    /** The Constant NUM_OF_NOISE_IN_ANY_SECTOR_WITH_OBJECT. */
    static final int NUM_OF_NOISE_IN_ANY_SECTOR_WITH_OBJECT = 4;

    /**
     * Instantiates a new sector deck.
     */
    public SectorDeck() {
        super();
        buildDeck();
    }

    @Override
    public void buildDeck() {

        Card tempCard;

        for (int count = 0; count < NUM_OF_NOISE_IN_YOUR_SECTOR_WITH_OBJECT; count++) {
            tempCard = new NoiseInYourSector();
            ((SectorCard) tempCard).setItem();
            getHeadDeck().add(tempCard);
        }

        for (int count = 0; count < NUM_OF_NOISE_IN_YOUR_SECTOR; count++)
            getHeadDeck().add(new NoiseInYourSector());

        for (int count = 0; count < NUM_OF_NOISE_IN_ANY_SECTOR; count++)
            getHeadDeck().add(new NoiseInAnySector());

        for (int count = 0; count < NUM_OF_NOISE_IN_ANY_SECTOR_WITH_OBJECT; count++) {
            tempCard = new NoiseInAnySector();
            ((SectorCard) tempCard).setItem();
            getHeadDeck().add(tempCard);
        }

        for (int count = 0; count < NUM_OF_SILENCE; count++)
            getHeadDeck().add(new Silence());
         Collections.shuffle((LinkedList<Card>) getHeadDeck());

    }

    /**
     * Takes the first card in the HeadDeck and puts it in the DiscardedDeck
     */
    @Override
    public void addtoDiscarded() {
        discardedDeck.add(getHeadDeck().peek());
    }

    /**
     * Fills the HeadDeck with the card's contained in the discardedDeck.
     */
    @Override
    public void reBuildDeck() {

        getHeadDeck().addAll(discardedDeck);
        discardedDeck.clear();

    }
}
