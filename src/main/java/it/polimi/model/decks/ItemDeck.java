package it.polimi.model.decks;

import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Adrenaline;
import it.polimi.model.cards.itemcards.HumanAttack;
import it.polimi.model.cards.itemcards.Light;
import it.polimi.model.cards.itemcards.Protection;
import it.polimi.model.cards.itemcards.Sedative;
import it.polimi.model.cards.itemcards.Teleportation;

import java.util.LinkedList;
import java.util.Collections;
import java.util.List;

public class ItemDeck extends Deck {

    /** The Constant NUM_OF_ADRENALINE. */
    static final int NUM_OF_ADRENALINE = 2;

    /** The Constant NUM_OF_PROTECTION. */
    static final int NUM_OF_PROTECTION = 1;

    /** The Constant NUM_OF_SEDATIVE. */
    static final int NUM_OF_SEDATIVE = 3;

    /** The Constant NUM_OF_LIGHT. */
    static final int NUM_OF_LIGHT = 2;

    /** The Constant NUM_OF_TELEPORTATION. */
    static final int NUM_OF_TELEPORTATION = 2;

    /** The Constant NUM_OF_HUMAN_ATTACK. */
    static final int NUM_OF_HUMAN_ATTACK = 2;

    /**
     * Instantiates a new item deck.
     */
    public ItemDeck() {
        buildDeck();
    }

    @Override
    public void buildDeck() {

        for (int count = 0; count < NUM_OF_PROTECTION; count++)
            getHeadDeck().add(new Protection());
        for (int count = 0; count < NUM_OF_TELEPORTATION; count++)
            getHeadDeck().add(new Teleportation());
        for (int count = 0; count < NUM_OF_ADRENALINE; count++)
            getHeadDeck().add(new Adrenaline());
        for (int count = 0; count < NUM_OF_HUMAN_ATTACK; count++)
            getHeadDeck().add(new HumanAttack());
        for (int count = 0; count < NUM_OF_SEDATIVE; count++)
            getHeadDeck().add(new Sedative());
        for (int count = 0; count < NUM_OF_LIGHT; count++)
            getHeadDeck().add(new Light());

        Collections.shuffle((LinkedList<Card>) getHeadDeck());

    }

    /**
     * adds the chosen card in the DiscardedDeck.
     */
    public void addtoDiscarded(Card take) {
        discardedDeck.add(take);
    }

    /**
     * adds the entire player's hand in the Discarded deck. It appends when the
     * player dies.
     * 
     * @param playerHand
     *            Represents the player's hand, a List.
     */
    public void addtoDiscarded(List<Card> playerHand) {
        for (int cont = 0; cont < playerHand.size(); cont++) {
            addtoDiscarded(playerHand.get(cont));
        }
    }

    /**
     * after checked if the DiscardedDeck isn't empty, fills the HeadDeck with
     * the card's contained in the discardedDeck.
     */
    @Override
    public void reBuildDeck() {
        if (!(discardedDeck.isEmpty())) {
            getHeadDeck().addAll(discardedDeck);
            discardedDeck.clear();
        }
    }

}
