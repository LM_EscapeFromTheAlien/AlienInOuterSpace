package it.polimi.model.decks;

import it.polimi.model.cards.Card;
import it.polimi.model.cards.escapehatchcards.Green;
import it.polimi.model.cards.escapehatchcards.Red;

import java.util.Collections;
import java.util.LinkedList;

public class EscapeHatchDeck extends Deck {

    /** The Constant NUM_OF_RED. */
    static final int NUM_OF_RED = 3;

    /** The Constant NUM_OF_GREEN. */
    static final int NUM_OF_GREEN = 3;

    /**
     * Instantiates a new escape hatch deck.
     */
    public EscapeHatchDeck() {
        buildDeck();
    }

    @Override
    public void buildDeck() {

        for (int count = 0; count < NUM_OF_RED; count++)
            getHeadDeck().add(new Red());
        for (int count = 0; count < NUM_OF_GREEN; count++)
            getHeadDeck().add(new Green());

        Collections.shuffle((LinkedList<Card>) getHeadDeck());

    }

    @Override
    public void reBuildDeck() {
        return;
    }

}
