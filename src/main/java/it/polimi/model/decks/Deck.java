package it.polimi.model.decks;

import java.util.LinkedList;
import java.util.Queue;

import it.polimi.model.cards.Card;

public abstract class Deck {

    private Queue<Card> headDeck;
    protected Queue<Card> discardedDeck;

    /**
     * Instantiates a new deck.
     */
    public Deck() {

        this.headDeck = new LinkedList<Card>();
        this.discardedDeck = new LinkedList<Card>();

    }

    public abstract void buildDeck();

    public abstract void reBuildDeck();

    /**
     * This method is overrided in ItemDeck & SectorDeck
     *
     * @see {@link ItemDeck#addtoDiscarded(Card)},
     *      {@link ItemDeck#addtoDiscarded(java.util.List)}
     * @see SectorDeck#addtoDiscarded()
     */
    public void addtoDiscarded() {
    }

    /**
     * Returns the first card from the HeadDeck
     *
     * @return a type Card
     */
    public Card draw() {
        return headDeck.poll();
    }

    /**
     * @return true if the HeadDeck is empty.
     */
    public boolean isHeadDeckEmpty() {
        return headDeck.isEmpty();
    }

    /**
     * @return true if the DiscardedDeck is empty.
     */

    public boolean isDiscardedDeckEmpty() {
        return discardedDeck.isEmpty();
    }

    public Queue<Card> getHeadDeck() {
        return headDeck;
    }

    public Queue<Card> getDiscardedDeck() {
        return discardedDeck;
    }

}
