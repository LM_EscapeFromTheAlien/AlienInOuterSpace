package it.polimi.model.cards.escapehatchcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.Card;
import it.polimi.model.gamephase.HatchCardPhaseMethods;
import it.polimi.model.map.SectorCoord;

/**
 * The class Red represents the Red Card for trying to escape
 */

public class Red extends HatchCardPhaseMethods implements Card {

    /**
     * Instantiates a new red.
     */
    public Red() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Remove the escape hatch sector, notify to the player who has drawn the
     * card that isn't in safe and control if the game is over.
     */
    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        turnPlayer = gameStatus.getPlayers().peek();
        SectorCoord escapeCoord = gameStatus.getPlayersPosition().get(
                turnPlayer);
        grid = gameStatus.getGridInstance();
        grid.remove(escapeCoord);
        String notEscapedText = String
                .format("%n%s isn't safe, this Escaped Hatch has been damaged, "
                        + "the other players won't be able to use this Escape Hatch Sector %s ",
                        turnPlayer.getPlayerName(), escapeCoord);
        cardDecoredPhase.notifyAll(new CardEvent(notEscapedText));

        tryGameEnd();
    }

}
