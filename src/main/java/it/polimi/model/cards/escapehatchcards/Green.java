package it.polimi.model.cards.escapehatchcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.Card;
import it.polimi.model.gamephase.HatchCardPhaseMethods;
import it.polimi.model.map.SectorCoord;

/**
 * The class Green represents the Green Card for trying to escape
 * 
 * 
 */

public class Green extends HatchCardPhaseMethods implements Card {

    /**
     * Instantiates a new green.
     */
    public Green() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Remove the escape hatch sector, notify to the player who has drawn the
     * card that is in safe and control if the game is over.
     * 
     */
    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        turnPlayer = gameStatus.getPlayers().peek();
        players = gameStatus.getPlayers();
        playersPosition = gameStatus.getPlayersPosition();

        SectorCoord escapeCoord = gameStatus.getPlayersPosition().get(
                turnPlayer);
        grid = gameStatus.getGridInstance();
        grid.remove(escapeCoord);
        String escapedMsg = String
                .format("%n%s is safe,the other players won't be able to use this escape hatch sector %s ",
                        turnPlayer.getPlayerName(), escapeCoord);
        cardDecoredPhase.notifyAll(new CardEvent(escapedMsg));
        players.remove(turnPlayer);
        playersPosition.remove(turnPlayer);

        tryGameEnd();
    }

}
