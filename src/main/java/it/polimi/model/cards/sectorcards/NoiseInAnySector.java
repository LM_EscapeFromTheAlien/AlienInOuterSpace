package it.polimi.model.cards.sectorcards;

import java.util.Map;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.SectorCard;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

/**
 * Represents the Noise in any sector card that makes noise in the selected
 * sector.
 */

public class NoiseInAnySector extends CardPhaseDecorator implements SectorCard {

    private boolean hasItem = false;

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Notifies all players with the sector in which the Turnplayer made noise
     * and starts the new phase.
     *
     * @param row
     * 
     * @param col
     */

    @Override
    public void noise(String row, String col) {
        SectorCoord tempNoisePos = new SectorCoord(row.charAt(0),
                Integer.parseInt(col));
        if (iCanMakeNoiseHere(tempNoisePos)) {
            cardDecoredPhase.notifyAll(new CardEvent(String.format(
                    "%n%s makes noise in %s;%s ", turnPlayer.getPlayerName(),
                    row, col)));
            GamePhase nextPhase = cardDecoredPhase.setNextPhase(gameStatus
                    .getPhaseChanger().getTurnEnd());
            nextPhase.start();
        } else {
            phaseDecorer();
        }
    }

    private boolean iCanMakeNoiseHere(SectorCoord tempNoisePos) {
        Map<SectorCoord, SectType> gridInstance = gameStatus.getGridInstance();
        if (gridInstance.containsKey(tempNoisePos))
            return tempNoisePos.canIMakeNoiseHere(gridInstance
                    .get(tempNoisePos));
        else
            return false;
    }

    @Override
    public boolean getItem() {
        return hasItem;
    }

    @Override
    public void setItem() {
        hasItem = true;
    }

    /**
     * Asks the player where he want to make noise.
     */
    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        turnPlayer = gameStatus.getPlayers().peek();
        cardDecoredPhase.setNextPhase(this);
        this.registerToNoteCenter(cardDecoredPhase.getNotificationCenter());
        cardDecoredPhase
                .notifyPlayer(
                        new CardEvent(
                                "\nSpecify the sector where do you wanna to make noise [follow this shape: noise in (.);(..)]"),
                        turnPlayer);
    }

}
