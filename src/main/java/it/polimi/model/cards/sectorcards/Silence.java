package it.polimi.model.cards.sectorcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.SectorCard;
import it.polimi.model.gamephase.CardPhaseDecorator;

/**
 * Represents the Silence card that makes silence.
 */

public class Silence extends CardPhaseDecorator implements SectorCard {

    /**
     * Instantiates a new silence.
     */
    public Silence() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Notifies all players that the Turnplayer made silence and starts the new
     * phase.
     */

    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        turnPlayer = gameStatus.getPlayers().peek();
        phaseContext = gameStatus.getPhaseChanger();
        this.registerToNoteCenter(cardDecoredPhase.getNotificationCenter());
        cardDecoredPhase.notifyAll(new CardEvent(String.format(
                "%n%s makes silence ", turnPlayer.getPlayerName())));
        nextPhase = cardDecoredPhase.setNextPhase(phaseContext.getTurnEnd());
        nextPhase.start();
    }

    @Override
    public boolean getItem() {
        return false;
    }

    @Override
    public void setItem() {
        return;
    }

}
