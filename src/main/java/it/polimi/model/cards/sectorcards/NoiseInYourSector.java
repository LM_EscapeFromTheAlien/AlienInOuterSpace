package it.polimi.model.cards.sectorcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.SectorCard;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.map.SectorCoord;

/**
 * Represents the Noise in your sector card that makes noise in turnPlayer
 * sector.
 *
 */

public class NoiseInYourSector extends CardPhaseDecorator implements SectorCard {

    private boolean hasItem = false;

    /**
     * Instantiates a new noise in your sector.
     */
    public NoiseInYourSector() {
    }

    @Override
    public void setItem() {
        this.hasItem = true;
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    @Override
    public boolean getItem() {
        return hasItem;
    }

    /**
     * Notifies all players with the sector in which the Turnplayer made noise
     * and starts the new phase.
     */

    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        turnPlayer = gameStatus.getPlayers().peek();
        phaseContext = gameStatus.getPhaseChanger();
        SectorCoord playerPos = gameStatus.getPlayersPosition().get(turnPlayer);

        this.registerToNoteCenter(cardDecoredPhase.getNotificationCenter());
        cardDecoredPhase.notifyAll(new CardEvent(String.format(
                "%n%s makes noise in %s ", turnPlayer.getPlayerName(),
                playerPos.toString())));
        nextPhase = cardDecoredPhase.setNextPhase(phaseContext.getTurnEnd());
        nextPhase.start();
    }

}
