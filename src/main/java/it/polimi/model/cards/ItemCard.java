package it.polimi.model.cards;

/**
 * This interface is implemented by Item cards.
 *
 */
public interface ItemCard extends Card {

    /**
     * Checks if the item Card can be used.
     *
     * @return true, if successful
     */
    public boolean itemConditionToUse();
}
