package it.polimi.model.cards.itemcards;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.ItemCard;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Player;

/**
 * Represents the Light Card to see if there is any players in the selected
 * sector and in its six neighbors.
 */

public class Light extends CardPhaseDecorator implements ItemCard {

    /** The sect with players. */
    protected Collection<SectorCoord> sectWithPlayers = new LinkedList<SectorCoord>();

    /**
     * Instantiates a new light.
     */
    public Light() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Ask to insert the coordinates where the player want to make light.
     */

    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        turnPlayer = gameStatus.getPlayers().peek();
        this.registerToNoteCenter(cardDecoredPhase.getNotificationCenter());
        cardDecoredPhase.setNextPhase(this);
        cardDecoredPhase
                .notifyPlayer(
                        new CardEvent(
                                "\nType Light in (.);(..) to see if there is somebody around there!"),
                        turnPlayer);
    }

    /**
     * After have make light return in the phase the player called the Light's
     * effect.
     *
     * @param col
     *            the col
     * @param row
     *            the row
     */

    @Override
    public void light(String col, String row) {

        int lightRow = Integer.parseInt(row);
        char lightCol = col.charAt(0);

        SectorCoord lightPos = new SectorCoord(lightCol, lightRow);

        playersPosition = gameStatus.getPlayersPosition();

        int offsetFromMyPos1 = lightPos.upperSidesNeighborsOffset(lightPos
                .getCol());
        int offsetFromMyPos2 = lightPos.lowerSidesNeighborsOffset(lightPos
                .getCol());

        if (thereIsSomeoneHere(playersPosition, lightCol, lightRow - 1) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition, lightCol,
                    lightRow - 1));

        if (thereIsSomeoneHere(playersPosition, lightCol - 1, lightRow
                + offsetFromMyPos1) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition,
                    lightCol - 1, lightRow + offsetFromMyPos1));

        if (thereIsSomeoneHere(playersPosition, lightCol - 1, lightRow
                + offsetFromMyPos2) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition,
                    lightCol - 1, lightRow + offsetFromMyPos2));

        if (thereIsSomeoneHere(playersPosition, lightCol, lightRow + 1) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition, lightCol,
                    lightRow + 1));

        if (thereIsSomeoneHere(playersPosition, lightCol + 1, lightRow
                + offsetFromMyPos2) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition,
                    lightCol + 1, lightRow + offsetFromMyPos2));

        if (thereIsSomeoneHere(playersPosition, lightCol + 1, lightRow
                + offsetFromMyPos1) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition,
                    lightCol + 1, lightRow + offsetFromMyPos1));

        if (thereIsSomeoneHere(playersPosition, lightCol, lightRow) != null)
            sectWithPlayers.add(thereIsSomeoneHere(playersPosition, lightCol,
                    lightRow));

        printResult();

        backToNotDecoredPhase();
    }

    private void backToNotDecoredPhase() {
        GamePhase backTo = cardDecoredPhase.setNextPhase(cardDecoredPhase);
        backTo.start();
    }

    private void printResult() {
        Player playerFound;
        if (!sectWithPlayers.isEmpty()) {
            for (SectorCoord sectWithPl : sectWithPlayers) {
                playerFound = getPlayerInThisPos(sectWithPl);
                cardDecoredPhase.notifyPlayer(
                        new CardEvent(sectWithPl.toString() + " "
                                + playerFound.getPlayerName()), turnPlayer);
            }
        } else {
            cardDecoredPhase.notifyPlayer(new CardEvent(
                    "\nThere aren't players around!"), turnPlayer);
        }
    }

    private Player getPlayerInThisPos(SectorCoord sectWithPl) {
        for (Player playerFound : playersPosition.keySet())
            if (playersPosition.get(playerFound).equals(sectWithPl))
                return playerFound;

        return null;
    }

    private SectorCoord thereIsSomeoneHere(
            Map<Player, SectorCoord> playersPosition, int col, int row) {

        SectorCoord tempCoord = new SectorCoord((char) col, row);

        for (Player player : playersPosition.keySet()) {
            if (playersPosition.get(player).equals(tempCoord)) {
                return tempCoord;
            }
        }
        return null;
    }

    @Override
    public boolean itemConditionToUse() {
        return true;
    }
}
