package it.polimi.model.cards.itemcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.cards.ItemCard;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;

/**
 * Represents the Human Attack Card to allow a human player to attack.
 */

public class HumanAttack extends CardPhaseDecorator implements ItemCard {

    private GamePhase goToAtk;

    /**
     * Instantiates a new human attack.
     */
    public HumanAttack() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Notify all players that the turn player are using the card Human Attack
     * and starts the AttackPhase.
     * 
     */
    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        turnPlayer = gameStatus.getPlayers().peek();
        String cardStartMsg = String.format("%n%s is using Human attack",
                turnPlayer.getPlayerName());
        cardDecoredPhase.notifyAll(new CardEvent(cardStartMsg));
        goToAtk = cardDecoredPhase.setNextPhase(phaseContext.getAttack());
        goToAtk.start();
    }

    @Override
    public boolean itemConditionToUse() {
        gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        return cardDecoredPhase.equals(phaseContext.getAttackOrSkip())
                || cardDecoredPhase.equals(phaseContext.getAttackOrDraw());
    }
}
