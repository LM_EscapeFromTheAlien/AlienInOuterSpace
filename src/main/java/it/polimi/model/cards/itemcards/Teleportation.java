package it.polimi.model.cards.itemcards;

import it.polimi.common.observer.event.NewPositionEvent;
import it.polimi.model.cards.ItemCard;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.map.SectorCoord;

/**
 * Represents the Teleport Card that moves the current TurnPlayer position in
 * the Human Sector.
 */

public class Teleportation extends CardPhaseDecorator implements ItemCard {

    /**
     * Instantiates a new teleportation.
     */
    public Teleportation() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Puts the player in the Human sector
     */
    @Override
    public void phaseDecorer() {
        gameStatus = cardDecoredPhase.getGameStatus();
        turnPlayer = gameStatus.getPlayers().peek();
        playersPosition = gameStatus.getPlayersPosition();
        teleportPlayer();
    }

    private void teleportPlayer() {
        SectorCoord to = new SectorCoord('L', 8);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, to);
        cardDecoredPhase.notifyAll(new NewPositionEvent(String.format(
                "%nNow %s is in %s, the human sector",
                turnPlayer.getPlayerName(), to.toString())));

    }

    @Override
    public boolean itemConditionToUse() {
        return true;
    }

}
