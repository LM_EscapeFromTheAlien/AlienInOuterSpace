package it.polimi.model.cards.itemcards;

import it.polimi.model.cards.ItemCard;
import it.polimi.model.gamephase.CardPhaseDecorator;

/**
 * Represents the Protection Card that enable the player's protection.
 */

public class Protection extends CardPhaseDecorator implements ItemCard {

    /**
     * Instantiates a new protection.
     */
    public Protection() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    @Override
    public boolean itemConditionToUse() {
        return false;
    }

    @Override
    public void phaseDecorer() {
        return;
    }

    /**
     * Calls the player's method 'setProtected'.
     */

    @Override
    public void drawEffetc() {
        gameStatus = cardDecoredPhase.getGameStatus();
        turnPlayer = gameStatus.getPlayers().peek();
        turnPlayer.setProtected();
    }
}
