package it.polimi.model.cards.itemcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.ItemCard;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;

/**
 * Represents the Sedative Card that prevents the player to draw a Sector Card.
 */

public class Sedative extends CardPhaseDecorator implements ItemCard {

    private GamePhase goToEnd;

    /**
     * Instantiates a new sedative.
     */
    public Sedative() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * After notifying all players that the TurnPlayer won't draw sends him to
     * turnEndPhase.
     */

    @Override
    public void phaseDecorer() {
        GameStatus gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        turnPlayer = gameStatus.getPlayers().peek();
        cardDecoredPhase.notifyAll(new CardEvent(String.format(
                "%n%s won't draw this turn", turnPlayer.getPlayerName())));
        goToEnd = cardDecoredPhase.setNextPhase(phaseContext.getTurnEnd());
        goToEnd.start();
    }

    @Override
    public boolean itemConditionToUse() {
        gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        return cardDecoredPhase.equals(phaseContext.getAttackOrDraw());
    }

}
