package it.polimi.model.cards.itemcards;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.ItemCard;
import it.polimi.model.gamephase.CardPhaseDecorator;

/**
 * Represents the Adrenaline Card to increase the number of steps
 */

public class Adrenaline extends CardPhaseDecorator implements ItemCard {

    /**
     * Instantiates a new adrenaline.
     */
    public Adrenaline() {
    }

    @Override
    public String toString() {
        return String.format("%s", getClass().getSimpleName());
    }

    /**
     * Calls the player's method 'speedUp'
     * 
     * @link Player
     */
    @Override
    public void phaseDecorer() {
        GameStatus gameStatus = cardDecoredPhase.getGameStatus();
        phaseContext = gameStatus.getPhaseChanger();
        turnPlayer = gameStatus.getPlayers().peek();
        turnPlayer.speedUp();
        String cardStartMsg = String
                .format("%nNow %s is boosted, he'll make two steps at the next movement",
                        turnPlayer.getPlayerName());
        cardDecoredPhase.notifyAll(new CardEvent(cardStartMsg));

    }

    @Override
    public boolean itemConditionToUse() {
        phaseContext = cardDecoredPhase.getGameStatus().getPhaseChanger();
        return cardDecoredPhase.equals(phaseContext.getTurnStart());
    }
}
