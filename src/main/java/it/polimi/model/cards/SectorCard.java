package it.polimi.model.cards;

/**
 * This interface is implemented by Sector cards.
 */

public interface SectorCard extends Card {

    /**
     * Use this for setting which card has the Item card associated.
     */
    public abstract void setItem();

    /**
     * For checking that a Sector card has an Item card associated.
     *
     * @return the item
     */

    public abstract boolean getItem();

}
