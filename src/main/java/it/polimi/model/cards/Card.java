package it.polimi.model.cards;

/**
 * A simple interface that represents type Card used by classes in the project.
 *
 */
public interface Card {

    /**
     * A method that using the Cards' effects.
     */
    void phaseDecorer();

}
