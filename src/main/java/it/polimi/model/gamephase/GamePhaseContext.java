package it.polimi.model.gamephase;

import it.polimi.model.GameStatus;
import it.polimi.model.NotificationCenter;

/**
 * This class is used to create and initialize each game phase. It provides the
 * related setters and getters.
 * 
 * @author Davide Francesco Losio.
 *
 */
public class GamePhaseContext {

    private GamePhase gamePhase;
    private final GamePhase turnStart;
    private final GamePhase attackOrDraw;
    private final GamePhase attackOrSkip;
    private final GamePhase discardItem;
    private final GamePhase turnEnd;
    private final GamePhase escapePhase;
    private final GamePhase useSectCard;
    private final GamePhase attack;
    private final GamePhase endGame;

    /**
     * Instantiates a new game phase context.
     *
     * @param gameStatus
     *            the game status
     */
    public GamePhaseContext(GameStatus gameStatus) {
        NotificationCenter noteCenter = gameStatus.getNoteCenter();
        escapePhase = new EscapePhase(gameStatus, this);
        attackOrSkip = new AttackOrSkipPhase(gameStatus, this);
        attackOrDraw = new AttackOrDrawPhase(gameStatus, this);
        turnStart = new TurnStartPhase(gameStatus, this);
        discardItem = new DiscardItemPhase(gameStatus, this);
        turnEnd = new TurnEndPhase(gameStatus, this);
        useSectCard = new UseSectCardPhase(gameStatus, this);
        attack = new AttackPhase(gameStatus, this);
        endGame = new GameEndPhase(gameStatus, this);

        endGame.registerToNoteCenter(noteCenter);
        attackOrSkip.registerToNoteCenter(noteCenter);
        attack.registerToNoteCenter(noteCenter);
        useSectCard.registerToNoteCenter(noteCenter);
        turnEnd.registerToNoteCenter(noteCenter);
        turnStart.registerToNoteCenter(noteCenter);
        attackOrDraw.registerToNoteCenter(noteCenter);
        discardItem.registerToNoteCenter(noteCenter);
        escapePhase.registerToNoteCenter(noteCenter);
        gamePhase = turnStart;
    }

    /**
     * Gets the end game.
     *
     * @return the end game
     */
    public GamePhase getEndGame() {
        return endGame;
    }

    /**
     * Gets the game phase.
     *
     * @return the game phase
     */
    public GamePhase getGamePhase() {
        return gamePhase;
    }

    /**
     * Sets the game phase.
     *
     * @param gamePhase
     *            the new game phase
     */
    public void setGamePhase(GamePhase gamePhase) {
        this.gamePhase = gamePhase;
    }

    /**
     * Gets the turn start.
     *
     * @return the turn start
     */
    public GamePhase getTurnStart() {
        return turnStart;
    }

    /**
     * Gets the attack or draw.
     *
     * @return the attack or draw
     */
    public GamePhase getAttackOrDraw() {
        return attackOrDraw;
    }

    /**
     * Gets the discard item.
     *
     * @return the discard item
     */
    public GamePhase getDiscardItem() {
        return discardItem;
    }

    /**
     * Gets the turn end.
     *
     * @return the turn end
     */
    public GamePhase getTurnEnd() {
        return turnEnd;
    }

    /**
     * Gets the escape phase.
     *
     * @return the escape phase
     */
    public GamePhase getEscapePhase() {
        return escapePhase;
    }

    /**
     * Gets the use sect card.
     *
     * @return the use sect card
     */
    public GamePhase getUseSectCard() {
        return useSectCard;
    }

    /**
     * Gets the attack.
     *
     * @return the attack
     */
    public GamePhase getAttack() {
        return attack;
    }

    /**
     * Gets the attack or skip.
     *
     * @return the attack or skip
     */
    public GamePhase getAttackOrSkip() {
        return attackOrSkip;
    }

    /**
     * End game.
     */
    public void endGame() {
        this.gamePhase = endGame;
    }

}
