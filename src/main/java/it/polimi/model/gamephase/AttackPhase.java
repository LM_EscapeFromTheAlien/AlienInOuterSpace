package it.polimi.model.gamephase;

import java.util.List;

import it.polimi.common.observer.event.AttackEvent;
import it.polimi.common.observer.event.GameEnd;
import it.polimi.common.observer.event.PlayerLoseEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.Card;
import it.polimi.model.decks.ItemDeck;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Player;

/**
 * This class represents Attack phase in which TurnPlayer try to attack
 * 
 */

public class AttackPhase extends GamePhase {

    private Player playerToRemove;

    /**
     * Instantiates a new attack phase.
     *
     * @param gameStatus
     * 
     * @param phaseChanger
     * 
     */
    public AttackPhase(GameStatus gameStatus, GamePhaseContext phaseChanger) {
        super(gameStatus, phaseChanger);
    }

    /**
     * This method checks if the attacked player has Protection card if yes the
     * attack is failed if false remove the attacked Player from the list,
     * notifies all players and then checks if there are any other players, if
     * yes starts TurnStartPhase else starts GameEndphase
     */
    @Override
    public void start() {
        ItemDeck itemDeck = (ItemDeck) gameStatus.getItemDeck();
        turnPlayer = gameStatus.getPlayers().peek();
        SectorCoord sectorCoord = gameStatus.getPlayersPosition().get(
                turnPlayer);
        notifyAll(new AttackEvent(String.format(
                "%n%s is going to attack in %s!", turnPlayer.getPlayerName(),
                sectorCoord)));
        if (otherPlayerHere()) {
            if (playerToRemove.iAmProtect()) {
                Card protectionUsed = playerToRemove.notProtected();
                notifyAll(new AttackEvent("\nAttack failed! "
                        + playerToRemove.getPlayerName() + " used Protection! "));
                itemDeck.addtoDiscarded(protectionUsed);

            } else {
                attackPlayer();
            }
            tryNextPhase();
        } else {
            notifyAll(new AttackEvent("Attack failed!"));
            tryNextPhase();
        }
    }

    private void attackPlayer() {
        ItemDeck itemDeck = (ItemDeck) gameStatus.getItemDeck();
        List<Card> playerHand = playerToRemove.getPlayerHand();
        String loseMsg;
        gameStatus.getPlayersPosition().remove(playerToRemove);
        gameStatus.getPlayers().remove(playerToRemove);
        itemDeck.addtoDiscarded(playerHand);
        loseMsg = String.format("%n%s was killed by %s, %s Lost!",
                playerToRemove.getPlayerName(), turnPlayer.getPlayerName(),
                playerToRemove.getPlayerName());
        notifyAll(new PlayerLoseEvent(loseMsg));
        if ("Alien".equals(turnPlayer))
            turnPlayer.speedUp();
    }

    private void tryNextPhase() {
        players = gameStatus.getPlayers();
        Player almostOneHuman = null;
        Player almostOneAlien = null;
        for (Player player : players) {
            if ("Human".equals(player.getPlayerRace()))
                almostOneHuman = player;
            if ("Alien".equals(player.getPlayerRace()))
                almostOneAlien = player;
        }
        if (almostOneHuman == null)
            allHumanDeads();
        if (almostOneAlien == null)
            allAlienDeads();
        if (almostOneHuman != null && almostOneAlien != null)
            nextPhase();
    }

    private void allAlienDeads() {
        notifyAll(new GameEnd("\nAll aliens have been killed!! Humans win!"));
        nextPhase = setNextPhase(phaseContext.getEndGame());
        nextPhase.start();
    }

    private void allHumanDeads() {
        notifyAll(new GameEnd(
                "\nAll humans have been Killed!! Aliens and Humans in safe win!"));
        nextPhase = setNextPhase(phaseContext.getEndGame());
        nextPhase.start();
    }

    private void nextPhase() {
        nextPhase = setNextPhase(phaseContext.getTurnEnd());
        nextPhase.start();
    }

    private boolean otherPlayerHere() {
        for (Player otherPlayer : playersPosition.keySet()) {
            if (isARealOtherPlayer(otherPlayer)) {
                playerToRemove = otherPlayer;
                return true;
            }
        }
        return false;
    }

    private boolean isARealOtherPlayer(Player otherPlayer) {
        Player turnPlayer = gameStatus.getPlayers().peek();
        SectorCoord actualPosition = gameStatus.getPlayersPosition().get(
                turnPlayer);
        String playerName = turnPlayer.getPlayerName();
        String otherPlayerName = otherPlayer.getPlayerName();
        return !(otherPlayerName.equals(playerName))
                && actualPosition.equals(playersPosition.get(otherPlayer));
    }

}
