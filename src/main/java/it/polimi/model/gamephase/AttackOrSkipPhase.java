package it.polimi.model.gamephase;

import it.polimi.common.observer.event.NewPhaseEvent;
import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

/**
 * This class represents game phase in which the TurnPlayer can choose between
 * make an attack or skip to TurnEndPhase. Can use an Item card or see his hand.
 *
 * @see GamePhase#useItemCard(int)
 * @see GamePhase#showCards()
 */

public class AttackOrSkipPhase extends GamePhase {

    /**
     * Instantiates a new attack or skip phase.
     *
     * @param gameStatus
     * 
     * @param gamePhaseContext
     * 
     */
    public AttackOrSkipPhase(GameStatus gameStatus,
            GamePhaseContext gamePhaseContext) {
        super(gameStatus, gamePhaseContext);
    }

    /**
     * Checking if the Sector is an Escape sector, if true starts the
     * EscapePhase otherwise allows to skip to TurnEndphase.
     * 
     */

    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        resetTurnPlayer();
        if (humanTryToEscape()) {
            GamePhase gamePhase = setNextPhase(phaseContext.getEscapePhase());
            gamePhase.start();
        } else {
            String whatIcanDo = "\nYou can:";
            String skip = "\n- skip ";
            String atk = "\n- attack ";
            String show = "\n- show  cards [you will see your item cards] ";
            String item = "\n- use item x [where x is the number of the card]";
            notifyPlayer(new NewPhaseEvent(whatIcanDo + skip + atk + show
                    + item), turnPlayer);
        }
    }

    /**
     * This method starts AttackPhase
     */

    @Override
    public void attack() {
        if (turnPlayer.canAttack()) {
            nextPhase = setNextPhase(phaseContext.getAttack());
            nextPhase.start();
        } else {
            notifyPlayer(new CardEvent(
                    "\nYou have to use item 'Human attack' for attack!"),
                    turnPlayer);
        }
    }

    /**
     * This method starts TurnEndPhase
     * 
     */
    @Override
    public void skip() {
        nextPhase = setNextPhase(phaseContext.getTurnEnd());
        nextPhase.start();
    }

    private boolean humanTryToEscape() {
        SectorCoord actualPosition = gameStatus.getPlayersPosition().get(
                turnPlayer);
        return turnPlayer.canEscape()
                && gameStatus.getGridInstance().get(actualPosition)
                        .equals(SectType.ESCAPE);
    }
}
