package it.polimi.model.gamephase;

import it.polimi.common.observer.event.NotValidCmd;

/**
 * This class is used by the cards for save in which phase is the game status.
 *
 *
 */

public abstract class CardPhaseDecorator extends GamePhase {

    protected GamePhase cardDecoredPhase;

    /**
     * Sets the card decored phase.
     *
     * @param cardDecoredPhase
     * 
     */
    public void setCardDecoredPhase(GamePhase cardDecoredPhase) {
        this.cardDecoredPhase = cardDecoredPhase;
    }

    @Override
    public void showCards() {
        notifyAll(new NotValidCmd(NOTVALIDMOVE));
    }

    @Override
    public void useItemCard(int chose) {
        notifyAll(new NotValidCmd(NOTVALIDMOVE));
    }

    public void drawEffetc() {
        return;
    }

    public abstract void phaseDecorer();

}
