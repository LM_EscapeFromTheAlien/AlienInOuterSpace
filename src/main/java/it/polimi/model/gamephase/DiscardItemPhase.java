package it.polimi.model.gamephase;

import java.util.List;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.common.observer.event.NewPhaseEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.Card;
import it.polimi.model.decks.ItemDeck;

/**
 * Checks if the player has four cards in his hand and makes he discards or uses
 * cards. Can use an Item card or see his hand.
 *
 * @see GamePhase#useItemCard(int)
 * @see GamePhase#showCards()
 */

public class DiscardItemPhase extends GamePhase {

    private GamePhase gamePhase;

    /**
     * Instantiates a new discard item phase.
     *
     * @param gameStatus
     * 
     * @param phaseChanger
     * 
     */
    public DiscardItemPhase(GameStatus gameStatus, GamePhaseContext phaseChanger) {
        super(gameStatus, phaseChanger);
    }

    /**
     * Checks if the player has four cards in his hand and starts TurnEndPhase
     */
    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        if (turnPlayer.getPlayerHand().size() < 4) {
            gamePhase = setNextPhase(phaseContext.getTurnEnd());
            gamePhase.start();
        } else {
            String whatIcanDo = "\nYou have to use or discard an item card!:";
            String show = "\n- show cards ";
            String item = "\n- use item x [where x is the number of the card]";
            String rename = "\n- discard x [where x is the number of the card]";
            notifyPlayer(new NewPhaseEvent(whatIcanDo + show + item + rename),
                    turnPlayer);
        }
    }

    /**
     * @see GamePhase#useItemCard(int)
     */

    @Override
    public void useItemCard(int chose) {
        super.useItemCard(chose);
        if (turnPlayer.getPlayerHand().size() < 4
                && phaseContext.getGamePhase().equals(this)) {
            gamePhase = setNextPhase(phaseContext.getTurnEnd());
            gamePhase.start();
        }
    }

    /**
     * Makes the player discards the chosen card and starts TurnEndPhase
     */
    @Override
    public void discard(int choice) {
        ItemDeck itemDeck = (ItemDeck) gameStatus.getItemDeck();
        List<Card> playerHand = turnPlayer.getPlayerHand();
        Card cardUsed = playerHand.get(choice);
        itemDeck.addtoDiscarded(cardUsed);
        turnPlayer.getPlayerHand().remove(choice);
        notifyAll(new CardEvent(String.format("%n%s discarded an Item card ",
                turnPlayer.getPlayerName())));

        gamePhase = setNextPhase(phaseContext.getTurnEnd());
        gamePhase.start();
    }
}
