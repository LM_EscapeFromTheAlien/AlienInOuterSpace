package it.polimi.model.gamephase;

import java.util.Timer;
import java.util.TimerTask;

import it.polimi.common.observer.event.*;
import it.polimi.model.GameStatus;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Player;

/**
 * 
 * This class represents the first game phase in which the TurnPlayer can make
 * move, use an Item card or see his hand.
 *
 * @see GamePhase#useItemCard(int)
 * @see GamePhase#showCards()
 */
public class TurnStartPhase extends GamePhase {

    private static final long TIMEOUT = (long) 5 * 60 * 1000;
    private static final long MAXTURN = 39;

    public TurnStartPhase(GameStatus gameStatus,
            GamePhaseContext gamePhaseContext) {
        super(gameStatus, gamePhaseContext);
    }

    /**
     * In this GamePhase the method saves the TurnPlayer, starts the Turn timer,
     * checks if the game is over and allow to make a movement.
     */
    @Override
    public void start() {
        gameStatus.turnPassed();
        turnPlayer = gameStatus.getPlayers().peek();
        startTimer();
        if (!tryGameEnd()) {
            String whatIcanDo = String
                    .format("%nYou are %s. You are in %s.%nYou can:",
                            turnPlayer.getPlayerRace(),
                            playersPosition.get(turnPlayer));
            String move = "\n- move (.);(..)";
            String show = "\n- show cards";
            String item = "\n- use item x [where x is the number of the card]";
            notifyAll(new TurnStartEvent(String.format("it's %s's turn",
                    turnPlayer.getPlayerName())));
            notifyPlayer(new TurnStartEvent(whatIcanDo + move + show + item),
                    turnPlayer);
        }
    }

    private void startTimer() {
        gameStatus.setTimer(new Timer());
        TimerTask turnTimeOut = new TimeOut(phaseContext);
        gameStatus.getTimer().schedule(turnTimeOut, TIMEOUT);
    }

    private boolean tryGameEnd() {
        if (gameStatus.getTurnCounter() > MAXTURN) {
            notifyAll(new GameEnd(
                    "\n The rounds are over! Humans in safe and Aliens win!!"));
            nextPhase = setNextPhase(phaseContext.getEndGame());
            nextPhase.start();
            return true;
        } else if (tooFewPlayers()) {
            notifyAll(new GameEnd("\nHumans in safe and Aliens win!!"));
            nextPhase = setNextPhase(phaseContext.getEndGame());
            nextPhase.start();
            return true;
        }
        return false;

    }

    private boolean tooFewPlayers() {
        players = gameStatus.getPlayers();
        Player almostOneHuman = null;
        Player almostOneAlien = null;
        for (Player player : players) {
            if ("Human".equals(player.getPlayerRace()))
                almostOneHuman = player;
            if ("Alien".equals(player.getPlayerRace()))
                almostOneAlien = player;
        }
        return !((almostOneHuman != null) && (almostOneAlien != null));

    }

    /**
     * This method , after have removed the player who has called move from the
     * hash, put him in another position, notifies all players and then starts
     * the new phase
     */
    @Override
    public void move(String toCol, String toRow) {
        turnPlayer = gameStatus.getPlayers().peek();
        char col = toCol.charAt(0);
        int row = Integer.parseInt(toRow);
        SectorCoord to = new SectorCoord(col, row);
        if (playerMoveValidation(turnPlayer, playersPosition.get(turnPlayer),
                to)) {
            playersPosition.remove(turnPlayer);
            playersPosition.put(turnPlayer, to);
            notifyPlayer(
                    new NewPositionEvent(String.format("%nnow you are in %s",
                            to.toString())), turnPlayer);
            nextPhase(to);
        } else {
            notifyPlayer(new BadPositionEvent("\nYou can't go there"),
                    turnPlayer);
        }
    }

    private void nextPhase(SectorCoord to) {
        grid = gameStatus.getGridInstance();
        if (grid.get(to) == SectType.DANGEROUS) {
            nextPhase = setNextPhase(phaseContext.getAttackOrDraw());
            nextPhase.start();
        } else {
            nextPhase = setNextPhase(phaseContext.getAttackOrSkip());
            nextPhase.start();
        }

    }

    private boolean playerMoveValidation(Player player, SectorCoord from,
            SectorCoord to) {
        return !from.equals(to) && player.canMove(grid, from, to);
    }
}
