package it.polimi.model.gamephase;

import it.polimi.common.observer.event.NewPhaseEvent;
import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.GameStatus;

/**
 * This class represents game phase in which the TurnPlayer can choose between
 * make an attack or draw a Sector card. Can use an Item card or see his hand.
 *
 * @see GamePhase#useItemCard(int)
 * @see GamePhase#showCards()
 */

public class AttackOrDrawPhase extends GamePhase {

    /**
     * Instantiates a new attack or draw phase.
     *
     * @param gameStatus
     * 
     * @param phaseChanger
     * 
     */
    public AttackOrDrawPhase(GameStatus gameStatus,
            GamePhaseContext phaseChanger) {
        super(gameStatus, phaseChanger);
    }

    /**
     * Checks and eventually resets the player's number of steps. Allows to make
     * an attack or to draw a sector card.
     */
    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        resetTurnPlayer();
        String whatIcanDo = "\nYou can:";
        String draw = "\n- draw [you will draw a sector card]";
        String atk = "\n- attack";
        String show = "\n- show  cards [you will see your item cards] ";
        String item = "\n- use item x [where x is the number of the card]";
        notifyPlayer(new NewPhaseEvent(whatIcanDo + draw + atk + show + item),
                turnPlayer);
    }

    /**
     * This method starts UseSectCardPhase
     */
    @Override
    public void drawSectCard() {
        nextPhase = phaseContext.getUseSectCard();
        nextPhase.start();
    }

    /**
     * This method starts AttackPhase
     */

    @Override
    public void attack() {
        if (turnPlayer.canAttack()) {
            nextPhase = phaseContext.getAttack();
            nextPhase.start();
        }
        if ("Human".equals(turnPlayer.getPlayerRace()))
            notifyPlayer(
                    new CardEvent(
                            "\nYou have to use item 'Human attack' for make an attack!"),
                    turnPlayer);

    }
}
