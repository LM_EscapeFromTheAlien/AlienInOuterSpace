package it.polimi.model.gamephase;

import it.polimi.common.observer.event.NewPhaseEvent;

import it.polimi.model.GameStatus;
import it.polimi.model.player.Player;

/**
 * In this phase the player can use an item card or passes his turn and checks
 * if the players has four cards in his hand.
 * 
 *
 */
public class TurnEndPhase extends GamePhase {

    private GamePhase gamePhase;

    /**
     * Instantiates a new turn end phase.
     *
     * @param gameStatus
     * 
     * @param gamePhaseContext
     * 
     */
    public TurnEndPhase(GameStatus gameStatus, GamePhaseContext gamePhaseContext) {
        super(gameStatus, gamePhaseContext);
    }

    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        if (turnPlayer.handFull())
            discadItem();
        else {
            String whatIcanDo = "\nYou can:";
            String show = "\n- show cards ";
            String item = "\n- use item x [where x is the number of the card]";
            String end = "\n- end";
            notifyPlayer(new NewPhaseEvent(whatIcanDo + show + item + end),
                    turnPlayer);
        }
    }

    private void discadItem() {
        gamePhase = setNextPhase(phaseContext.getDiscardItem());
        gamePhase.start();
    }

    /**
     * Stops the turn's timer and starts TurnStartPhase
     */
    @Override
    public void end() {
        stopTimer();
        Player tempPlayer = players.poll();
        players.add(tempPlayer);
        gamePhase = setNextPhase(phaseContext.getTurnStart());
        gamePhase.start();
    }

    private void stopTimer() {
        gameStatus.getTimer().cancel();
        gameStatus.getTimer().purge();
    }
}
