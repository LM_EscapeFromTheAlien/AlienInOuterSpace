package it.polimi.model.gamephase;

import it.polimi.common.observer.event.NotValidCmd;
import it.polimi.model.GameStatus;

/**
 * In this class the player is allowed to type 'exit' because the game is over.
 *
 */
public class GameEndPhase extends GamePhase {
    /**
     * Instantiates a new game end phase.
     *
     * @param gameStatus
     * 
     * @param phaseContext
     * 
     */
    public GameEndPhase(GameStatus gameStatus, GamePhaseContext phaseContext) {
        super(gameStatus, phaseContext);
    }

    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        notifyEnd();
    }

    @Override
    public void showCards() {
        notifyEnd();
    }

    private void notifyEnd() {
        notifyAll(new NotValidCmd(
                String.format("%nThe game is over type Exit to quit.")));
    }

    @Override
    public void useItemCard(int chose) {
        notifyEnd();
    }

    @Override
    public void move(String row, String col) {
        notifyEnd();
    }

    @Override
    public void cmdUkn() {
        notifyEnd();
    }

    @Override
    public void drawSectCard() {
        notifyEnd();
    }

    @Override
    public void noise(String row, String col) {
        notifyEnd();
    }

    @Override
    public void attack() {
        notifyEnd();
    }

    @Override
    public void end() {
        notifyEnd();
    }

    @Override
    public void skip() {
        notifyEnd();
    }

    @Override
    public void light(String row, String col) {
        notifyEnd();
    }

    @Override
    public void discard(int discardChoice) {
        notifyEnd();
    }
}
