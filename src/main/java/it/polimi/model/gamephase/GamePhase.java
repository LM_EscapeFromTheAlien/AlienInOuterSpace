package it.polimi.model.gamephase;

import java.util.List;
import java.util.Map;
import java.util.Queue;

import it.polimi.common.observer.event.NotValidCmd;
import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.NotificationCenter;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.ItemCard;
import it.polimi.model.decks.ItemDeck;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Player;

/**
 * This class rappresents how the game phases are structured, each game phase
 * have to contain the reference to the Game status, to the Notification center,
 * to the GamePhasecontext, to the players' list, to players'position and to the
 * Grid.
 *
 */

public abstract class GamePhase {

    /** The next phase. Used by each phase for switch to the next */
    protected GamePhase nextPhase;
    private NotificationCenter notificationCenter;

    /** The game status. Contains the reference to GameStatus object */
    protected GameStatus gameStatus;

    /** This is used to get the references of game phases */
    protected GamePhaseContext phaseContext;

    /**
     * This is used to get the reference to the first player in Players' list
     */
    protected Player turnPlayer;

    /**
     * This contains a queue where are saved the players that are playing this
     * game
     */
    protected Queue<Player> players;

    /** Contains an instance of the the maps */
    protected Map<SectorCoord, SectType> grid;

    /**
     * Contains the references of Players' position during the game
     */
    protected Map<Player, SectorCoord> playersPosition;

    /** The Constant NOTVALIDMOVE. */
    protected static final String NOTVALIDMOVE = "\nYou can't use this command now!";

    /**
     * Instantiates a new game phase.
     */
    public GamePhase() {
    }

    /**
     * Instantiates a new game phase.
     *
     * @param gameStatus
     *            This is the reference to the GameStatus.
     * @param phaseChanger
     *            This is the reference to the GamePhaseContext.
     */
    public GamePhase(GameStatus gameStatus, GamePhaseContext phaseChanger) {
        this.gameStatus = gameStatus;
        this.phaseContext = phaseChanger;
        players = gameStatus.getPlayers();
        grid = gameStatus.getGridInstance();
        playersPosition = gameStatus.getPlayersPosition();
    }

    /**
     * this saves the NoteCenter.
     *
     * @param noteCenter
     */
    public void registerToNoteCenter(NotificationCenter noteCenter) {
        this.notificationCenter = noteCenter;
    }

    /**
     * Sets the next phase.
     *
     * @param gamePhase
     * 
     * @return the new game phase
     */
    public GamePhase setNextPhase(GamePhase gamePhase) {
        phaseContext.setGamePhase(gamePhase);
        return phaseContext.getGamePhase();
    }

    /**
     * Make possible to use the notification center.
     *
     * @return the notification center
     */
    public NotificationCenter getNotificationCenter() {
        return notificationCenter;
    }

    /**
     * Make possible to use the game status.
     *
     * @return the game status
     */
    public GameStatus getGameStatus() {
        return gameStatus;
    }

    /**
     * This method is used to notify all players' view.
     *
     * @param event
     * 
     * 
     */
    public void notifyAll(Event event) {
        notificationCenter.notifyAll(event);
    }

    /**
     * This method is used to notify only the Turnplayer view.
     *
     * @param event
     * 
     * @param player
     * 
     */
    public void notifyPlayer(Event event, Player player) {
        notificationCenter.notifyPlayer(event, player);
    }

    /**
     * This method can be called in any game phase and show the Turnplayer's
     * hand.
     */
    public void showCards() {
        String deck;
        deck = turnPlayer.getPlayerHand().toString();
        notifyPlayer(new CardEvent(deck), turnPlayer);
    }

    /**
     *
     * This method can be called in any game phase for using the Items card
     * 
     *
     * @param choose
     *            the selected card to use
     */
    public void useItemCard(int choose) {
        ItemDeck itemDeck = (ItemDeck) gameStatus.getItemDeck();
        turnPlayer = gameStatus.getPlayers().peek();
        List<Card> playerHand = turnPlayer.getPlayerHand();
        if (!canIHuseItem(choose, playerHand)) {
            notifyPlayer(new NotValidCmd("\nCannot use this item now"),
                    turnPlayer);
        } else if (itemActivableCheck(playerHand, choose)) {
            Card itemCardToUse = playerHand.get(choose - 1);
            itemDeck.addtoDiscarded(itemCardToUse);
            playerHand.remove(itemCardToUse);
            ((CardPhaseDecorator) itemCardToUse).setCardDecoredPhase(this);
            itemCardToUse.phaseDecorer();
        } else {
            notifyPlayer(new NotValidCmd("\nCannot use this item"), turnPlayer);
        }
    }

    private boolean itemActivableCheck(List<Card> playerHand, int chose) {
        Card itemCardToUse = (ItemCard) playerHand.get(chose - 1);
        ((CardPhaseDecorator) itemCardToUse).setCardDecoredPhase(this);
        return ((ItemCard) itemCardToUse).itemConditionToUse();
    }

    private boolean canIHuseItem(int chose, List<Card> playerHand) {
        return (chose - 1) < playerHand.size() && chose > 0
                && turnPlayer.canUseItem();
    }

    /**
     * Player exit. Each Players can use this method for exit from the game in
     * any moment
     *
     * @param playerWhoSendCmd
     *            the player who send cmd
     */
    public void playerExit(Player playerWhoSendCmd) {
        ItemDeck itemDeck = (ItemDeck) gameStatus.getItemDeck();
        turnPlayer = gameStatus.getPlayers().peek();
        itemDeck.addtoDiscarded(playerWhoSendCmd.getPlayerHand());
        gameStatus.getPlayers().remove(playerWhoSendCmd);
        gameStatus.getPlayersPosition().remove(playerWhoSendCmd);
        notifyAll(new NotValidCmd(playerWhoSendCmd.getPlayerName()
                + " has left the game"));
        if (turnPlayer.equals(playerWhoSendCmd)
                && gameStatus.getTurnCounter() > 0) {
            nextPhase = setNextPhase(phaseContext.getTurnStart());
            nextPhase.start();
        }
    }

    /**
     * Move. this method is invoked for chsnge the Turnplayers' position
     *
     * @param row
     * @param col
     * 
     */
    public void move(String row, String col) {
        notifyPlayer(
                new NotValidCmd(String.format(
                        "%nYou can't use the command 'move %s;%s' now!", row,
                        col)), turnPlayer);
    }

    /**
     * This method is overrided in each game phase and able the current phase to
     * start.
     */
    public void start() {
        cmdUkn();
    }

    /**
     * Each phase will call this method when any players type an unknown command
     */
    public void cmdUkn() {
        notifyPlayer(new NotValidCmd(NOTVALIDMOVE), turnPlayer);
    }

    /**
     * This is invoked every time a player try to type a command during another
     * player's turn.
     *
     * @param gameCharacter
     *            is the player who typed
     */
    public void turnWait(Player gameCharacter) {
        notifyPlayer(new NotValidCmd("\n Wait your turn!"), gameCharacter);
    }

    /**
     * This is overrided in the UseSectCardPhase
     */
    public void drawSectCard() {
        cmdUkn();
    }

    /**
     * This is overrided in the Noise in your sector/ Noise in any sector cards.
     *
     * @param row
     * 
     * @param col
     * 
     */
    public void noise(String row, String col) {
        notifyPlayer(
                new NotValidCmd(String.format(
                        "%nYou can't use command 'noise %s;%s' now!", row, col)),
                turnPlayer);
    }

    /**
     * This is overrided in the AttackPhase.
     */
    public void attack() {
        cmdUkn();
    }

    /**
     * This is overrided in each phase
     */
    public void end() {
        cmdUkn();
    }

    /**
     * This is overrided in AttackorSkipPhase
     */
    public void skip() {
        cmdUkn();
    }

    /**
     * This is overrided in Light card.
     *
     * @param row
     * 
     * @param col
     * 
     */
    public void light(String row, String col) {
        notifyPlayer(
                new NotValidCmd(String.format(
                        "%nYou can't use command light %s;%s now!", row, col)),
                turnPlayer);
    }

    /**
     * This is overrided in DiscardedItemPhase.
     *
     * @param discardChoice
     *            the discard choice
     */
    public void discard(int discardChoice) {
        notifyPlayer(
                new NotValidCmd(String.format(
                        "%nYou can't discard item card %d now!", discardChoice)),
                turnPlayer);
    }

    protected void resetTurnPlayer() {
        if ("Human".equals(turnPlayer.getPlayerRace())) {
            turnPlayer.speedDown();
        }
    }
}
