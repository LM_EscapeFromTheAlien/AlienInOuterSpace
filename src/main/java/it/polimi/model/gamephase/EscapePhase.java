package it.polimi.model.gamephase;

import it.polimi.common.observer.event.NewPhaseEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.Card;

/**
 * 
 * Checks if the TurnPlayer arrived in an EscapeHatch sector
 */
public class EscapePhase extends GamePhase {

    /**
     * Instantiates a new escape phase.
     *
     * @param gameStatus
     * 
     * @param phaseContext
     * 
     */
    public EscapePhase(GameStatus gameStatus, GamePhaseContext phaseContext) {
        super(gameStatus, phaseContext);
    }

    /**
     * Draws an Escape Hatch card
     */
    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        String name = turnPlayer.getPlayerName().toString();
        String whatIcanDo = "\n" + name + " arrived to a Hatch Sector," + name
                + "is drawing an Escape Hatch card...";
        notifyAll(new NewPhaseEvent(whatIcanDo));
        Card canEscape = gameStatus.getEscapeHatchDeck().draw();
        ((CardPhaseDecorator) canEscape).setCardDecoredPhase(this);
        canEscape.phaseDecorer();
    }
}
