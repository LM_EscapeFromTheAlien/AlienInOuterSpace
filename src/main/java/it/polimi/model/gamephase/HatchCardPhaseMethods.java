package it.polimi.model.gamephase;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.map.SectType;

public abstract class HatchCardPhaseMethods extends CardPhaseDecorator {

    /**
     * An abstract class that contains a method used by Green and Red cards
     */
    protected void tryGameEnd() {
        if (grid.containsValue(SectType.ESCAPE)) {
            gameStatus.turnPassed();
            cardDecoredPhase = setNextPhase(phaseContext.getTurnStart());
            cardDecoredPhase.start();
        } else {
            cardDecoredPhase
                    .notifyAll(new CardEvent(
                            "\nThere aren't more escape Hatch avaible! all Humans who escaped and Aliens wins!"));
            nextPhase = setNextPhase(phaseContext.getEndGame());
            nextPhase.start();
        }

    }
}
