package it.polimi.model.gamephase;

import it.polimi.common.observer.event.CardEvent;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.SectorCard;
import it.polimi.model.decks.Deck;

/**
 * This class is used as a phase but simply controls the draw checking if decks
 * are empty or not.
 *
 *
 */

public class UseSectCardPhase extends GamePhase {

    /**
     * Instantiates a new use sect card phase.
     *
     * @param gameStatus
     *            the game status
     * @param phaseChanger
     *            the phase changer
     */
    public UseSectCardPhase(GameStatus gameStatus, GamePhaseContext phaseChanger) {
        super(gameStatus, phaseChanger);
    }

    /**
     * Checks if Sector deck is empty if yes rebuilds the SectorDeck with the
     * discarded cards. Invokes the SectorCard's effect pretending that is an
     * game phase. Allows player drawing an Item Card after checked if the Draws
     * is possible.
     */
    @Override
    public void start() {
        turnPlayer = gameStatus.getPlayers().peek();
        Deck itemDeck = gameStatus.getItemDeck();
        Deck sectorDeck = gameStatus.getSectorDeck();

        emptyDeck(sectorDeck);
        sectorDeck.addtoDiscarded();

        SectorCard sectCard = (SectorCard) sectorDeck.draw();

        notifyPlayer(
                new CardEvent(String.format("%nYou drew a sector card! %s",
                        sectCard.toString())), turnPlayer);
        notifyAll(new CardEvent(String.format("%n%s drew a sector card!",
                turnPlayer.getPlayerName())));

        if (sectCard.getItem()) {
            drawItemCard(itemDeck);
        }
        handleSectCards(sectCard);
    }

    private void drawItemCard(Deck itemDeck) {
        if (itemDeck.isHeadDeckEmpty()) {
            itemDeck.reBuildDeck();
        }
        if (!itemDeck.isHeadDeckEmpty()) {
            Card itemCard = itemDeck.draw();
            turnPlayer.addCard(itemCard);

            notifyAll(new CardEvent(String.format(
                    "%n%s added a new item card to his hand",
                    turnPlayer.getPlayerName())));

            notifyPlayer(new CardEvent(
                    "\nYou added a new item card to your hand\n"), turnPlayer);

            ((CardPhaseDecorator) itemCard).setCardDecoredPhase(this);
            ((CardPhaseDecorator) itemCard).drawEffetc();
            showCards();
        } else {
            notifyAll(new CardEvent("Item deck is empty"));
        }
    }

    private void emptyDeck(Deck sectorDeck) {
        if (sectorDeck.isHeadDeckEmpty())
            sectorDeck.reBuildDeck();
    }

    private void handleSectCards(Card sectCard) {
        ((CardPhaseDecorator) sectCard).setCardDecoredPhase(this);
        sectCard.phaseDecorer();
    }

}
