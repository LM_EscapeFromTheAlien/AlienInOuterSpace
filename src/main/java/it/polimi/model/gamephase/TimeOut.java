package it.polimi.model.gamephase;

import java.util.TimerTask;

public class TimeOut extends TimerTask {

    private final GamePhaseContext phaseChanger;

    /**
     * Instantiates a new time out.
     *
     * @param phaseChanger
     *            the phase changer
     */
    public TimeOut(GamePhaseContext phaseChanger) {
        this.phaseChanger = phaseChanger;
    }

    /**
     * When the time is over starts TurnEndPhase
     */

    @Override
    public void run() {
        phaseChanger.getTurnEnd().end();
    }

}
