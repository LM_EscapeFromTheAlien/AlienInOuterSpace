package it.polimi.model.player;

import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

import java.util.Map;

public class EquivalentPlayer extends Player {

    /**
     * 
     *
     * @param playerName
     *            the player name
     */
    public EquivalentPlayer(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public Boolean canMove(Map<SectorCoord, SectType> grid, SectorCoord from,
            SectorCoord to) {
        return null;
    }

    @Override
    public String getPlayerRace() {
        return null;
    }

    @Override
    public void speedUp() {
        return;
    }

    @Override
    public void speedDown() {
        return;
    }

    @Override
    public boolean canUseItem() {
        return false;
    }

    @Override
    public boolean canEscape() {
        return false;
    }

    @Override
    public boolean canAttack() {
        return false;
    }

}
