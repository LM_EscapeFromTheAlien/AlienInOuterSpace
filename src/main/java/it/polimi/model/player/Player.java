package it.polimi.model.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import it.polimi.model.cards.Card;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

public abstract class Player {

    private static final int MAXCARDICANHAVE = 3;
    private char col;
    private Integer row;

    /** The player name. */
    protected String playerName;
    private boolean iAmProtected = false;

    /** The player cards hand. */
    protected List<Card> playerHand = new ArrayList<Card>();

    /**
     * 
     *
     * @param grid
     * 
     * @param from
     * 
     * @param to
     * 
     * @return the boolean
     */
    public abstract Boolean canMove(Map<SectorCoord, SectType> grid,
            SectorCoord from, SectorCoord to);

    /**
     * Checks if the player can make a movement.
     *
     * @param grid
     * 
     * @param footSteps
     * 
     * @param myPos
     * 
     * @param searchPos
     * 
     * @return the boolean
     */
    public Boolean canMove(Map<SectorCoord, SectType> grid, int footSteps,
            SectorCoord myPos, SectorCoord searchPos) {

        if (myPos != null && myPos.equals(searchPos))
            return true;

        if (myPos == null || footSteps < 1)
            return false;

        int offset1 = myPos.upperSidesNeighborsOffset(myPos.getCol());
        int offset2 = myPos.lowerSidesNeighborsOffset(myPos.getCol());

        initColAndRow(myPos);

        if (canMove(grid, footSteps - 1, istanceNeighbor(grid, col, row - 1),
                searchPos))
            return true;

        initColAndRow(myPos);

        if (canMove(grid, footSteps - 1,
                istanceNeighbor(grid, col - 1, row + offset1), searchPos))
            return true;

        initColAndRow(myPos);

        if (canMove(grid, footSteps - 1,
                istanceNeighbor(grid, col - 1, row + offset2), searchPos))
            return true;

        initColAndRow(myPos);

        if (canMove(grid, footSteps - 1, istanceNeighbor(grid, col, row + 1),
                searchPos))
            return true;

        initColAndRow(myPos);

        if (canMove(grid, footSteps - 1,
                istanceNeighbor(grid, col + 1, row + offset2), searchPos))
            return true;

        initColAndRow(myPos);

        if (canMove(grid, footSteps - 1,
                istanceNeighbor(grid, col + 1, row + offset1), searchPos))
            return true;

        return false;
    }

    private void initColAndRow(SectorCoord myPos) {
        col = myPos.getCol();
        row = myPos.getRow();
    }

    private SectorCoord istanceNeighbor(Map<SectorCoord, SectType> grid,
            int neighbCol, int neighbRow) {

        SectorCoord tempCoord = new SectorCoord((char) neighbCol, neighbRow);

        if (!sectorExist(grid, tempCoord))
            return null;

        return tempCoord;
    }

    private boolean sectorExist(Map<SectorCoord, SectType> grid,
            Object tempCoord) {
        return grid.containsKey(tempCoord)
                && grid.get(tempCoord) != SectType.ALIEN_SECTOR
                && grid.get(tempCoord) != SectType.HUMAN_SECTOR;
    }

    /**
     * Adds the card to the player's hand.
     *
     * @param card
     * 
     */
    public void addCard(Card card) {
        playerHand.add(card);
    }

    /**
     * Gets the player's race.
     *
     * @return the player race
     */
    public abstract String getPlayerRace();

    /**
     * Gets the player's name.
     *
     * @return the player name
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Gets the player's hand.
     *
     * @return the player hand
     */
    public List<Card> getPlayerHand() {
        return playerHand;
    }

    /**
     * Can use item.
     *
     * @return true, if successful
     */
    public abstract boolean canUseItem();

    /**
     * Can escape.
     *
     * @return true, if successful
     */
    public abstract boolean canEscape();

    /**
     * Can attack.
     *
     * @return true, if successful
     */
    public abstract boolean canAttack();

    /**
     * Hand full.
     *
     * @return true, if successful
     */
    public boolean handFull() {
        return playerHand.size() > MAXCARDICANHAVE;
    }

    public abstract void speedUp();

    public abstract void speedDown();

    /**
     * I am protect.
     *
     * @return true, if successful
     */
    public boolean iAmProtect() {
        return iAmProtected;
    }

    /**
     * Sets player as protected.
     */
    public void setProtected() {
        iAmProtected = true;
    }

    /**
     * set player as Not protected.
     *
     * @return the card
     */
    public Card notProtected() {
        iAmProtected = false;
        for (Card protectionCard : playerHand)
            if ("Protection".equals(protectionCard.toString())) {
                playerHand.remove(protectionCard);
                return protectionCard;
            }
        return null;
    }

    /**
     * Sets the player name.
     *
     * @param name
     *            the new player name
     */
    public void setPlayerName(String name) {
        playerName = name;
    }

    /**
     * Checks if two players are the same by their name
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(playerName).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Player))
            return false;
        if (obj == this)
            return true;
        Player testEquals = (Player) obj;
        return new EqualsBuilder().append(playerName, testEquals.playerName)
                .isEquals();
    }
}
