package it.polimi.model.player;

import java.util.LinkedList;

import java.util.Queue;

public class PlayerInstancer {

    private final Queue<Player> players = new LinkedList<Player>();
    private final String[] humanNames = new String[4];
    private final String[] alienNames = new String[4];

    /**
     * Instantiates a new player instancer.
     *
     * @param numberOfPlayers
     *            the number of players
     */
    public PlayerInstancer(int numberOfPlayers) {

        for (int counter = 0; counter < 4; counter++) {
            alienNames[counter] = "Alien " + counter;
            humanNames[counter] = "Human " + counter;
        }

        int counter;

        for (counter = 0; counter < numberOfPlayers / 2; counter++) {
            players.add(new Alien(alienNames[counter]));
            players.add(new Human(humanNames[counter]));
        }
    }

    /**
     * Gets the player list.
     *
     * @return the player list
     */
    public Queue<Player> getPlayerList() {
        return players;
    }
}
