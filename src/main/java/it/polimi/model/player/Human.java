package it.polimi.model.player;

import java.util.Map;

import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

/**
 * This class represents the Human player
 *
 *
 */
public class Human extends Player {

    private static final String RACE = "Human";
    private int maxFootSteps = 1;

    /**
     * Instantiates a new human.
     *
     * @param playerName
     *            the player name
     */
    public Human(String playerName) {
        this.playerName = playerName;
    }

    /**
     * Checks if the human can move in the specify sector
     */

    @Override
    public Boolean canMove(Map<SectorCoord, SectType> grid, SectorCoord myPos,
            SectorCoord searchPos) {
        return super.canMove(grid, maxFootSteps, myPos, searchPos);
    }

    @Override
    public String getPlayerRace() {
        return RACE;
    }

    /**
     * Gets the maxfootsteps.
     *
     * @return the maxfootsteps
     */
    public int getMaxfootsteps() {
        return maxFootSteps;
    }

    /**
     * Checks if the human can use an item card
     */
    @Override
    public boolean canUseItem() {
        return !playerHand.isEmpty();
    }

    @Override
    public boolean canEscape() {
        return true;
    }

    @Override
    public boolean canAttack() {
        return false;
    }

    /**
     * Changes the number of steps to 2
     */

    @Override
    public void speedUp() {
        maxFootSteps = 2;
    }

    /**
     * Changes the number of steps to 1
     */

    @Override
    public void speedDown() {
        maxFootSteps = 1;
    }
}
