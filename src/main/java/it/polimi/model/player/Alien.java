package it.polimi.model.player;

import java.util.Map;

import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

/**
 * This class is used for create Alien players
 *
 *
 */
public class Alien extends Player {

    private static final String RACE = "Alien";
    private int maxFootSteps = 2;

    /**
     * Instantiates a new alien.
     *
     * @param playerName
     *            the Player's name
     */
    public Alien(String playerName) {
        this.playerName = playerName;
    }

    /**
     * Checks if the Alien can move in the specify sector
     */
    @Override
    public Boolean canMove(Map<SectorCoord, SectType> grid, SectorCoord myPos,
            SectorCoord searchPos) {
        return super.canMove(grid, maxFootSteps, myPos, searchPos)
                && !grid.get(searchPos).equals(SectType.ESCAPE);
    }

    /**
     *
     */
    @Override
    public String getPlayerRace() {
        return RACE;
    }

    /**
     * Gets the max foot steps.
     *
     * @return the max foot steps
     */
    public int getMaxFootSteps() {
        return maxFootSteps;
    }

    /** 
     * 
     */
    @Override
    public boolean canUseItem() {
        return false;
    }

    @Override
    public boolean canEscape() {
        return false;
    }

    @Override
    public boolean canAttack() {
        return true;
    }

    @Override
    public void speedDown() {
        return;
    }

    /**
     * Changes the number of steps to 3
     */
    @Override
    public void speedUp() {
        maxFootSteps = 3;
    }
}
