package it.polimi.model;

import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.InitEvent;
import it.polimi.model.decks.*;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Player;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;

public class GameStatus {

    /** The players position. */
    protected Map<Player, SectorCoord> playersPosition = new HashMap<Player, SectorCoord>();

    /** The players. */
    protected Queue<Player> players = new LinkedList<Player>();

    /** The grid instance. */
    protected Map<SectorCoord, SectType> gridInstance;

    /** The sector deck. */
    protected Deck sectorDeck;

    /** The item deck. */
    protected Deck itemDeck;

    /** The escape hatch deck. */
    protected Deck escapeHatchDeck;

    /** The phase context. */
    protected GamePhaseContext phaseContext;

    /** The note center. */
    protected NotificationCenter noteCenter;
    private int turnCounter = 0;

    /** The map. */
    protected String map;
    private Timer timer;
    /**
     * Instantiates a new game status.
     */
    public GameStatus() {
        noteCenter = new NotificationCenter();
    }

    /**
     * Adds the player.
     *
     * @param player
     *            the player
     */
    public void addPlayer(Player player) {
        players.add(player);
        Event playerAdd = new InitEvent("player " + player.getPlayerName()
                + " added");
        ((InitEvent) playerAdd).setPlayerName(player.getPlayerName());
        noteCenter.notifyPlayer(playerAdd, player);
    }

    /**
     * Postion players.
     */
    protected void postionPlayers() {
        for (Player playerToPut : players)
            startPosSelector(playerToPut);
    }

    private void startPosSelector(Player playerToPut) {
        SectorCoord humanStart = null;
        for (SectorCoord start : gridInstance.keySet())
            if (gridInstance.get(start).equals(SectType.HUMAN_SECTOR))
                humanStart = start;
        SectorCoord alienStart = null;
        for (SectorCoord start : gridInstance.keySet())
            if (gridInstance.get(start).equals(SectType.ALIEN_SECTOR))
                alienStart = start;
        if ("Human".equals(playerToPut.getPlayerRace()) && humanStart != null)
            playersPosition.put(playerToPut, humanStart);
        else if ("Alien".equals(playerToPut.getPlayerRace())
                && alienStart != null)
            playersPosition.put(playerToPut, alienStart);
    }

    /**
     * Gets the players position.
     *
     * @return the players position
     */
    public Map<Player, SectorCoord> getPlayersPosition() {
        return playersPosition;
    }

    /**
     * Gets the players.
     *
     * @return the players
     */
    public Queue<Player> getPlayers() {
        return players;
    }

    /**
     * Gets the grid instance.
     *
     * @return the grid instance
     */
    public Map<SectorCoord, SectType> getGridInstance() {
        return gridInstance;
    }

    /**
     * Gets the sector deck.
     *
     * @return the sector deck
     */
    public Deck getSectorDeck() {
        return sectorDeck;
    }

    /**
     * Gets the item deck.
     *
     * @return the item deck
     */
    public Deck getItemDeck() {
        return itemDeck;
    }

    /**
     * Gets the escape hatch deck.
     *
     * @return the escape hatch deck
     */
    public Deck getEscapeHatchDeck() {
        return escapeHatchDeck;
    }

    /**
     * Gets the phase changer.
     *
     * @return the phase changer
     */
    public GamePhaseContext getPhaseChanger() {
        return phaseContext;
    }

    /**
     * Gets the note center.
     *
     * @return the note center
     */
    public NotificationCenter getNoteCenter() {
        return noteCenter;
    }

    /**
     * Gets the map.
     *
     * @return the map
     */
    public String getMap() {
        return map;
    }

    /**
     * Turn passed.
     */
    public void turnPassed() {
        turnCounter++;
    }

    /**
     * Gets the turn counter.
     *
     * @return the turn counter
     */
    public int getTurnCounter() {
        return turnCounter;
    }

    /**
     * Gets the timer.
     *
     * @return the timer
     */
    public Timer getTimer() {
        return timer;
    }

    /**
     * Sets the timer.
     *
     * @param timer
     *            the new timer
     */
    public void setTimer(Timer timer) {
        this.timer = timer;
    }
}
