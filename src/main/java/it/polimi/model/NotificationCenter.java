package it.polimi.model;

import java.util.HashSet;
import java.util.Set;

import it.polimi.common.observer.Observable;
import it.polimi.common.observer.Observer;
import it.polimi.common.observer.event.Event;
import it.polimi.model.player.Player;

public class NotificationCenter implements Observable {

    private final Set<Observer> observers;

    /**
     * Instantiates a new notification center.
     */
    public NotificationCenter() {
        this.observers = new HashSet<Observer>();
    }

    @Override
    public void register(Observer obs) {
        observers.add(obs);
    }

    /**
     * Notify all.
     *
     * @param event
     *            the event
     */
    public final void notifyAll(Event event) {
        for (Observer obs : observers) {
            obs.notifyAll(event);
        }
    }

    /**
     * Notify player.
     *
     * @param event
     *            the event
     * @param player
     *            the player
     */
    public final void notifyPlayer(Event event, Player player) {
        for (Observer obs : observers) {
            obs.notifyPlayer(event, player);
        }
    }

    /**
     * Notify init game.
     *
     * @param initEvent
     *            the init event
     */
    public void notifyInitGame(Event initEvent) {
        for (Observer obs : observers) {
            obs.notifyAll(initEvent);
        }
    }
}
