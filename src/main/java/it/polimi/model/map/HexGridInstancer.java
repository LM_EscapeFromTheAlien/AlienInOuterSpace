package it.polimi.model.map;

import java.io.*;
import java.nio.file.InvalidPathException;
import java.util.*;

public class HexGridInstancer {

    private String mapChoice;
    private final Map<SectorCoord, SectType> gridInstance = new HashMap<SectorCoord, SectType>();

    /**
     * Instantiates a new hex grid instancer.
     *
     * @param mapChoice
     *            the map choice
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public HexGridInstancer(String mapChoice) throws FileNotFoundException {
        this.mapChoice = mapChoice;

        Scanner readMap = mapFileOpener();
        mapFileReader(readMap);
    }

    private Scanner mapFileOpener() throws FileNotFoundException {
        Scanner readMap;

        try {
            File map = new File("src/main/resources/it/polimi/file/map/"
                    + mapChoice + ".txt");
            readMap = new Scanner(new File(map.getPath()));
        } catch (InvalidPathException er) {
            throw er;
        } catch (FileNotFoundException er) {
            throw er;
        }
        return readMap;
    }

    private void mapFileReader(Scanner readMap) {
        String type = new String();
        while (readMap.hasNext()) {
            SectorCoord coord = new SectorCoord(readMap.next().charAt(0),
                    readMap.nextInt());
            type = readMap.nextLine();
            type = type.trim();
            gridInstance.put(coord, SectType.valueOf(type));
        }
    }

    /**
     * Gets the grid instance.
     *
     * @return the grid instance
     */
    public Map<SectorCoord, SectType> getGridInstance() {
        return gridInstance;
    }

}
