package it.polimi.model.map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class SectorCoord {
    private Character col;
    private Integer row;

    /**
     * Instantiates a new sector coord.
     *
     * @param col
     *            the col
     * @param row
     *            the row
     */
    public SectorCoord(Character col, Integer row) {
        this.col = col;
        this.row = row;
    }

    /**
     * Gets the col.
     *
     * @return the col
     */
    public Character getCol() {
        return col;
    }

    /**
     * Gets the row.
     *
     * @return the row
     */
    public Integer getRow() {
        return row;
    }

    /**
     * Upper sides neighbors offset.
     *
     * @param col
     *            the col
     * @return the int
     */
    public int upperSidesNeighborsOffset(Character col) {
        if (col % 2 == 0)
            return 0;
        else
            return -1;
    }

    /**
     * Lower sides neighbors offset.
     *
     * @param col
     *            the col
     * @return the int
     */
    public int lowerSidesNeighborsOffset(Character col) {
        if (col % 2 == 0)
            return 1;
        else
            return 0;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(col).append(row).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SectorCoord))
            return false;
        if (obj == this)
            return true;
        SectorCoord testEquals = (SectorCoord) obj;
        return new EqualsBuilder().append(col, testEquals.col)
                .append(row, testEquals.row).isEquals();
    }

    @Override
    public String toString() {
        return col + ";" + row;
    }

    /**
     * Can i make noise here.
     *
     * @param sectType
     *            the sect type
     * @return true, if successful
     */
    public boolean canIMakeNoiseHere(SectType sectType) {
        return sectType != SectType.ALIEN_SECTOR
                && sectType != SectType.HUMAN_SECTOR
                && sectType != SectType.SECURE && sectType != SectType.ESCAPE;
    }
}
