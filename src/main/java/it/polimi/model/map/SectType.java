package it.polimi.model.map;

public enum SectType {

    /** The secure. */
    SECURE,
    /** The dangerous. */
    DANGEROUS,
    /** The escape. */
    ESCAPE,
    /** The human sector. */
    HUMAN_SECTOR,
    /** The alien sector. */
    ALIEN_SECTOR;
}
