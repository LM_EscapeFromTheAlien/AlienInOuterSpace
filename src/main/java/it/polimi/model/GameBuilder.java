package it.polimi.model;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.GameEnd;
import it.polimi.common.observer.event.InitEvent;
import it.polimi.model.decks.*;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.map.HexGridInstancer;

public class GameBuilder {

    private boolean mapFind = true;
    private Event start;
    private static final Logger LOGGER = Logger.getLogger(GameBuilder.class
            .getName());

    /**
     * Model build.
     *
     * @param gameStatus
     *            the game status
     * @param map
     *            the map
     */
    public void modelBuild(GameStatus gameStatus, String map) {
        mapBuilder(map, gameStatus.noteCenter, gameStatus);
        deckBuilder(gameStatus);
        if (mapFind)
            gameStatus.postionPlayers();
        gameStatus.phaseContext = new GamePhaseContext(gameStatus);
        if (!mapFind) {
            gameStatus.phaseContext.endGame();
        } else {
            start = new InitEvent(
                    "\nA new game is started.The first player has choosen: "
                            + gameStatus.getMap() + " map");
            ((InitEvent) start).setMap(map);
        }
        gameStatus.noteCenter.notifyInitGame(start);
    }

    private void mapBuilder(String map, NotificationCenter noteCenter,
            GameStatus gameStatus) {
        try {
            gameStatus.gridInstance = new HexGridInstancer(map)
                    .getGridInstance();
        } catch (FileNotFoundException notFound) {
            LOGGER.log(Level.INFO, "map not found, model err", notFound);
            noteCenter.notifyAll(new GameEnd(
                    "\nthe file with the map was not found"));
            mapFind = false;
        }
        gameStatus.map = map;
    }

    private void deckBuilder(GameStatus gameStatus) {
        gameStatus.sectorDeck = new SectorDeck();
        gameStatus.itemDeck = new ItemDeck();
        gameStatus.escapeHatchDeck = new EscapeHatchDeck();
    }
}
