package it.polimi.common.rmi;

import it.polimi.common.observer.event.Event;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface EventWriterInterface extends Remote {

    /**
     * Notify all.
     *
     * @param event
     * @throws RemoteException
     */
    public void notifyAll(Event event) throws RemoteException;

    /**
     * Dispatch message.
     *
     * @param string
     * 
     * @throws RemoteException
     */
    public void dispatchMessage(String string) throws RemoteException;

    /**
     * Gets the player name.
     *
     * @return the player's name
     * @throws RemoteException
     *             the remote exception
     */
    public String getPlayerName() throws RemoteException;

}
