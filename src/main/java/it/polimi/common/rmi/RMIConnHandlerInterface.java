package it.polimi.common.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIConnHandlerInterface extends Remote {

    public void newRMIPlayer(EventWriterInterface eventWriter)
            throws RemoteException;
}
