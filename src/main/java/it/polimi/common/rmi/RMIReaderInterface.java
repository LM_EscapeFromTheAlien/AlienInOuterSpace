package it.polimi.common.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIReaderInterface extends Remote {

    /**
     * Read commands.
     *
     * @param cmd
     * 
     * @param playerName
     *            the player's name
     * @throws RemoteException
     *             the remote exception
     */
    public void readCmd(String cmd, String playerName) throws RemoteException;
}
