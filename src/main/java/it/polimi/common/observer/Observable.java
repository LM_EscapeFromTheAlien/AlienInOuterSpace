package it.polimi.common.observer;

public interface Observable {

    /**
     * Register.
     *
     * @param obs
     */
    void register(Observer obs);
}
