package it.polimi.common.observer;

import it.polimi.common.observer.event.Event;
import it.polimi.model.player.Player;

public interface Observer {

    /**
     * Notify all.
     *
     * @param event
     */
    void notifyAll(Event event);

    /**
     * Notify player.
     *
     * @param event
     * 
     * @param player
     */
    void notifyPlayer(Event event, Player player);

}
