package it.polimi.common.observer.event;

public class TurnStartEvent extends Event {
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new turn start event.
     */
    public TurnStartEvent() {
        myType = EventType.TURNSTARTEVENT;
    }

    /**
     * Instantiates a new turn start event.
     *
     * @param msg
     *            the msg
     */
    public TurnStartEvent(String msg) {
        myType = EventType.TURNSTARTEVENT;
        this.msg = msg;
    }
}
