package it.polimi.common.observer.event;

public class InitEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private String map = null;
    private String playerName;

    /**
     * Instantiates a new inits the event.
     */
    public InitEvent() {
        myType = EventType.INITEVENT;
    }

    /**
     * Instantiates a new inits the event.
     *
     * @param msg
     *            the msg
     */
    public InitEvent(String msg) {
        myType = EventType.INITEVENT;
        this.msg = msg;
    }

    public void setMap(String map) {
        this.map = map;
    }

    @Override
    public String getMap() {
        return map;
    }

    @Override
    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
