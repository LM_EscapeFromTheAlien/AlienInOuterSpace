package it.polimi.common.observer.event;

public class PlayerLoseEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new player lose event.
     */
    public PlayerLoseEvent() {
        myType = EventType.PLAYERLOSEEVENT;
    }

    /**
     * Instantiates a new player lose event.
     *
     * @param msg
     * 
     */
    public PlayerLoseEvent(String msg) {
        myType = EventType.PLAYERLOSEEVENT;
        this.msg = msg;
    }
}
