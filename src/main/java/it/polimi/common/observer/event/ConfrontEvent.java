package it.polimi.common.observer.event;

public class ConfrontEvent extends Event {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new confront event.
     */
    public ConfrontEvent() {
    }

    /**
     * Sets the event type.
     *
     * @param type
     *            the new event type
     */
    public void setEventType(EventType type) {
        myType = type;
    }
}
