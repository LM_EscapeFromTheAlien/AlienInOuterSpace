package it.polimi.common.observer.event;

public class CardEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new card event.
     */
    public CardEvent() {
        myType = EventType.CARDEVENT;
    }

    /**
     * Instantiates a new card event.
     *
     * @param msg
     * 
     */
    public CardEvent(String msg) {
        myType = EventType.CARDEVENT;
        this.msg = msg;
    }
}
