package it.polimi.common.observer.event;

import it.polimi.common.observer.event.Event;

public class BadPositionEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new bad position event.
     */
    public BadPositionEvent() {
        myType = EventType.BADPOSITIONEVENT;
    }

    /**
     * Instantiates a new bad position event.
     *
     * @param msg
     * 
     */
    public BadPositionEvent(String msg) {
        this.msg = msg;
        myType = EventType.BADPOSITIONEVENT;
    }
}
