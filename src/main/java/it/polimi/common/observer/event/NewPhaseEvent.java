package it.polimi.common.observer.event;

public class NewPhaseEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new new phase event.
     */
    public NewPhaseEvent() {
        myType = EventType.NEWPHASEEVENT;
    }

    /**
     * Instantiates a new new phase event.
     *
     * @param msg
     *            the msg
     */
    public NewPhaseEvent(String msg) {
        myType = EventType.NEWPHASEEVENT;
        this.msg = msg;
    }
}
