package it.polimi.common.observer.event;

public class NotValidCmd extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new not valid cmd.
     */
    public NotValidCmd() {
        myType = EventType.NOTVALIDCMD;
    }

    /**
     * Instantiates a new not valid cmd.
     *
     * @param msg
     * 
     */
    public NotValidCmd(String msg) {
        myType = EventType.NOTVALIDCMD;
        this.msg = msg;
    }
}
