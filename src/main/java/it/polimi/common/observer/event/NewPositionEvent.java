package it.polimi.common.observer.event;

public class NewPositionEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new new position event.
     */
    public NewPositionEvent() {
        myType = EventType.NEWPOSITIONEVENT;
    }

    /**
     * Instantiates a new new position event.
     *
     * @param msg
     * 
     */
    public NewPositionEvent(String msg) {
        this.msg = msg;
        myType = EventType.NEWPOSITIONEVENT;
    }
}
