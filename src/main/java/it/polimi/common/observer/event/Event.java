package it.polimi.common.observer.event;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class Event implements Serializable {

    private String map = null;
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /** The msg. */
    protected String msg;

    /** The my type. */
    protected EventType myType;

    public String getMap() {
        return map;
    }

    /**
     * Gets the msg.
     *
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Gets the who iam.
     *
     * @return the who iam
     */
    public EventType getWhoIam() {
        return myType;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(myType).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Event))
            return false;
        if (obj == this)
            return true;
        Event testEquals = (Event) obj;
        return new EqualsBuilder().append(myType, testEquals.myType).isEquals();
    }

    public String getPlayerName() {
        return null;
    }

}
