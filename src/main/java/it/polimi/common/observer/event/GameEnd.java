package it.polimi.common.observer.event;

public class GameEnd extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new game end.
     */
    public GameEnd() {
        myType = EventType.GAMEEND;
    }

    /**
     * Instantiates a new game end.
     *
     * @param msg
     *            the msg
     */
    public GameEnd(String msg) {
        this.msg = msg;
        myType = EventType.GAMEEND;
    }
}
