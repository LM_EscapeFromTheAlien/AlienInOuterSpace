package it.polimi.common.observer.event;

public class AttackEvent extends Event {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new attack event.
     */
    public AttackEvent() {
        myType = EventType.ATTACKEVENT;
    }

    /**
     * Instantiates a new attack event.
     *
     * @param msg
     *
     */
    public AttackEvent(String msg) {
        this.msg = msg;
        myType = EventType.ATTACKEVENT;
    }
}
