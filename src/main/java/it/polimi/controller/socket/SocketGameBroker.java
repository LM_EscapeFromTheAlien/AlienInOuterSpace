package it.polimi.controller.socket;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.observer.Observer;
import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.EventType;
import it.polimi.common.observer.event.InitEvent;
import it.polimi.controller.Controller;
import it.polimi.model.GameStatus;
import it.polimi.model.player.Player;

/**
 * This class manage the delivering of the event raised by the model, to the
 * player.
 *
 *
 */

public class SocketGameBroker extends Thread implements Observer {

    private GameStatus gameStatus;
    private Controller controller;
    private final List<SocketPlayerConnection> players;
    private static final Logger LOGGER = Logger
            .getLogger(SocketGameBroker.class.getName());
    private final ExecutorService playersExec = Executors.newCachedThreadPool();

    /**
     * Instantiates a new socket game broker.
     *
     * @param gameStatus
     *            the game status
     * @param controller
     *            the controller
     * @param players
     *            the players
     */
    public SocketGameBroker(GameStatus gameStatus, Controller controller,
            List<SocketPlayerConnection> players) {
        this.players = players;
        this.gameStatus = gameStatus;
        this.controller = controller;
        gameStatus.getNoteCenter().register(this);
    }

    @Override
    public void run() {
        for (SocketPlayerConnection player : players) {
            player.setPlayerName(gameStatus);
            playersExec.execute(player);
        }
        try {
            notifyPlayer(
                    new InitEvent(
                            "\nA new Game is starting, choose a map between Galilei, Galvani or Fermi"),
                    gameStatus.getPlayers().peek());
        } catch (NoSuchElementException noMap) {
            LOGGER.log(Level.INFO, "map not found", noMap);
            killAllPlayers();
            close();
        }
    }

    protected void close() {
        try {
            SocketGameBroker.sleep((long) 20 * 1000);
        } catch (InterruptedException err) {
            LOGGER.log(Level.INFO, "cannot wait", err);
        } finally {
            killAllPlayers();
            gameStatus = null;
            controller = null;
        }
    }

    private void killAllPlayers() {
        for (SocketPlayerConnection player : players)
            player.close();
    }

    @Override
    public void notifyAll(Event event) {
        if (event.getWhoIam().equals(EventType.TURNSTARTEVENT))
            controller.setTurnPlayer(gameStatus.getPlayers().peek());
        if (!players.isEmpty()) {
            for (SocketPlayerConnection ply : players) {
                ply.dispatchEvent(event);
            }
        } else {
            LOGGER.info("no players in game!");
            close();
        }
    }

    @Override
    public void notifyPlayer(Event event, Player playerToNote) {
        if (!players.isEmpty()) {
            LOGGER.info("notifying event");
            for (SocketPlayerConnection ply : players) {
                ply.dispatchEvent(event, playerToNote);
            }
        } else {
            LOGGER.info("no players in game!");
        }
    }
}
