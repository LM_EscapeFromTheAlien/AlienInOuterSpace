package it.polimi.controller.socket;

public class SocketServerMain {

    private SocketServerMain() {

    }

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     * @throws Exception
     *             the exception
     */
    public static void main(String[] args) throws Exception {
        SocketServer server;
        server = new SocketServer();
        server.start();
    }
}
