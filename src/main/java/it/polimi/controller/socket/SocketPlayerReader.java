package it.polimi.controller.socket;

import it.polimi.controller.Controller;
import it.polimi.model.player.Player;

import java.io.IOException;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketPlayerReader extends Thread {

    private Controller controller;
    private Scanner inCmd;
    private Player gameCharacter;
    private Socket socket;
    private static final Logger LOGGER = Logger
            .getLogger(SocketPlayerReader.class.getName());
    private boolean conUp = true;

    /**
     * Instantiates a new socket player reader that handle the reception of
     * client player command.
     *
     * @param socket
     *            the socket
     * @param controller
     *            the controller
     * @param gameCharacter
     *            the game character
     */
    public SocketPlayerReader(Socket socket, Controller controller,
            Player gameCharacter) {
        this.socket = socket;
        this.gameCharacter = gameCharacter;
        this.controller = controller;
        try {
            inCmd = new Scanner(socket.getInputStream());
        } catch (IOException err) {
            LOGGER.log(Level.INFO, "cannot open input reader for player"
                    + gameCharacter.getPlayerName(), err);
            close();
        }
    }

    @Override
    public void run() {
        while (conUp) {
            read();
        }
    }

    private void read() {
        String cmd = "";
        try {
            cmd = inCmd.nextLine();
        } catch (NoSuchElementException clientDown) {
            LOGGER.log(Level.INFO, "client has gone down", clientDown);
            close();
        }
        LOGGER.info("cmd:" + cmd);
        if ("exit".equals(cmd)) {
            close();
        } else
            controller.reading(cmd, gameCharacter);
    }

    public synchronized void close() {
        conUp = false;
        controller.removeMeFromGame(gameCharacter);
        try {
            inCmd.close();
            socket.close();
        } catch (Exception err) {
            LOGGER.log(Level.INFO, "closing socket err", err);
            socket = null;
        }
    }

    /**
     * method used only to Read player name.
     *
     * @return the string
     */
    public String readName() {
        return inCmd.nextLine();
    }
}
