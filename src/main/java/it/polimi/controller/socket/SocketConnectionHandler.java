package it.polimi.controller.socket;

import it.polimi.controller.ActualController;
import it.polimi.controller.Controller;
import it.polimi.model.GameStatus;
import it.polimi.model.player.Player;
import it.polimi.model.player.PlayerInstancer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketConnectionHandler extends Thread {

    private List<SocketPlayerConnection> players = new ArrayList<SocketPlayerConnection>();
    private static final Logger LOGGER = Logger
            .getLogger(SocketConnectionHandler.class.getName());
    private final ExecutorService gameExec = Executors.newFixedThreadPool(5);
    private ServerSocket server;
    private Controller controller;
    private GameStatus gameStatus;
    private SocketGameBroker broker;
    private Queue<Player> tempPlayers;
    private static final int MAXNUMBOFPLAYERS = 8;
    private static final int TIMEOUT = 1 * 60 * 1000;
    private static final int TIMEROFF = 0;

    /**
     * Instantiates a new socket connection handler.
     *
     * @param server
     *            the server
     * @throws SocketException
     *             the socket exception
     */
    public SocketConnectionHandler(ServerSocket server) throws SocketException {
        this.server = server;
        server.setSoTimeout(TIMEROFF);
    }

    @Override
    public void run() {
        while (true) {
            initGame();
            try {
                while (players.size() < MAXNUMBOFPLAYERS) {
                    newPlayer();
                    server.setSoTimeout(TIMEOUT);
                }
            } catch (SocketTimeoutException tooTimeToStart) {
                LOGGER.log(Level.INFO, tooTimeToStart.getMessage(),
                        tooTimeToStart);
            } catch (SocketException err) {
                LOGGER.log(Level.INFO, "failed to start timer", err);
                killAllPlayer();
            } finally {
                if (players.size() > 1) {
                    gameExec.execute(broker);
                } else {
                    killAllPlayer();
                }
            }
        }
    }

    private void killAllPlayer() {
        for (SocketPlayerConnection player : players)
            player.close();
    }

    private void initGame() {
        try {
            server.setSoTimeout(TIMEROFF);
        } catch (SocketException err) {
            LOGGER.log(Level.INFO, "failed to start timer", err);
            killAllPlayer();
        }
        gameStatus = new GameStatus();
        controller = new ActualController(gameStatus);
        players = new ArrayList<SocketPlayerConnection>();
        broker = new SocketGameBroker(gameStatus, controller, players);
        tempPlayers = new PlayerInstancer(8).getPlayerList();
    }

    private void newPlayer() throws SocketTimeoutException {
        Player newPlayer = tempPlayers.poll();
        SocketPlayerConnection player;
        try {
            player = new SocketPlayerConnection(server.accept(), newPlayer,
                    controller);
            LOGGER.info("Adding new Player");
            players.add(player);
        } catch (IOException tooLate) {
            LOGGER.log(Level.INFO, "No more Players for this game", tooLate);
            throw new SocketTimeoutException();
        }
    }
}
