package it.polimi.controller.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketServer extends Thread {

    private ServerSocket server;
    private static final int PORT = 1337;
    private static final Logger LOGGER = Logger.getLogger(SocketServer.class
            .getName());

    /**
     * Instantiates a new socket server.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public SocketServer() throws IOException {
        waitFirstConnection();
        SocketConnectionHandler connectionHandler = new SocketConnectionHandler(
                server);
        connectionHandler.start();
    }

    private void waitFirstConnection() throws IOException {
        LOGGER.log(Level.INFO, "waiting connection");
        try {
            server = new ServerSocket(PORT);
        } catch (IOException ioEx) {
            LOGGER.log(Level.INFO, ioEx.getMessage(), ioEx);
            close();
        }
    }

    private void close() throws IOException {
        server.close();
    }
}
