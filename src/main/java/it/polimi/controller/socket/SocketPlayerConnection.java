package it.polimi.controller.socket;

import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.GameEnd;
import it.polimi.common.observer.event.InitEvent;
import it.polimi.controller.Controller;
import it.polimi.model.GameStatus;
import it.polimi.model.player.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketPlayerConnection extends Thread {

    private ObjectOutputStream eventSender;
    private SocketPlayerReader myReader;
    private Queue<Event> buffer;
    private static final Logger LOGGER = Logger
            .getLogger(SocketPlayerConnection.class.getName());
    private Player gameCharacter;
    private Socket socket;
    private boolean conUp = true;

    /**
     * Instantiates a new socket player connection. the class get the
     * connection, and starts a new dedicated thread that handle it for the
     * player.
     * 
     * @param socket
     *            the socket
     * @param gameCharacter
     *            the game character
     * @param controller
     *            the controller
     */
    public SocketPlayerConnection(Socket socket, Player gameCharacter,
            Controller controller) {
        this.socket = socket;
        try {
            eventSender = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException err) {
            LOGGER.log(Level.INFO, "no Players Found!", err);
        }
        buffer = new ConcurrentLinkedQueue<Event>();
        this.gameCharacter = gameCharacter;
        myReader = new SocketPlayerReader(socket, controller, gameCharacter);
    }

    @Override
    public void run() {
        myReader.start();
        while (conUp) {
            Event event = buffer.poll();
            if (event != null)
                send(event);
            else {
                try {
                    synchronized (buffer) {
                        buffer.wait();
                    }
                } catch (InterruptedException err) {
                    LOGGER.log(Level.INFO, "event buffer error!", err);
                    close();
                }
            }
        }
    }

    /**
     * Dispatch event to all player
     *
     * @param event
     *            the event
     */
    public void dispatchEvent(Event event) {
        buffer.add(event);
        synchronized (buffer) {
            buffer.notify();
        }
    }

    /**
     * Dispatch event to a single player identified by his game character that
     * is gived by the model.
     *
     * @param event
     *            the event
     */
    public void dispatchEvent(Event event, Player playerToNote) {
        if (gameCharacter.equals(playerToNote)) {
            buffer.add(event);
            synchronized (buffer) {
                buffer.notify();
            }
        }
    }

    /**
     * this method physically Send event to the player, and continusly check
     * client connection in the same time
     *
     * @param event
     *            the event
     */
    protected void send(Event event) {
        try {
            eventSender.writeObject(event);
        } catch (IOException clientDown) {
            LOGGER.log(Level.INFO,
                    "IOex! from" + gameCharacter.getPlayerName(), clientDown);
        } catch (NullPointerException clientDown) {
            LOGGER.log(Level.INFO, "Client: " + gameCharacter.getPlayerName()
                    + "has gone down", clientDown);
        }
    }

    /**
     * Sets the player name.
     *
     * @param gameStatus
     *            the new player name
     */
    public void setPlayerName(GameStatus gameStatus) {
        send(new InitEvent("\ninsert your name please: "));
        gameCharacter.setPlayerName(myReader.readName());
        gameStatus.addPlayer(gameCharacter);
    }

    public synchronized void close() {
        conUp = false;
        send(new GameEnd("\nthe server closed the game!"));
        try {
            socket.close();
        } catch (Exception err) {
            LOGGER.log(Level.INFO,
                    "Cannot close socket of: " + gameCharacter.getPlayerName(),
                    err);
        } finally {
            eventSender = null;
            socket = null;
        }
    }
}
