package it.polimi.controller;

import it.polimi.common.observer.event.GameEnd;
import it.polimi.model.GameStatus;
import it.polimi.model.gamephase.GamePhaseContext;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InitGameTimeOut extends TimerTask {

    private GameStatus gameStatus;
    private static final Logger LOGGER = Logger.getLogger(InitGameTimeOut.class
            .getName());

    /**
     * Instantiates a new inits the game time out.
     *
     * @param gameStatus
     *            the game status
     */
    public InitGameTimeOut(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "The new game failed to start, TIMEOUT REACHED");

        gameStatus.getNoteCenter().notifyAll(
                new GameEnd("\ntoo time to start the game!"));
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getEndGame());
        phaseContext.getGamePhase().start();
    }

}
