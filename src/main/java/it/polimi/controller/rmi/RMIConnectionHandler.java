package it.polimi.controller.rmi;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.observer.event.InitEvent;
import it.polimi.common.rmi.EventWriterInterface;
import it.polimi.common.rmi.RMIConnHandlerInterface;
import it.polimi.controller.ActualController;
import it.polimi.controller.Controller;
import it.polimi.model.GameStatus;

public class RMIConnectionHandler implements RMIConnHandlerInterface {

    private GameStatus gameStatus;
    private Controller controller;
    private RMIGameBroker gameBroker;
    private static final Logger LOGGER = Logger
            .getLogger(RMIConnectionHandler.class.getName());
    protected Timer timer;
    protected int gameFill = 0;
    private Registry registry;
    private static final int MAXNUMBOFPLAYERS = 8;
    private static final long TIMEOUT = (long) 60 * 1000;

    /**
     * Instantiates a new RMI connection handler.
     *
     * @param registry
     *            the registry
     */
    public RMIConnectionHandler(Registry registry) {
        this.registry = registry;
        initGame();
    }

    @Override
    public synchronized void newRMIPlayer(EventWriterInterface writer)
            throws RemoteException {
        stopTimer();
        LOGGER.log(Level.INFO,
                "I'm waiting to add a new player:" + writer.getPlayerName());
        gameBroker.addPlayer(writer);
        startTimer();
        gameFill++;
        if (gameFill == MAXNUMBOFPLAYERS)
            start();
    }

    protected void start() {
        stopTimer();
        LOGGER.log(Level.INFO, "Game full");
        gameBroker
                .notifyPlayer(
                        new InitEvent(
                                "\nA new Game is starting,  choose a map between Galilei, Galvani or Fermi"),
                        gameStatus.getPlayers().peek());
        initGame();
    }

    private void initGame() {
        gameFill = 0;
        gameStatus = new GameStatus();
        controller = new ActualController(gameStatus);
        startTimer();
        try {
            gameBroker = new RMIGameBroker(controller, gameStatus, registry);
        } catch (RemoteException err) {
            LOGGER.log(Level.SEVERE, err.getMessage(), err);
        }
    }

    protected void startTimer() {
        timer = new Timer();
        TimerTask initTimeOut = new RMITimeOut(this);
        timer.schedule(initTimeOut, TIMEOUT);
    }

    protected void stopTimer() {
        timer.cancel();
        timer.purge();
    }

}
