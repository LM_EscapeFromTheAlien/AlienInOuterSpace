package it.polimi.controller.rmi;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.observer.Observer;
import it.polimi.common.observer.event.Event;
import it.polimi.common.observer.event.EventType;
import it.polimi.common.rmi.EventWriterInterface;
import it.polimi.common.rmi.RMIReaderInterface;
import it.polimi.controller.Controller;
import it.polimi.model.GameStatus;
import it.polimi.model.player.Player;
import it.polimi.model.player.PlayerInstancer;

/**
 * This method sends to any player an event raised by the model.
 *
 *
 */

public class RMIGameBroker implements Observer {

    private static final Logger LOGGER = Logger.getLogger(RMIGameBroker.class
            .getName());
    protected Controller controller;
    private Player player;
    private RMIReader reader;
    private Map<Player, EventWriterInterface> myGameCharacter = new HashMap<Player, EventWriterInterface>();
    private Queue<Player> tempPlayers;
    protected GameStatus gameStatus;

    /**
     * Instantiates a new RMI game broker. the game broker pretend the role of
     * the broker in a publish subscriber design pattern. here the broker can
     * also send message to an individual subscriber: the player!
     *
     * @param controller
     *            the controller
     * @param gameStatus
     *            the game status
     * @param registry
     *            the registry
     * @throws RemoteException
     *             the remote exception
     */
    public RMIGameBroker(Controller controller, GameStatus gameStatus,
            Registry registry) throws RemoteException {
        this.controller = controller;
        this.gameStatus = gameStatus;
        gameStatus.getNoteCenter().register(this);
        reader = new RMIReader(controller, myGameCharacter);
        RMIReaderInterface readerInt = (RMIReaderInterface) UnicastRemoteObject
                .exportObject(reader, 0);
        registry.rebind("Reader", readerInt);
        tempPlayers = new PlayerInstancer(8).getPlayerList();
    }

    /**
     * Adds the connected player to the game, by putting his view in a list.
     *
     * @param writer
     *            the writer
     */
    public void addPlayer(EventWriterInterface writer) {
        player = tempPlayers.poll();
        try {
            player.setPlayerName(writer.getPlayerName());
        } catch (RemoteException nameErr) {
            LOGGER.log(Level.INFO, "name not received", nameErr);
        }
        myGameCharacter.put(player, writer);
        gameStatus.addPlayer(player);
    }

    @Override
    public void notifyAll(Event event) {
        if (myGameCharacter != null && !myGameCharacter.isEmpty()) {
            LOGGER.log(Level.INFO, "publishing message");
            for (Player write : myGameCharacter.keySet()) {
                try {
                    myGameCharacter.get(write).notifyAll(event);
                } catch (RemoteException clientErr) {
                    LOGGER.log(Level.SEVERE, clientErr.getMessage(), clientErr);
                }
            }
        } else {
            LOGGER.log(Level.INFO, "no players");
        }
    }

    @Override
    public void notifyPlayer(Event event, Player playerToNote) {
        if (event.getWhoIam().equals(EventType.TURNSTARTEVENT)) {
            controller.setTurnPlayer(gameStatus.getPlayers().peek());
        }
        if (myGameCharacter.containsKey(playerToNote)) {
            LOGGER.log(Level.INFO, "notifying event");
            EventWriterInterface write = myGameCharacter.get(playerToNote);
            try {
                write.notifyAll(event);
            } catch (RemoteException clientDown) {
                controller.removeMeFromGame(playerToNote);
                LOGGER.log(Level.INFO, "player not found!!", clientDown);
            }
        } else {
            LOGGER.log(Level.INFO, "no players");
        }
    }
}
