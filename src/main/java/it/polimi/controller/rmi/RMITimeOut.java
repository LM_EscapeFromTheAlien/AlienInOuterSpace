package it.polimi.controller.rmi;

import java.util.TimerTask;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class checks if there are almost two player and starts the game after a
 * time out
 *
 *
 */

public class RMITimeOut extends TimerTask {

    private final RMIConnectionHandler handler;
    private static final Logger LOGGER = Logger.getLogger(RMITimeOut.class
            .getName());

    /**
     * Instantiates a new RMI time out.
     *
     * @param handler
     * 
     */
    public RMITimeOut(RMIConnectionHandler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "new game failed to start, TIMEOUT REACHED");
        if (handler.gameFill > 1)
            handler.start();
        else {
            handler.stopTimer();
            handler.startTimer();
        }
    }

}
