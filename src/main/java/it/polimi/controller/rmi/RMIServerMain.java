package it.polimi.controller.rmi;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RMIServerMain {

    private static final Logger LOGGER = Logger.getLogger(RMIServerMain.class
            .getName());

    private RMIServerMain() {
    }

    public static void main(String[] args) {

        LOGGER.log(Level.INFO, "waiting connections");
        RMIServer server = new RMIServer();
        server.start();
    }

}
