package it.polimi.controller.rmi;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.common.rmi.EventWriterInterface;
import it.polimi.common.rmi.RMIReaderInterface;
import it.polimi.controller.Controller;
import it.polimi.model.player.Player;
import it.polimi.model.player.EquivalentPlayer;

public class RMIReader implements RMIReaderInterface {

    private final Controller controller;
    private static final Logger LOGGER = Logger.getLogger(RMIReader.class
            .getName());
    private Map<Player, EventWriterInterface> myGameCharacter;

    /**
     * Instantiates a new RMI reader. the role of the reader is about tacking
     * player commands from his view, and bring them to the controller.
     *
     * @param controller
     *            the controller
     * @param myGameCharacter
     *            the my game character
     */
    public RMIReader(Controller controller,
            Map<Player, EventWriterInterface> myGameCharacter) {
        this.myGameCharacter = myGameCharacter;
        this.controller = controller;
    }

    @Override
    public synchronized void readCmd(String cmd, String playerName)
            throws RemoteException {
        Player playerWhoSendCMD = new EquivalentPlayer(playerName);
        if ("exit".equals(cmd))
            close(playerWhoSendCMD);
        else
            controller.reading(cmd, playerWhoSendCMD);
        LOGGER.log(Level.INFO, "cmd:" + cmd);
    }

    private void close(Player playerWhoSendCmd) {
        myGameCharacter.remove(playerWhoSendCmd);
        controller.removeMeFromGame(playerWhoSendCmd);
    }

}
