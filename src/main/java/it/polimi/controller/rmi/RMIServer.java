package it.polimi.controller.rmi;

import it.polimi.common.rmi.RMIConnHandlerInterface;

import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RMIServer extends Thread {

    private static final Logger LOGGER = Logger.getLogger(RMIServer.class
            .getName());

    private static Registry REGISTRY;
    private static final int PORT = 7777;
    private RMIConnectionHandler server;

    @Override
    public void run() {
        try {
            REGISTRY = LocateRegistry.createRegistry(PORT);
        } catch (RemoteException regNotFound) {
            LOGGER.log(Level.SEVERE, regNotFound.getMessage(), regNotFound);
        }
        try {

            server = new RMIConnectionHandler(REGISTRY);
            RMIConnHandlerInterface serverInt = (RMIConnHandlerInterface) UnicastRemoteObject
                    .exportObject(server, 0);

            REGISTRY.rebind("Server", serverInt);

        } catch (AccessException registryErr) {
            LOGGER.log(Level.SEVERE, registryErr.getMessage(), registryErr);
        } catch (RemoteException remoteObjNotFound) {
            LOGGER.log(Level.SEVERE, remoteObjNotFound.getMessage(),
                    remoteObjNotFound);
        }
    }

}
