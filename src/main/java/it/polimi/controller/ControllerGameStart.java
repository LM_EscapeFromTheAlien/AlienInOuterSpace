package it.polimi.controller;

import java.util.Timer;
import java.util.TimerTask;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Player;

public class ControllerGameStart implements Controller {

    private GameStatus gameStatus;
    private ActualController actualController;
    private GamePhaseContext phaseContext;
    private Timer timer;

    /**
     * Instantiates a new controller game start.
     *
     * @param gameStatus
     *            the game status
     * @param actualController
     *            the actual controller
     */
    public ControllerGameStart(GameStatus gameStatus,
            ActualController actualController) {
        this.actualController = actualController;
        this.gameStatus = gameStatus;
        startTimer();
    }

    @Override
    public void reading(String map, Player playerWhoSendMap) {
        stopTimer();
        if (canIEnterCmd(playerWhoSendMap)) {
            GameBuilder socketModelBuilder = new GameBuilder();
            socketModelBuilder.modelBuild(gameStatus, map);

            phaseContext = gameStatus.getPhaseChanger();
            actualController.setRunningController(gameStatus);
            phaseContext.getGamePhase().start();
        }

    }

    protected boolean canIEnterCmd(Player playerWhoSendCmd) {
        Player turnPlayer = gameStatus.getPlayers().peek();
        return turnPlayer.equals(playerWhoSendCmd);
    }

    @Override
    public void setTurnPlayer(Player turnPlayer) {
        return;
    }

    @Override
    public void removeMeFromGame(Player playerWhoSendCmd) {
        phaseContext.getGamePhase().playerExit(playerWhoSendCmd);
    }

    protected void startTimer() {
        timer = new Timer();
        TimerTask gameInitTimeOut = new InitGameTimeOut(gameStatus);
        timer.schedule(gameInitTimeOut, (long) 5 * 60 * 1000);
    }

    protected void stopTimer() {
        timer.cancel();
        timer.purge();
    }
}
