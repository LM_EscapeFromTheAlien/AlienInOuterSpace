package it.polimi.controller;

import it.polimi.controller.command.CommandBroker;
import it.polimi.model.GameStatus;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Player;

public class ControllerGameRunning implements Controller {

    private GamePhaseContext phaseContext;
    private boolean commandFind = false;
    private Player turnPlayer;
    private CommandBroker cmdBroker = new CommandBroker();

    /**
     * Sets the controller parameter.
     *
     * @param gameStatus
     *            the new controller parameter
     */
    public void setControllerParameter(GameStatus gameStatus) {
        phaseContext = gameStatus.getPhaseChanger();
        turnPlayer = gameStatus.getPlayers().peek();
        cmdBroker.addCommand();
    }

    @Override
    public void reading(String cmd, Player playerWhoSendCmd) {
        if (canIEnterCmd(playerWhoSendCmd)) {

            commandFind = cmdBroker.sendThisCommand(phaseContext, cmd);

            if (!commandFind)
                cmdUnk();
        } else {
            turnWait(playerWhoSendCmd);
        }
    }

    private boolean canIEnterCmd(Player playerWhoSendCmd) {
        return turnPlayer.equals(playerWhoSendCmd);
    }

    private void cmdUnk() {
        phaseContext.getGamePhase().cmdUkn();
    }

    /**
     * Turn wait. if is not my turn, i receive a notify to wait
     *
     * @param gameCharacter
     *            the game character
     */
    public void turnWait(Player gameCharacter) {
        phaseContext.getGamePhase().turnWait(gameCharacter);
    }

    @Override
    public void setTurnPlayer(Player turnPlayer) {
        this.turnPlayer = turnPlayer;
    }

    @Override
    public void removeMeFromGame(Player playerWhoSendCmd) {
        phaseContext.getGamePhase().playerExit(playerWhoSendCmd);
    }

}
