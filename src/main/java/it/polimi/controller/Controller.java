package it.polimi.controller;

import it.polimi.model.player.Player;

public interface Controller {

    /**
     * Reading players command.
     *
     * @param cmd
     *            the cmd
     * @param player
     *            the player
     */
    public void reading(String cmd, Player player);

    /**
     * Sets the turn player.
     *
     * @param turnPlayer
     *            the new turn player
     */
    public void setTurnPlayer(Player turnPlayer);

    /**
     * Removes the player who send command from game.
     *
     * @param playerWhoSendCmd
     *            the player who send cmd
     */
    public void removeMeFromGame(Player playerWhoSendCmd);
}
