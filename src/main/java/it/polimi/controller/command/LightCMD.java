package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LightCMD implements Command {

    private static final Pattern LIGHT = Pattern.compile("light in (.);(..)");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = LIGHT.matcher(cmd);
        if (m.matches()) {
            light(m, phaseContext);
            return true;
        }
        return false;
    }

    private void light(Matcher m, GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().light(m.group(1).toUpperCase(), m.group(2));
    }

}
