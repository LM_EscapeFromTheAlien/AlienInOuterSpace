package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SkipCMD implements Command {

    private static final Pattern SKIP = Pattern.compile("skip");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = SKIP.matcher(cmd);
        if (m.matches()) {
            skip(phaseContext);
            return true;
        }
        return false;
    }

    private void skip(GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().skip();
    }

}
