package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UseItemCMD implements Command {

    private static final Pattern USEITEM = Pattern.compile("use item (.)");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {

        m = USEITEM.matcher(cmd);
        if (m.matches()) {
            useItem(m, phaseContext);
            return true;
        }
        return false;
    }

    /**
     * Use item.
     *
     * @param m
     *            the m
     * @param phaseContext
     *            the phase context
     */
    public void useItem(Matcher m, GamePhaseContext phaseContext) {
        GamePhase gamePhase = phaseContext.getGamePhase();

        gamePhase.useItemCard(Integer.parseInt(m.group(1)));

    }
}
