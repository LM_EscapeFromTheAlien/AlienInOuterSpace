package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AttackCMD implements Command {

    private static final Pattern ATTACK = Pattern.compile("attack");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = ATTACK.matcher(cmd);
        if (m.matches()) {
            attack(phaseContext);
            return true;
        }
        return false;

    }

    /**
     * Acting Attack command, by calling it in the actual game phase.
     *
     * @param phaseContext
     *            the phase context
     */
    public void attack(GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().attack();
    }

}
