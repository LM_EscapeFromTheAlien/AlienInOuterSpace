package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiscardCMD implements Command {

    private static final Pattern DISCARD = Pattern.compile("discard (.)");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = DISCARD.matcher(cmd);
        if (m.matches()) {
            discardItem(m, phaseContext);
            return true;
        }
        return false;
    }

    private void discardItem(Matcher m, GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().discard(Integer.parseInt(m.group(1)));
    }

}
