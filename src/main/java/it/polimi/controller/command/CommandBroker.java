package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.ArrayList;
import java.util.List;

public class CommandBroker {

    private List<Command> commandImplementedList = new ArrayList<Command>();
    private boolean commandFind = false;
    Command discard = new DiscardCMD();
    Command draw = new DrawCMD();
    Command end = new EndCMD();
    Command light = new LightCMD();
    Command atk = new AttackCMD();
    Command move = new MoveCMD();
    Command noise = new NoiseCMD();
    Command showCard = new ShowCardCMD();
    Command skip = new SkipCMD();
    Command useItem = new UseItemCMD();

    /**
     * Adds the command.
     */
    public void addCommand() {
        commandImplementedList.add(discard);
        commandImplementedList.add(draw);
        commandImplementedList.add(end);
        commandImplementedList.add(light);
        commandImplementedList.add(atk);
        commandImplementedList.add(move);
        commandImplementedList.add(noise);
        commandImplementedList.add(showCard);
        commandImplementedList.add(skip);
        commandImplementedList.add(useItem);
    }

    /**
     * Send this command to the command list. the command who match have to act
     * it.
     *
     * @param phaseContext
     *            the phase context
     * @param cmd
     *            the cmd
     * @return true, if successful
     */
    public boolean sendThisCommand(GamePhaseContext phaseContext, String cmd) {
        for (Command tryToTakeCommand : commandImplementedList) {
            commandFind = tryToTakeCommand.excuteMyCmd(phaseContext, cmd);
            if (commandFind) {
                commandFind = false;
                return true;
            }
        }
        return false;
    }
}
