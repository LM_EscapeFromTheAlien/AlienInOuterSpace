package it.polimi.controller.command;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.polimi.model.gamephase.GamePhaseContext;

public class MoveCMD implements Command {

    private static final Pattern MOVE = Pattern.compile("move (.);(..)");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {

        m = MOVE.matcher(cmd);
        if (m.matches()) {
            move(m, phaseContext);
            return true;
        }
        return false;
    }

    /**
     * Acting Move command, by calling it in the actual game phase.
     *
     * @param m
     *            the m
     * @param phaseContext
     *            the phase context
     */
    public void move(Matcher m, GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().move(m.group(1).toUpperCase(), m.group(2));
    }

}
