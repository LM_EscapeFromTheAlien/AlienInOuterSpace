package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

public interface Command {

    /**
     * Excute my cmd.
     *
     * @param phaseContext
     *            the phase context
     * @param cmd
     *            the cmd
     * @return true, if successful
     */
    boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd);
}
