package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NoiseCMD implements Command {

    private static final Pattern NOISE = Pattern.compile("noise in (.);(..)");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = NOISE.matcher(cmd);
        if (m.matches()) {
            noise(m, phaseContext);
            return true;
        }
        return false;
    }

    /**
     * Acting Noise command, by calling it in the actual game phase.
     *
     * @param m
     *            the m
     * @param phaseContext
     *            the phase context
     */
    public void noise(Matcher m, GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().noise(m.group(1).toUpperCase(), m.group(2));

    }

}
