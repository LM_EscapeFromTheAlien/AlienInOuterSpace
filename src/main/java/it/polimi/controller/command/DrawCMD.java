package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DrawCMD implements Command {

    private static final Pattern DRAWSECT = Pattern.compile("draw");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = DRAWSECT.matcher(cmd);
        if (m.matches()) {
            drawSect(phaseContext);
            return true;
        }
        return false;
    }

    /**
     * Acting Draw sect command, by calling it in the actual game phase.
     *
     * @param phaseContext
     *            the phase context
     */
    public void drawSect(GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().drawSectCard();
    }

}
