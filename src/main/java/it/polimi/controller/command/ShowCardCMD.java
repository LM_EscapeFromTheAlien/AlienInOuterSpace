package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShowCardCMD implements Command {

    private static final Pattern SHOWCARD = Pattern.compile("show cards");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {

        m = SHOWCARD.matcher(cmd);
        if (m.matches()) {
            showCards(phaseContext);
            return true;
        }
        return false;
    }

    /**
     * Acting Show Cards command, by calling it in the actual game phase.
     *
     * @param phaseContext
     *            the phase context
     */
    public void showCards(GamePhaseContext phaseContext) {

        phaseContext.getGamePhase().showCards();

    }
}
