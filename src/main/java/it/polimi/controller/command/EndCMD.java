package it.polimi.controller.command;

import it.polimi.model.gamephase.GamePhaseContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EndCMD implements Command {

    private static final Pattern END = Pattern.compile("end");
    private Matcher m;

    @Override
    public boolean excuteMyCmd(GamePhaseContext phaseContext, String cmd) {
        m = END.matcher(cmd);
        if (m.matches()) {
            end(phaseContext);
            return true;
        }
        return false;

    }

    /**
     * Acting end command, by calling it in the actual game phase.
     *
     * @param phaseContext
     *            the phase context
     */
    public void end(GamePhaseContext phaseContext) {
        phaseContext.getGamePhase().end();

    }

}
