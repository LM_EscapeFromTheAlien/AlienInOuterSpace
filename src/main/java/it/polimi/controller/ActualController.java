package it.polimi.controller;

import it.polimi.model.GameStatus;
import it.polimi.model.player.Player;

/**
 * Receives and saves commands from server's readers and sends them to model.
 *
 *
 */

public class ActualController implements Controller {

    private Controller startController;
    private Controller runningController;

    /** The controller. */
    protected Controller controller;

    /**
     * Instantiates a new actual controller.
     *
     * @param gameStatus
     *            the game status
     */
    public ActualController(GameStatus gameStatus) {
        startController = new ControllerGameStart(gameStatus, this);
        runningController = new ControllerGameRunning();
        controller = startController;
    }

    @Override
    public synchronized void reading(String cmd, Player player) {
        controller.reading(cmd, player);
    }

    /**
     * 
     *
     * @param gameStatus
     *            the new running controller
     */
    public void setRunningController(GameStatus gameStatus) {
        controller = runningController;
        ((ControllerGameRunning) controller).setControllerParameter(gameStatus);
    }

    @Override
    public void setTurnPlayer(Player turnPlayer) {
        controller.setTurnPlayer(turnPlayer);
    }

    @Override
    public synchronized void removeMeFromGame(Player playerWhoSendCmd) {
        controller.removeMeFromGame(playerWhoSendCmd);
    }

}