package it.polimi.common.observer.event;

import static org.junit.Assert.*;

import org.junit.Test;

public class EventTest {

    Event initEvent = new InitEvent();
    Event attackEvent = new AttackEvent();
    Event badPositionEvent = new BadPositionEvent();
    Event cardEvent = new CardEvent();
    Event confrontEvent = new ConfrontEvent();
    Event gameEnd = new GameEnd();
    Event newPhaseEvent = new NewPhaseEvent();
    Event newPositionEvent = new NewPositionEvent();
    Event notValidCmd = new NotValidCmd();
    Event playerLoseEvent = new PlayerLoseEvent();
    Event turnStartEvent = new TurnStartEvent();

    @Test
    public void testGetWhoIam() {
        assertEquals(EventType.INITEVENT, initEvent.getWhoIam());
        assertEquals(EventType.ATTACKEVENT, attackEvent.getWhoIam());
        assertEquals(EventType.BADPOSITIONEVENT, badPositionEvent.getWhoIam());
        assertEquals(EventType.CARDEVENT, cardEvent.getWhoIam());
        assertEquals(EventType.GAMEEND, gameEnd.getWhoIam());
        assertEquals(EventType.NEWPHASEEVENT, newPhaseEvent.getWhoIam());
        assertEquals(EventType.NEWPOSITIONEVENT, newPositionEvent.getWhoIam());
        assertEquals(EventType.NOTVALIDCMD, notValidCmd.getWhoIam());
        assertEquals(EventType.PLAYERLOSEEVENT, playerLoseEvent.getWhoIam());
        assertEquals(EventType.TURNSTARTEVENT, turnStartEvent.getWhoIam());
    }

    @Test
    public void testEqualsObject() {
        assertEquals(new InitEvent(), initEvent);
        assertEquals(new AttackEvent(), attackEvent);
        assertEquals(new BadPositionEvent(), badPositionEvent);
        assertEquals(new CardEvent(), cardEvent);
        assertEquals(new GameEnd(), gameEnd);
        assertEquals(new NewPhaseEvent(), newPhaseEvent);
        assertEquals(new NewPositionEvent(), newPositionEvent);
        assertEquals(new NotValidCmd(), notValidCmd);
        assertEquals(new PlayerLoseEvent(), playerLoseEvent);
        assertEquals(new TurnStartEvent(), turnStartEvent);
    }

}
