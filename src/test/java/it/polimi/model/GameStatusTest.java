package it.polimi.model;

import static org.junit.Assert.*;

import java.util.Map;

import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Test;

public class GameStatusTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase attackOrPhaseToTest;
    private GamePhaseContext phaseContext;
    private Map<Player, SectorCoord> playersPos;
    private SectorCoord rightPosTest;

    @Test
    public void positionSelectorTestGalilei() {
        Player human1 = new Human("Human1");
        Player alien1 = new Alien("Alien1");
        Player human2 = new Human("Human2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        gameStatus.addPlayer(human2);

        builder.modelBuild(gameStatus, "galilei");

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getAttackOrSkip());
        attackOrPhaseToTest = phaseContext.getAttackOrSkip();
        attackOrPhaseToTest.start();

        playersPos = gameStatus.getPlayersPosition();

        rightPosTest = playersPos.get(human1);
        assertEquals(new SectorCoord('L', 8), rightPosTest);

        rightPosTest = playersPos.get(alien1);
        assertEquals(new SectorCoord('L', 6), rightPosTest);

        rightPosTest = playersPos.get(human2);
        assertEquals(new SectorCoord('L', 8), rightPosTest);
    }

    @Test
    public void positionSelectorTestGalvani() {
        Player player1 = new Human("Human1");
        Player player2 = new Alien("Alien2");
        Player player3 = new Human("Human3");

        gameStatus.addPlayer(player1);
        gameStatus.addPlayer(player2);
        gameStatus.addPlayer(player3);

        builder.modelBuild(gameStatus, "galvani");

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getAttackOrSkip());
        attackOrPhaseToTest = phaseContext.getAttackOrSkip();
        attackOrPhaseToTest.start();

        playersPos = gameStatus.getPlayersPosition();

        rightPosTest = playersPos.get(player1);
        assertEquals(new SectorCoord('L', 8), rightPosTest);

        rightPosTest = playersPos.get(player2);
        assertEquals(new SectorCoord('L', 6), rightPosTest);

        rightPosTest = playersPos.get(player3);
        assertEquals(new SectorCoord('L', 8), rightPosTest);
    }

    @Test
    public void positionSelectorTestFermi() {
        Player player1 = new Human("Human1");
        Player player2 = new Alien("Alien2");
        Player player3 = new Human("Human3");

        gameStatus.addPlayer(player1);
        gameStatus.addPlayer(player2);
        gameStatus.addPlayer(player3);

        builder.modelBuild(gameStatus, "fermi");

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getAttackOrSkip());
        attackOrPhaseToTest = phaseContext.getAttackOrSkip();
        attackOrPhaseToTest.start();

        playersPos = gameStatus.getPlayersPosition();

        rightPosTest = playersPos.get(player1);
        assertEquals(new SectorCoord('L', 10), rightPosTest);

        rightPosTest = playersPos.get(player2);
        assertEquals(new SectorCoord('L', 9), rightPosTest);

        rightPosTest = playersPos.get(player3);
        assertEquals(new SectorCoord('L', 10), rightPosTest);
    }
}
