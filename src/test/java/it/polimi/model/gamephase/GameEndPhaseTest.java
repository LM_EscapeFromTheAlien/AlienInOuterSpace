package it.polimi.model.gamephase;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import java.util.Map;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

public class GameEndPhaseTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase endGamePhaseToTest;
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Map<Player, SectorCoord> playersPosition;
    private Queue<Player> players;

    @Before
    public void setUpEnviroment() {

        human1 = new Human("Human1");
        alien1 = new Alien("Alien2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);

        builder.modelBuild(gameStatus, "galilei");
        playersPosition = gameStatus.getPlayersPosition();
        players = gameStatus.getPlayers();

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getEndGame());
        endGamePhaseToTest = phaseContext.getEndGame();
    }

    @Test
    public void exitCMDTest() {
        endGamePhaseToTest.playerExit(human1);
        assertFalse(playersPosition.containsKey(human1));
        assertFalse(players.contains(human1));
        assertEquals(phaseContext.getEndGame(), phaseContext.getGamePhase());
    }

    @Test
    public void exitButNotMyTurn() {
        endGamePhaseToTest.playerExit(alien1);
        assertFalse(playersPosition.containsKey(alien1));
        assertFalse(players.contains(alien1));
        assertEquals(phaseContext.getEndGame(), phaseContext.getGamePhase());
    }

}
