package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.Queue;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Teleportation;
import it.polimi.model.cards.sectorcards.NoiseInAnySector;
import it.polimi.model.cards.sectorcards.NoiseInYourSector;
import it.polimi.model.cards.sectorcards.Silence;
import it.polimi.model.decks.Deck;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class UseSectCardPhaseTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Card silenceTestCard;
    private Card noiseInAnyTestCard;
    private Card noiseInYourTestCard;
    private Deck sectDeck;
    private Player human2;
    private Player alien2;
    private Queue<Card> headSectDeck;
    private Queue<Card> discardedSectDeck;
    private GamePhase useSectPhaseToTest;
    private Deck itemDeck;
    private Queue<Card> headItemDeck;
    private Queue<Card> discardedItemDeck;
    private Card teleportationTestItem;

    @Before
    public void setUpEnviroment() {

        silenceTestCard = new Silence();
        noiseInAnyTestCard = new NoiseInAnySector();
        noiseInYourTestCard = new NoiseInYourSector();

        ((NoiseInAnySector) noiseInAnyTestCard).setItem();
        ((NoiseInYourSector) noiseInYourTestCard).setItem();

        teleportationTestItem = new Teleportation();

        human1 = new Human("Human1");
        alien1 = new Alien("Alien1");
        human2 = new Human("Human2");
        alien2 = new Alien("Alien2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        gameStatus.addPlayer(human2);
        gameStatus.addPlayer(alien2);
        builder.modelBuild(gameStatus, "galilei");

        itemDeck = gameStatus.getItemDeck();
        headItemDeck = itemDeck.getHeadDeck();
        discardedItemDeck = itemDeck.getDiscardedDeck();

        sectDeck = gameStatus.getSectorDeck();
        headSectDeck = sectDeck.getHeadDeck();
        discardedSectDeck = sectDeck.getDiscardedDeck();

        headSectDeck.clear();
        headSectDeck.add(noiseInAnyTestCard);
        headSectDeck.add(noiseInYourTestCard);
        headSectDeck.add(silenceTestCard);

        headItemDeck.clear();
        headItemDeck.add(teleportationTestItem);

        assertFalse(headSectDeck.isEmpty());
        assertTrue(discardedSectDeck.isEmpty());

        assertFalse(headItemDeck.isEmpty());
        assertTrue(discardedItemDeck.isEmpty());

        phaseContext = gameStatus.getPhaseChanger();

        useSectPhaseToTest = phaseContext.getUseSectCard();
        phaseContext.setGamePhase(useSectPhaseToTest);
    }

    @Test
    public void noiseInAnytest() {
        useSectPhaseToTest.start();

        assertFalse(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(noiseInAnyTestCard));
        assertFalse(headSectDeck.contains(noiseInAnyTestCard));

        assertTrue(headSectDeck.contains(noiseInYourTestCard));
        assertTrue(headSectDeck.contains(silenceTestCard));

        assertFalse(discardedSectDeck.contains(noiseInYourTestCard));
        assertFalse(discardedSectDeck.contains(silenceTestCard));

        assertSame(noiseInAnyTestCard, phaseContext.getGamePhase());

    }

    @Test
    public void noiseInYourtest() {

        Card tempCard = headSectDeck.poll();

        headSectDeck.add(tempCard);

        useSectPhaseToTest.start();

        assertFalse(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(noiseInYourTestCard));
        assertFalse(headSectDeck.contains(noiseInYourTestCard));

        assertTrue(headSectDeck.contains(noiseInAnyTestCard));
        assertTrue(headSectDeck.contains(silenceTestCard));

        assertFalse(discardedSectDeck.contains(noiseInAnyTestCard));
        assertFalse(discardedSectDeck.contains(silenceTestCard));

        assertSame(phaseContext.getTurnEnd(), phaseContext.getGamePhase());

    }

    @Test
    public void silencetest() {

        Card tempCard = headSectDeck.poll();

        headSectDeck.add(tempCard);

        tempCard = headSectDeck.poll();

        headSectDeck.add(tempCard);

        useSectPhaseToTest.start();

        assertFalse(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(silenceTestCard));
        assertFalse(headSectDeck.contains(silenceTestCard));

        assertTrue(headSectDeck.contains(noiseInAnyTestCard));
        assertTrue(headSectDeck.contains(noiseInYourTestCard));

        assertFalse(discardedSectDeck.contains(noiseInAnyTestCard));
        assertFalse(discardedSectDeck.contains(noiseInYourTestCard));

        assertSame(phaseContext.getTurnEnd(), phaseContext.getGamePhase());

    }

    @Test
    public void rebuildSectDecktest() {

        useSectPhaseToTest.start();

        assertFalse(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(noiseInAnyTestCard));
        assertFalse(headSectDeck.contains(noiseInAnyTestCard));

        assertTrue(headSectDeck.contains(noiseInYourTestCard));
        assertTrue(headSectDeck.contains(silenceTestCard));

        assertFalse(discardedSectDeck.contains(noiseInYourTestCard));
        assertFalse(discardedSectDeck.contains(silenceTestCard));

        useSectPhaseToTest.start();

        assertFalse(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(noiseInYourTestCard));
        assertTrue(discardedSectDeck.contains(noiseInAnyTestCard));

        assertFalse(headSectDeck.contains(noiseInYourTestCard));
        assertFalse(headSectDeck.contains(noiseInAnyTestCard));

        assertTrue(headSectDeck.contains(silenceTestCard));

        assertFalse(discardedSectDeck.contains(silenceTestCard));

        useSectPhaseToTest.start();

        assertTrue(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(silenceTestCard));
        assertTrue(discardedSectDeck.contains(noiseInAnyTestCard));
        assertTrue(discardedSectDeck.contains(noiseInYourTestCard));

        assertFalse(headSectDeck.contains(silenceTestCard));
        assertFalse(headSectDeck.contains(noiseInYourTestCard));
        assertFalse(headSectDeck.contains(noiseInAnyTestCard));

        useSectPhaseToTest.start();

        assertFalse(headSectDeck.isEmpty());
        assertFalse(discardedSectDeck.isEmpty());

        assertTrue(discardedSectDeck.contains(noiseInAnyTestCard));
        assertFalse(headSectDeck.contains(noiseInAnyTestCard));

        assertFalse(discardedSectDeck.contains(silenceTestCard));
        assertFalse(discardedSectDeck.contains(noiseInYourTestCard));

        assertTrue(headSectDeck.contains(silenceTestCard));
        assertTrue(headSectDeck.contains(noiseInYourTestCard));

    }

    @Test
    public void drawItemTest() {

        Card tempCard = headSectDeck.poll();

        headSectDeck.add(tempCard);

        useSectPhaseToTest.start();

        assertTrue(headItemDeck.isEmpty());
        assertTrue(discardedItemDeck.isEmpty());

        useSectPhaseToTest.start(); // not rebuilded deck testing

        assertTrue(headItemDeck.isEmpty());
        assertTrue(discardedItemDeck.isEmpty());

        useSectPhaseToTest.useItemCard(1);

        assertTrue(headItemDeck.isEmpty());
        assertFalse(discardedItemDeck.isEmpty());
        assertEquals(1, discardedItemDeck.size());

        assertTrue(discardedItemDeck.contains(teleportationTestItem));

        headSectDeck.poll();
        headSectDeck.poll();
        headSectDeck.poll();

        headSectDeck.add(noiseInYourTestCard);

        useSectPhaseToTest.start();

        assertTrue(headItemDeck.isEmpty());
        assertTrue(discardedItemDeck.isEmpty());

    }

}
