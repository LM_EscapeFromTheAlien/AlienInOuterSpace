package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class EscapePhaseTest {
    private Player humanPlayer;

    /** The players position. */
    Map<Player, SectorCoord> playersPosition = new HashMap<Player, SectorCoord>();

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase phase;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {

        humanPlayer = new Human("test1");
        gameStatus.addPlayer(humanPlayer);
        builder.modelBuild(gameStatus, "galilei");
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        phase = phaseContext.getEscapePhase();

    }

    /**
     * Test start.
     */
    @Test
    public void testStart() {
        phase.start();
        assertNotEquals(6, gameStatus.getEscapeHatchDeck().getHeadDeck().size());
    }

}
