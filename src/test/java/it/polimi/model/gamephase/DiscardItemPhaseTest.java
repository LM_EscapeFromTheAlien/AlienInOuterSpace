package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Queue;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Adrenaline;
import it.polimi.model.cards.itemcards.HumanAttack;
import it.polimi.model.cards.itemcards.Light;
import it.polimi.model.cards.itemcards.Protection;
import it.polimi.model.cards.itemcards.Sedative;
import it.polimi.model.cards.itemcards.Teleportation;
import it.polimi.model.decks.Deck;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class DiscardItemPhaseTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase discardPhaseToTest;
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Deck itemDeck;
    private Card itemCarddrew1;
    private Card itemCarddrew2;
    private Card itemCarddrew3;
    private Card itemCarddrew4;
    private List<Card> playerHand;

    @Before
    public void setUpEnviroment() {

        human1 = new Human("Human1");
        alien1 = new Alien("Alien2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        builder.modelBuild(gameStatus, "galilei");

        itemDeck = gameStatus.getItemDeck();

        itemCarddrew1 = itemDeck.draw();
        itemCarddrew2 = itemDeck.draw();
        itemCarddrew3 = itemDeck.draw();
        itemCarddrew4 = itemDeck.draw();

        assertEquals(human1.getPlayerHand().size(), 0);

        human1.addCard(itemCarddrew1);
        human1.addCard(itemCarddrew2);
        human1.addCard(itemCarddrew3);
        human1.addCard(itemCarddrew4);

        assertEquals(human1.getPlayerHand().size(), 4);

        playerHand = human1.getPlayerHand();

        Queue<Card> itemHeadDeck = itemDeck.getHeadDeck();

        assertFalse(itemHeadDeck.contains(itemCarddrew1));
        assertFalse(itemHeadDeck.contains(itemCarddrew2));
        assertFalse(itemHeadDeck.contains(itemCarddrew3));
        assertFalse(itemHeadDeck.contains(itemCarddrew4));

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getDiscardItem());
        discardPhaseToTest = phaseContext.getDiscardItem();
        discardPhaseToTest.start();
    }

    @Test
    public void discardCMDTest() {

        assertTrue(itemDeck.isDiscardedDeckEmpty());
        discardPhaseToTest.discard(1);
        assertFalse(itemDeck.isDiscardedDeckEmpty());

        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertTrue(itemDiscardedDeck.contains(itemCarddrew2));

        assertEquals(phaseContext.getTurnEnd(), phaseContext.getGamePhase());

    }

    @Test
    public void discardRightCard() {
        human1.getPlayerHand().clear();

        HumanAttack umanAtkTest = new HumanAttack();
        Sedative sedativeTest = new Sedative();

        human1.addCard(umanAtkTest);
        human1.addCard(sedativeTest);

        discardPhaseToTest.discard(1);
        assertFalse(playerHand.contains(sedativeTest));

        human1.addCard(sedativeTest);

        discardPhaseToTest.discard(0);
        assertFalse(playerHand.contains(umanAtkTest));

    }

    @Test
    public void useItemCMDTestTelep() {

        itemCarddrew3 = new Teleportation();

        human1.getPlayerHand().remove(2);
        human1.getPlayerHand().add(2, itemCarddrew3);
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        discardPhaseToTest.useItemCard(3);

        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertTrue(itemDiscardedDeck.contains(itemCarddrew3));

        assertSame(phaseContext.getTurnEnd(), phaseContext.getGamePhase());

    }

    @Test
    public void useItemCMDTestProt() {

        itemCarddrew3 = new Protection();

        human1.getPlayerHand().remove(2);
        human1.getPlayerHand().add(2, itemCarddrew3);
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        discardPhaseToTest.useItemCard(3);

        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertFalse(itemDiscardedDeck.contains(itemCarddrew3));
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        assertSame(phaseContext.getDiscardItem(), phaseContext.getGamePhase());

    }

    @Test
    public void useItemCMDTestAdren() {

        itemCarddrew3 = new Adrenaline();

        human1.getPlayerHand().remove(2);
        human1.getPlayerHand().add(2, itemCarddrew3);
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        discardPhaseToTest.useItemCard(3);

        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertFalse(itemDiscardedDeck.contains(itemCarddrew3));
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        assertSame(phaseContext.getDiscardItem(), phaseContext.getGamePhase());

    }

    @Test
    public void useItemCMDLight() {

        itemCarddrew3 = new Light();

        human1.getPlayerHand().remove(2);
        human1.getPlayerHand().add(2, itemCarddrew3);
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        discardPhaseToTest.useItemCard(3);

        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertTrue(itemDiscardedDeck.contains(itemCarddrew3));

        assertSame(itemCarddrew3, phaseContext.getGamePhase());

    }
}
