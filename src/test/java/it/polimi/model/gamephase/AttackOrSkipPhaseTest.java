package it.polimi.model.gamephase;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class AttackOrSkipPhaseTest {

    private Player player;
    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase attackOrPhaseToTest;
    private GamePhaseContext phaseContext;

    @Before
    public void setUpEnviroment() {

        Player player1 = new Human("Human1");
        Player player2 = new Alien("Alien2");
        Player player3 = new Human("Human3");
        Player player4 = new Alien("Alien4");

        gameStatus.addPlayer(player1);
        gameStatus.addPlayer(player2);
        gameStatus.addPlayer(player3);
        gameStatus.addPlayer(player4);

        builder.modelBuild(gameStatus, "galilei");

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getAttackOrSkip());
        attackOrPhaseToTest = phaseContext.getAttackOrSkip();
        attackOrPhaseToTest.start();
    }

    @Test
    public void tryToEscapeTestFalse() {
        assertEquals(phaseContext.getGamePhase(),
                phaseContext.getAttackOrSkip());
    }

    @Test
    public void tryToEscapeTestTrue() {

        player = gameStatus.getPlayers().peek();

        gameStatus.getPlayersPosition().remove(player);
        gameStatus.getPlayersPosition().put(player, new SectorCoord('B', 13));

        attackOrPhaseToTest.start();
        assertEquals(phaseContext.getGamePhase(), phaseContext.getTurnStart());

    }

    @Test
    public void skipCMDTest() {
        attackOrPhaseToTest.skip();
        assertEquals(phaseContext.getGamePhase(), phaseContext.getTurnEnd());
    }

    @Test
    public void attackCMDTestForHuman() {
        attackOrPhaseToTest.attack();
        ;
        assertEquals(phaseContext.getGamePhase(),
                phaseContext.getAttackOrSkip());
    }

    @Test
    public void attackCMDTestForAlien() {
        gameStatus.getPlayers().poll();
        attackOrPhaseToTest.start();
        attackOrPhaseToTest.attack();
        ;
        assertEquals(phaseContext.getGamePhase(), phaseContext.getTurnEnd());
    }

}
