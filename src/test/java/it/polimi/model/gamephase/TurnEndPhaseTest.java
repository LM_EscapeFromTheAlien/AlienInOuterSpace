package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Queue;
import java.util.Timer;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.decks.Deck;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class TurnEndPhaseTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase turnEndPhaseToTest;
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Deck itemDeck;
    private Card itemCarddrew1;
    private Card itemCarddrew2;
    private Card itemCarddrew3;
    private Card itemCarddrew4;
    private List<Card> playerHand;

    @Before
    public void setUpEnviroment() {

        human1 = new Human("Human1");
        alien1 = new Alien("Alien2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        builder.modelBuild(gameStatus, "galilei");

        itemDeck = gameStatus.getItemDeck();

        itemCarddrew1 = itemDeck.draw();
        itemCarddrew2 = itemDeck.draw();
        itemCarddrew3 = itemDeck.draw();
        itemCarddrew4 = itemDeck.draw();

        assertEquals(human1.getPlayerHand().size(), 0);

        human1.addCard(itemCarddrew1);
        human1.addCard(itemCarddrew2);
        human1.addCard(itemCarddrew3);
        human1.addCard(itemCarddrew4);

        assertEquals(human1.getPlayerHand().size(), 4);

        playerHand = human1.getPlayerHand();

        Queue<Card> itemHeadDeck = itemDeck.getHeadDeck();

        assertFalse(itemHeadDeck.contains(itemCarddrew1));
        assertFalse(itemHeadDeck.contains(itemCarddrew2));
        assertFalse(itemHeadDeck.contains(itemCarddrew3));
        assertFalse(itemHeadDeck.contains(itemCarddrew4));

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getTurnEnd());
        turnEndPhaseToTest = phaseContext.getTurnEnd();
    }

    @Test
    public void toDiscardItemTest() {
        turnEndPhaseToTest.start();
        assertSame(phaseContext.getDiscardItem(), phaseContext.getGamePhase());
    }

    @Test
    public void notToDiscardItemTest() {

        playerHand.remove(1);

        turnEndPhaseToTest.start();
        assertSame(phaseContext.getTurnEnd(), phaseContext.getGamePhase());
    }

    @Test
    public void endCMDTest() {
        Queue<Player> players = gameStatus.getPlayers();
        playerHand.remove(1);

        gameStatus.setTimer(new Timer());
        turnEndPhaseToTest.start();
        turnEndPhaseToTest.end();

        assertSame(phaseContext.getTurnStart(), phaseContext.getGamePhase());
        assertSame(alien1, players.peek());
    }

}
