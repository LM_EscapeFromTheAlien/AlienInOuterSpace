package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.Map;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.decks.Deck;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class AttackPhaseTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase attackPhaseToTest;
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Player human2;
    private Player alien2;
    private Map<Player, SectorCoord> playersPosition;
    private SectorCoord humanStart = new SectorCoord('L', 8);
    private SectorCoord alienStart = new SectorCoord('L', 6);

    @Before
    public void setUpEnviroment() {

        human1 = new Human("Human1");
        alien1 = new Alien("Alien2");
        human2 = new Human("Human3");
        alien2 = new Alien("Alien4");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        gameStatus.addPlayer(human2);
        gameStatus.addPlayer(alien2);

        builder.modelBuild(gameStatus, "galilei");
        playersPosition = gameStatus.getPlayersPosition();

        phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getAttack());
        attackPhaseToTest = phaseContext.getAttack();
    }

    @Test
    public void humanAtkTest() {

        attackPhaseToTest.start();

        assertEquals(gameStatus.getPlayers().size(), 3);
        assertFalse(gameStatus.getPlayers().contains(human2));
        assertFalse(gameStatus.getPlayersPosition().containsKey(human2));

        assertTrue(gameStatus.getPlayers().contains(alien1));
        assertTrue(gameStatus.getPlayersPosition().containsKey(alien1));
        assertTrue(gameStatus.getPlayers().contains(alien2));
        assertTrue(gameStatus.getPlayersPosition().containsKey(alien2));
        assertTrue(gameStatus.getPlayers().contains(human1));
        assertTrue(gameStatus.getPlayersPosition().containsKey(human1));

        assertEquals(phaseContext.getTurnEnd(), phaseContext.getGamePhase());
    }

    @Test
    public void allHumanKilledTest() {

        attackPhaseToTest.start();

        gameStatus.getPlayers().poll();
        gameStatus.getPlayers().add(human1);
        playersPosition.remove(alien1);
        playersPosition.put(alien1, humanStart);
        assertEquals(gameStatus.getPlayers().size(), 3);
        attackPhaseToTest.start();
        assertEquals(gameStatus.getPlayers().size(), 2);

        assertFalse(gameStatus.getPlayers().contains(human2));
        assertFalse(gameStatus.getPlayers().contains(human1));
        assertFalse(gameStatus.getPlayersPosition().containsKey(human2));
        assertFalse(gameStatus.getPlayersPosition().containsKey(human1));

        assertTrue(gameStatus.getPlayers().contains(alien1));
        assertTrue(gameStatus.getPlayersPosition().containsKey(alien1));
        assertTrue(gameStatus.getPlayers().contains(alien2));
        assertTrue(gameStatus.getPlayersPosition().containsKey(alien2));

        assertEquals(phaseContext.getEndGame(), phaseContext.getGamePhase());
    }

    @Test
    public void allAlienKilledTest() {

        gameStatus.getPlayers().poll();
        gameStatus.getPlayers().add(human1);

        attackPhaseToTest.start();
        assertEquals(gameStatus.getPlayers().size(), 3);
        assertFalse(gameStatus.getPlayers().contains(alien2));
        assertFalse(gameStatus.getPlayersPosition().containsKey(alien2));

        assertTrue(gameStatus.getPlayers().contains(alien1));
        assertTrue(gameStatus.getPlayersPosition().containsKey(alien1));
        assertTrue(gameStatus.getPlayers().contains(human2));
        assertTrue(gameStatus.getPlayersPosition().containsKey(human2));
        assertTrue(gameStatus.getPlayers().contains(human1));
        assertTrue(gameStatus.getPlayersPosition().containsKey(human1));

        gameStatus.getPlayers().poll();
        gameStatus.getPlayers().add(alien1);

        playersPosition.remove(human2);
        playersPosition.put(human2, alienStart);
        attackPhaseToTest.start();

        assertEquals(gameStatus.getPlayers().size(), 2);

        assertFalse(gameStatus.getPlayers().contains(alien1));
        assertFalse(gameStatus.getPlayersPosition().containsKey(alien1));

        assertTrue(gameStatus.getPlayers().contains(human2));
        assertTrue(gameStatus.getPlayersPosition().containsKey(human2));
        assertTrue(gameStatus.getPlayers().contains(human1));
        assertTrue(gameStatus.getPlayersPosition().containsKey(human1));

        assertEquals(phaseContext.getEndGame(), phaseContext.getGamePhase());
    }

    @Test
    public void itemDeckRestoringTest() {

        Deck itemDeck = gameStatus.getItemDeck();

        Card itemCarddrew1 = itemDeck.draw();
        Card itemCarddrew2 = itemDeck.draw();

        assertEquals(human2.getPlayerHand().size(), 0);

        human2.addCard(itemCarddrew2);
        human2.addCard(itemCarddrew1);

        assertEquals(human2.getPlayerHand().size(), 2);
        assertTrue(itemDeck.isDiscardedDeckEmpty());

        attackPhaseToTest.start();

        assert (gameStatus.getPlayers().size() == 3);
        assertFalse(gameStatus.getPlayers().contains(human2));
        assertFalse(gameStatus.getPlayersPosition().containsKey(human2));

        assertFalse(itemDeck.isDiscardedDeckEmpty());

        assertEquals(phaseContext.getTurnEnd(), phaseContext.getGamePhase());
    }

}
