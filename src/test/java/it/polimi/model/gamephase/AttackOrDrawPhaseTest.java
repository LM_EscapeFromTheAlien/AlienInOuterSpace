package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.decks.Deck;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class AttackOrDrawPhaseTest {

    private Player humanPlayer;
    private Player alienPlayer;
    private Player turnPlayer;
    private Queue<Player> players;

    Map<Player, SectorCoord> playersPosition = new HashMap<Player, SectorCoord>();

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase phase;

    @Before
    public void setUp() throws Exception {
        humanPlayer = new Human("test1");
        alienPlayer = new Alien("test2");
        gameStatus.addPlayer(humanPlayer);
        gameStatus.addPlayer(alienPlayer);

        builder.modelBuild(gameStatus, "galilei");
        turnPlayer = gameStatus.getPlayers().peek();
        players = gameStatus.getPlayers();
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        phaseContext.setGamePhase(phaseContext.getAttackOrDraw());
        phase = phaseContext.getAttackOrDraw();
        playersPosition = gameStatus.getPlayersPosition();
    }

    @Test
    public void testResetTurnPlayer() {
        humanPlayer.speedUp();
        phase.start();
        assertEquals(1, ((Human) humanPlayer).getMaxfootsteps());
    }

    @Test
    public void testDrawSectCard() {
        SectorCoord test = new SectorCoord('M', 8);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, test);
        phase.start();

        phase.drawSectCard();
        Deck sectorDeck = gameStatus.getSectorDeck();

        assertFalse(sectorDeck.isHeadDeckEmpty());

    }

    @Test
    public void testAttack() {
        Player tempPlayer = players.poll();
        players.add(tempPlayer);
        phase.start();
        phase.attack();
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnEnd());
    }

    @Test
    public void testCantAttack() {
        phase.start();
        phase.attack();
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), phase);
    }
}
