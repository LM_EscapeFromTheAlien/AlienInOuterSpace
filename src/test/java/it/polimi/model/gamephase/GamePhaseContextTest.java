package it.polimi.model.gamephase;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;

import org.junit.Before;
import org.junit.Test;

public class GamePhaseContextTest {

    private GameStatus status;
    private GamePhaseContext phaseChanger;
    private GamePhase testIsThisPhase;
    private boolean isRightPhase;

    /**
     * Enviroment init.
     */
    @Before
    public void enviromentInit() {
        status = new GameStatus();
        phaseChanger = new GamePhaseContext(status);
    }

    /**
     * Variable ok test.
     */
    @Test
    public void variableOkTest() {
        testIsThisPhase = phaseChanger.getTurnStart();
        isRightPhase = testIsThisPhase instanceof TurnStartPhase;
        assertTrue(isRightPhase);
        testIsThisPhase = phaseChanger.getAttackOrDraw();
        isRightPhase = testIsThisPhase instanceof AttackOrDrawPhase;
        assertTrue(isRightPhase);
        testIsThisPhase = phaseChanger.getAttackOrSkip();
        isRightPhase = testIsThisPhase instanceof AttackOrSkipPhase;
        assertTrue(isRightPhase);
        testIsThisPhase = phaseChanger.getDiscardItem();
        isRightPhase = testIsThisPhase instanceof DiscardItemPhase;
        assertTrue(isRightPhase);
        testIsThisPhase = phaseChanger.getEscapePhase();
        isRightPhase = testIsThisPhase instanceof EscapePhase;
        assertTrue(isRightPhase);
        testIsThisPhase = phaseChanger.getTurnEnd();
        isRightPhase = testIsThisPhase instanceof TurnEndPhase;
        assertTrue(isRightPhase);
        testIsThisPhase = phaseChanger.getUseSectCard();
        isRightPhase = testIsThisPhase instanceof UseSectCardPhase;
        assertTrue(isRightPhase);

    }
}
