package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Queue;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Adrenaline;
import it.polimi.model.cards.itemcards.HumanAttack;
import it.polimi.model.cards.itemcards.Protection;
import it.polimi.model.cards.itemcards.Teleportation;
import it.polimi.model.decks.Deck;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class GamePhaseTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Card andrenalineTestItem;
    private Card humanAtkTestItem;
    private Card protectionTestItem;
    private Card teleportationTestItem;
    private List<Card> playerHand;
    private Deck itemDeck;
    private Player human2;
    private Player alien2;

    @Before
    public void setUpEnviroment() {

        andrenalineTestItem = new Adrenaline();
        humanAtkTestItem = new HumanAttack();
        protectionTestItem = new Protection();
        teleportationTestItem = new Teleportation();

        human1 = new Human("Human1");
        alien1 = new Alien("Alien1");
        human2 = new Human("Human2");
        alien2 = new Alien("Alien2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        gameStatus.addPlayer(human2);
        gameStatus.addPlayer(alien2);
        builder.modelBuild(gameStatus, "galilei");

        itemDeck = gameStatus.getItemDeck();

        assertEquals(human1.getPlayerHand().size(), 0);

        human1.addCard(andrenalineTestItem);
        human1.addCard(humanAtkTestItem);
        human1.addCard(protectionTestItem);
        human1.addCard(teleportationTestItem);

        alien1.addCard(andrenalineTestItem);
        alien1.addCard(humanAtkTestItem);
        alien1.addCard(protectionTestItem);
        alien1.addCard(teleportationTestItem);

        assertEquals(human1.getPlayerHand().size(), 4);

        playerHand = human1.getPlayerHand();

        phaseContext = gameStatus.getPhaseChanger();
    }

    @Test
    public void testSetNextPhase() {

        GamePhase phaseTesting = phaseContext.getGamePhase();
        GamePhase phaseChanged = phaseTesting.setNextPhase(phaseContext
                .getAttackOrDraw());
        assertEquals(phaseContext.getGamePhase(),
                phaseContext.getAttackOrDraw());
        assertEquals(phaseChanged, phaseContext.getAttackOrDraw());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getAttack());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getAttack());
        assertEquals(phaseChanged, phaseContext.getAttack());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting
                .setNextPhase(phaseContext.getAttackOrSkip());
        assertEquals(phaseContext.getGamePhase(),
                phaseContext.getAttackOrSkip());
        assertEquals(phaseChanged, phaseContext.getAttackOrSkip());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getTurnStart());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getTurnStart());
        assertEquals(phaseChanged, phaseContext.getTurnStart());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getTurnEnd());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getTurnEnd());
        assertEquals(phaseChanged, phaseContext.getTurnEnd());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getDiscardItem());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getDiscardItem());
        assertEquals(phaseChanged, phaseContext.getDiscardItem());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getEscapePhase());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getEscapePhase());
        assertEquals(phaseChanged, phaseContext.getEscapePhase());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getUseSectCard());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getUseSectCard());
        assertEquals(phaseChanged, phaseContext.getUseSectCard());
        phaseTesting = phaseContext.getGamePhase();
        phaseChanged = phaseTesting.setNextPhase(phaseContext.getGamePhase());
        assertEquals(phaseContext.getGamePhase(), phaseContext.getGamePhase());
        assertEquals(phaseChanged, phaseContext.getGamePhase());

    }

    @Test
    public void registerToNoteCenter() {
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getAttack()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getAttackOrDraw()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getAttackOrSkip()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getDiscardItem()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getEscapePhase()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getTurnEnd()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getUseSectCard()
                .getNotificationCenter());
        assertEquals(gameStatus.getNoteCenter(), phaseContext.getGamePhase()
                .getNotificationCenter());
    }

    @Test
    public void gameStatusTest() {
        assertEquals(gameStatus, phaseContext.getAttack().getGameStatus());
        assertEquals(gameStatus, phaseContext.getAttackOrDraw().getGameStatus());
        assertEquals(gameStatus, phaseContext.getAttackOrSkip().getGameStatus());
        assertEquals(gameStatus, phaseContext.getDiscardItem().getGameStatus());
        assertEquals(gameStatus, phaseContext.getEscapePhase().getGameStatus());
        assertEquals(gameStatus, phaseContext.getTurnEnd().getGameStatus());
        assertEquals(gameStatus, phaseContext.getUseSectCard().getGameStatus());
        assertEquals(gameStatus, phaseContext.getGamePhase().getGameStatus());
    }

    @Test
    public void useItemCMDCardUsed() {

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

        phaseContext.setGamePhase(phaseContext.getTurnStart());

        phaseContext.getGamePhase().useItemCard(1);

        assertEquals(1, itemDeck.getDiscardedDeck().size());
        assertFalse(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));
    }

    @Test
    public void useItemCMDNotRightPhase() {

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

        phaseContext.setGamePhase(phaseContext.getAttackOrSkip());

        phaseContext.getGamePhase().useItemCard(1);

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));
    }

    @Test
    public void useItemCMDNeverOnProtection() {

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

        phaseContext.getGamePhase().useItemCard(3);

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));
    }

    @Test
    public void useItemCMDerr() {

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

        phaseContext.getGamePhase().useItemCard(5);

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

        phaseContext.getGamePhase().useItemCard(0);

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

        gameStatus.getPlayers().poll();

        playerHand = alien1.getPlayerHand();

        phaseContext.getGamePhase().useItemCard(2);

        assertEquals(0, itemDeck.getDiscardedDeck().size());
        assertTrue(playerHand.contains(protectionTestItem));
        assertTrue(playerHand.contains(teleportationTestItem));
        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));

    }

    @Test
    public void turnPlayerExitTest() {

        phaseContext.getGamePhase().playerExit(human1);
        assertSame(gameStatus.getPlayers().peek(), alien1);
        assertFalse(gameStatus.getPlayers().contains(human1));
        assertFalse(gameStatus.getPlayersPosition().containsKey(human1));

        itemDeck = gameStatus.getItemDeck();
        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertTrue(itemDiscardedDeck.contains(protectionTestItem));
        assertTrue(itemDiscardedDeck.contains(teleportationTestItem));
        assertTrue(itemDiscardedDeck.contains(andrenalineTestItem));
        assertTrue(itemDiscardedDeck.contains(humanAtkTestItem));

    }

    @Test
    public void notTurnPlayerExitTest() {

        phaseContext.getGamePhase().playerExit(alien1);
        assertSame(gameStatus.getPlayers().peek(), human1);
        assertFalse(gameStatus.getPlayers().contains(alien1));
        assertFalse(gameStatus.getPlayersPosition().containsKey(alien1));

        itemDeck = gameStatus.getItemDeck();
        Queue<Card> itemDiscardedDeck = itemDeck.getDiscardedDeck();

        assertTrue(itemDiscardedDeck.contains(protectionTestItem));
        assertTrue(itemDiscardedDeck.contains(teleportationTestItem));
        assertTrue(itemDiscardedDeck.contains(andrenalineTestItem));
        assertTrue(itemDiscardedDeck.contains(humanAtkTestItem));

    }

}
