package it.polimi.model.gamephase;

import static org.junit.Assert.*;

import java.util.Map;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class TurnStartPhaseTest {

    private Player player;
    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhase startPhaseToTest;
    private GamePhaseContext phaseContext;

    @Before
    public void setUpEnviroment() {
        player = new Human("test1");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        phaseContext = gameStatus.getPhaseChanger();
        startPhaseToTest = phaseContext.getTurnStart();
    }

    @Test
    public void startTest() {
        startPhaseToTest.start();
        assertNotNull(gameStatus.getTimer());
    }

    @Test
    public void tooFewPlayers() {
        startPhaseToTest.start();
        assertEquals(phaseContext.getEndGame(), phaseContext.getGamePhase());
    }

    @Test
    public void notTooFewPlayers() {
        player = new Alien("test2");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        startPhaseToTest.start();
        assertEquals(phaseContext.getTurnStart(), phaseContext.getGamePhase());
    }

    @Test
    public void moveTest() {
        Player turnPlayer = gameStatus.getPlayers().peek();
        startPhaseToTest.move("L", "9");
        SectorCoord newPos = new SectorCoord('L', 9);
        SectorCoord storedPosInStatus = gameStatus.getPlayersPosition().get(
                turnPlayer);
        assertEquals(storedPosInStatus, newPos);

    }

    @Test
    public void nextPhaseTestSafe() {

        GamePhase isTheRightPhase = gameStatus.getPhaseChanger()
                .getAttackOrSkip();

        startPhaseToTest.move("L", "9");

        assertSame(isTheRightPhase, gameStatus.getPhaseChanger().getGamePhase());

    }

    @Test
    public void nextPhaseTestDNG() {

        GamePhase isTheRightPhase = gameStatus.getPhaseChanger()
                .getAttackOrDraw();

        startPhaseToTest.move("M", "8");

        assertSame(isTheRightPhase, gameStatus.getPhaseChanger().getGamePhase());

    }

    @Test
    public void playerMoveValidation() {

        Player turnPlayer = gameStatus.getPlayers().peek();
        Map<Player, SectorCoord> playersPosition = gameStatus
                .getPlayersPosition();

        SectorCoord newPos = new SectorCoord('L', 9);
        SectorCoord oldPos = new SectorCoord('L', 9);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, oldPos);
        startPhaseToTest.move("L", "9");

        assertEquals(oldPos, new SectorCoord('L', 9));

        newPos = new SectorCoord('L', 9);
        oldPos = new SectorCoord('L', 8);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, oldPos);
        startPhaseToTest.move("L", "9");

        assertEquals(newPos, playersPosition.get(turnPlayer));

        newPos = new SectorCoord('X', 9);
        oldPos = new SectorCoord('W', 9);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, oldPos);
        startPhaseToTest.move("X", "9");

        assertEquals(oldPos, playersPosition.get(turnPlayer));

        newPos = new SectorCoord('L', 15);
        oldPos = new SectorCoord('L', 14);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, oldPos);
        startPhaseToTest.move("L", "15");

        assertEquals(oldPos, playersPosition.get(turnPlayer));

        newPos = new SectorCoord('L', 0);
        oldPos = new SectorCoord('L', 1);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, oldPos);
        startPhaseToTest.move("L", "0");

        assertEquals(oldPos, playersPosition.get(turnPlayer));

        newPos = new SectorCoord('z', 9);
        oldPos = new SectorCoord('A', 9);
        playersPosition.remove(turnPlayer);
        playersPosition.put(turnPlayer, oldPos);
        startPhaseToTest.move("z", "9");

        assertEquals(oldPos, playersPosition.get(turnPlayer));
    }

}
