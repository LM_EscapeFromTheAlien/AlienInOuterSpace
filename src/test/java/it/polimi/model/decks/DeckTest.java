package it.polimi.model.decks;

import static org.junit.Assert.*;

import java.util.List;

import it.polimi.model.cards.Card;
import it.polimi.model.decks.Deck;
import it.polimi.model.decks.EscapeHatchDeck;
import it.polimi.model.decks.ItemDeck;
import it.polimi.model.decks.SectorDeck;
import it.polimi.model.player.Human;

import org.junit.Test;

public class DeckTest {

    Deck testSectorDeck = new SectorDeck();
    Deck testItemDeck = new ItemDeck();
    Deck testEscapeHatchDeck = new EscapeHatchDeck();
    int dimOfSector = testSectorDeck.getHeadDeck().size();
    int dimOfItem = testItemDeck.getHeadDeck().size();
    int dimOfEscape = testEscapeHatchDeck.getHeadDeck().size();
    private Human player;

    @Test
    public void testIsDeckEmpty() {
        assertFalse(testSectorDeck.isHeadDeckEmpty());
        assertFalse(testItemDeck.isHeadDeckEmpty());
        assertFalse(testEscapeHatchDeck.isHeadDeckEmpty());
    }

    @Test
    public void testNumOfCard() {

        int dimOfSector = testSectorDeck.getHeadDeck().size();
        int dimOfItem = testItemDeck.getHeadDeck().size();
        int dimOfEscape = testEscapeHatchDeck.getHeadDeck().size();
        assertEquals(25, dimOfSector);
        assertEquals(12, dimOfItem);
        assertEquals(6, dimOfEscape);
    }

    @Test
    public void testAddToDiscardedForSector() {

        Card save = testSectorDeck.getHeadDeck().peek();
        testSectorDeck.addtoDiscarded();
        assertEquals(save, testSectorDeck.discardedDeck.peek());
        assertEquals(save, testSectorDeck.draw());
        assertNotEquals(25, testSectorDeck.getHeadDeck().size());
        assertEquals(dimOfSector - 1, testSectorDeck.getHeadDeck().size());
    }

    @Test
    public void testAddToDiscardedForItem() {

        player = new Human("test");
        Card save = testItemDeck.getHeadDeck().peek();
        player.addCard(testItemDeck.draw());
        List<Card> playerHand = player.getPlayerHand();
        Card itemCardToUse = playerHand.get(0);
        ((ItemDeck) testItemDeck).addtoDiscarded(itemCardToUse);
        playerHand.remove(itemCardToUse);

        assertEquals(itemCardToUse, testItemDeck.discardedDeck.peek());
        assertEquals(save, itemCardToUse);
        assertNotEquals(12, testItemDeck.getHeadDeck().size());
        assertEquals(dimOfItem - 1, testItemDeck.getHeadDeck().size());
    }

    @Test
    public void testRebuildForSector() {
        testSectorDeck.getDiscardedDeck().addAll(testSectorDeck.getHeadDeck());
        testSectorDeck.getHeadDeck().clear();
        assertTrue(testSectorDeck.getHeadDeck().isEmpty());
        testSectorDeck.reBuildDeck();
        assertFalse(testSectorDeck.getHeadDeck().isEmpty());
    }

    @Test
    public void testRebuildForItemWithDiscarded() {
        testItemDeck.getDiscardedDeck().addAll(testSectorDeck.getHeadDeck());
        testItemDeck.getHeadDeck().clear();
        assertTrue(testItemDeck.getHeadDeck().isEmpty());
        testItemDeck.reBuildDeck();
        assertFalse(testItemDeck.getHeadDeck().isEmpty());
    }

    @Test
    public void testRebuildForItemWithOutDiscarded() {
        testItemDeck.getHeadDeck().clear();
        assertTrue(testItemDeck.getHeadDeck().isEmpty());
        testItemDeck.reBuildDeck();
        assertTrue(testItemDeck.getHeadDeck().isEmpty());
    }

}
