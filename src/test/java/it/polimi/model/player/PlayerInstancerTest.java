package it.polimi.model.player;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

public class PlayerInstancerTest {

    private PlayerInstancer plInst;
    private Queue<Player> players;

    /**
     * Inits the enviroment for player instancer.
     */
    @Before
    public void initEnviromentForPlayerInstancer() {
        plInst = new PlayerInstancer(8);
        players = plInst.getPlayerList();
    }

    /**
     * Test one alien one human.
     */
    @Test
    public void testOneAlienOneHuman() {
        Player playerTested;
        Iterator<Player> plIterator = players.iterator();
        while (plIterator.hasNext()) {
            playerTested = plIterator.next();
            assertEquals("Alien", playerTested.getPlayerRace());
            playerTested = plIterator.next();
            assertEquals("Human", playerTested.getPlayerRace());
        }
    }

}
