package it.polimi.model.player;

import static org.junit.Assert.*;
import it.polimi.model.GameBuilder;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Adrenaline;
import it.polimi.model.cards.itemcards.HumanAttack;
import it.polimi.model.cards.itemcards.Protection;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.map.SectorCoord;

import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private Player human1;
    private Player alien1;
    private Card andrenalineTestItem;
    private Card humanAtkTestItem;
    private Card protectionTestItem;
    private List<Card> playerHand;
    private GamePhaseContext phaseContext;
    private Queue<Player> players;
    private Map<Player, SectorCoord> playersPos;

    @Before
    public void setUpEnviroment() {

        andrenalineTestItem = new Adrenaline();
        humanAtkTestItem = new HumanAttack();
        protectionTestItem = new Protection();

        human1 = new Human("Human1");
        alien1 = new Alien("Alien1");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        builder.modelBuild(gameStatus, "galilei");

        assertEquals(human1.getPlayerHand().size(), 0);

        human1.addCard(andrenalineTestItem);
        human1.addCard(humanAtkTestItem);
        human1.addCard(protectionTestItem);

        assertEquals(human1.getPlayerHand().size(), 3);

        playerHand = human1.getPlayerHand();
        players = gameStatus.getPlayers();
        playersPos = gameStatus.getPlayersPosition();

        phaseContext = gameStatus.getPhaseChanger();
    }

    @Test
    public void testNotProtected() {

        human1.setProtected();
        assertTrue(human1.iAmProtect());
        Player humanToProtectTest = players.poll();
        players.add(humanToProtectTest);

        playersPos.remove(alien1);
        playersPos.put(alien1, new SectorCoord('L', 8));

        phaseContext.getAttack().start();

        assertFalse(human1.iAmProtect());
        assertFalse(playerHand.contains(protectionTestItem));

        assertTrue(playerHand.contains(andrenalineTestItem));
        assertTrue(playerHand.contains(humanAtkTestItem));
    }

}
