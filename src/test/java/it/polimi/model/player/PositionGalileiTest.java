package it.polimi.model.player;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.Map;

import it.polimi.model.map.HexGridInstancer;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Human;

import org.junit.Before;
import org.junit.Test;

public class PositionGalileiTest {

    private Human human;
    private Alien alien;

    @Before
    public void playerMovementEnviroment() {
        human = new Human("test");
        alien = new Alien("test");
    }

    @Test
    public void testCanMovedPari() throws FileNotFoundException {
        SectorCoord from = new SectorCoord('L', 6);
        HexGridInstancer gridInstancer = new HexGridInstancer("galilei");
        Map<SectorCoord, SectType> grid = gridInstancer.getGridInstance();

        human.speedUp();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('L', 5)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('L', 4)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 3)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 5)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 4)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 3)));

        alien.speedUp();
        human.speedDown();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('L', 5)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 4)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 1)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 5)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 4)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 3)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 1)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 7)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 7)));

        from = new SectorCoord('L', 5);
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 6)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 6)));

    }

    @Test
    public void testCanMovedDispari() throws FileNotFoundException {
        SectorCoord from = new SectorCoord('M', 6);
        HexGridInstancer gridInstancer = new HexGridInstancer("galilei");
        Map<SectorCoord, SectType> grid = gridInstancer.getGridInstance();

        human.speedUp();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('M', 5)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('M', 4)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 3)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 5)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 4)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('M', 3)));

        alien.speedUp();
        human.speedDown();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('M', 5)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 4)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 1)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 5)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 4)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 3)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('M', 1)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('M', 7)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 7)));

    }

    @Test
    public void casuallyWalkingAroundGalilei() throws FileNotFoundException {
        SectorCoord from = new SectorCoord('Q', 9);
        HexGridInstancer gridInstancer = new HexGridInstancer("galilei");
        Map<SectorCoord, SectType> grid = gridInstancer.getGridInstance();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('R', 9)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('R', 8)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('Q', 8)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('Q', 10)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('P', 8)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('P', 9)));

        assertEquals(false, human.canMove(grid, from, new SectorCoord('R', 10)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('R', 7)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('Q', 7)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('Q', 11)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('O', 9)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('P', 10)));

        assertEquals(true, alien.canMove(grid, from, new SectorCoord('R', 9)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('R', 8)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('Q', 8)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('Q', 10)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('P', 8)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('P', 9)));

        assertEquals(true, alien.canMove(grid, from, new SectorCoord('O', 10)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('R', 8)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('Q', 7)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('Q', 11)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('O', 9)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('P', 10)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('R', 10)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('T', 7)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('Q', 6)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('Q', 12)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('N', 9)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('N', 10)));

    }

}
