package it.polimi.model.player;

import static org.junit.Assert.assertEquals;
import it.polimi.model.map.HexGridInstancer;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;

import java.io.FileNotFoundException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class PositionTestFermi {
    private Human human;
    private Alien alien;

    @Before
    public void playerMovementEnviroment() {
        human = new Human("test");
        alien = new Alien("test");
    }

    @Test
    public void testCanMovedPari() throws FileNotFoundException {
        SectorCoord from = new SectorCoord('L', 9);
        HexGridInstancer gridInstancer = new HexGridInstancer("fermi");
        Map<SectorCoord, SectType> grid = gridInstancer.getGridInstance();

        human.speedUp();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('L', 7)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('K', 8)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('M', 8)));

        alien.speedUp();
        human.speedDown();

        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 7)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('L', 8)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 9)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('K', 10)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('O', 7)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('J', 7)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('I', 9)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 10)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 10)));

        from = new SectorCoord('L', 8);
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 9)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 9)));

    }

    @Test
    public void testCanMovedDispari() throws FileNotFoundException {
        SectorCoord from = new SectorCoord('K', 4);
        HexGridInstancer gridInstancer = new HexGridInstancer("fermi");
        Map<SectorCoord, SectType> grid = gridInstancer.getGridInstance();

        human.speedUp();

        assertEquals(true, human.canMove(grid, from, new SectorCoord('M', 3)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('J', 5)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 6)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 3)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('K', 2)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 6)));

        alien.speedUp();
        human.speedDown();

        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 6)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 3)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('N', 3)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 2)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 6)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('J', 1)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 7)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 2)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('J', 2)));

    }

    @Test
    public void casuallyWalkingAroundFermi() throws FileNotFoundException {
        SectorCoord from = new SectorCoord('N', 11);
        HexGridInstancer gridInstancer = new HexGridInstancer("fermi");
        Map<SectorCoord, SectType> grid = gridInstancer.getGridInstance();

        assertEquals(false, human.canMove(grid, from, new SectorCoord('O', 10)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('L', 9)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('O', 12)));
        assertEquals(true, human.canMove(grid, from, new SectorCoord('M', 11)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('O', 10)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('M', 13)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('N', 9)));
        assertEquals(false, human.canMove(grid, from, new SectorCoord('P', 11)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('N', 9)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('O', 11)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('O', 10)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('L', 11)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 10)));
        assertEquals(true, alien.canMove(grid, from, new SectorCoord('M', 13)));

        assertEquals(false, alien.canMove(grid, from, new SectorCoord('O', 9)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('L', 12)));
        assertEquals(false, alien.canMove(grid, from, new SectorCoord('P', 10)));

    }
}
