package it.polimi.model.player;

import static org.junit.Assert.*;
import it.polimi.model.GameBuilder;
import it.polimi.model.GameStatus;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.HumanAttack;
import it.polimi.model.cards.itemcards.Protection;
import it.polimi.model.cards.itemcards.Teleportation;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class HumanTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private Player human1;
    private Player alien1;
    private Card itemCarddrew1;
    private Card itemCarddrew2;
    private Card itemCarddrew3;
    private List<Card> playerHand;

    @Before
    public void setUpEnviroment() {

        human1 = new Human("Human1");
        alien1 = new Alien("Alien2");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        builder.modelBuild(gameStatus, "galilei");

        itemCarddrew1 = new Teleportation();
        itemCarddrew2 = new HumanAttack();
        itemCarddrew3 = new Protection();

        assertEquals(human1.getPlayerHand().size(), 0);

        human1.addCard(itemCarddrew1);
        human1.addCard(itemCarddrew2);
        human1.addCard(itemCarddrew3);

        assertEquals(human1.getPlayerHand().size(), 3);

        playerHand = human1.getPlayerHand();
    }

    @Test
    public void testCanUseItem() {

        assertTrue(human1.canUseItem());
        playerHand.remove(2);
        playerHand.remove(1);
        playerHand.remove(0);
        assertFalse(human1.canUseItem());
    }

}
