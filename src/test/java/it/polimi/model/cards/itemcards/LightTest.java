package it.polimi.model.cards.itemcards;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.Map;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Light;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class LightTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private Card lightCardToUse;
    private Map<Player, SectorCoord> playersPosition;
    private LinkedList<SectorCoord> sectWithPlayersTest;

    @Before
    public void setUpEnviroment() {
        builder.modelBuild(gameStatus, "galilei");
        lightCardToUse = new Light();
        ((CardPhaseDecorator) lightCardToUse).setCardDecoredPhase(gameStatus
                .getPhaseChanger().getAttackOrDraw());
        lightCardToUse.phaseDecorer();

        Player player1 = new Human("Human1");
        Player player2 = new Alien("Alien2");
        Player player3 = new Human("Human3");
        Player player4 = new Alien("Alien4");
        Player player5 = new Human("Human5");
        Player player6 = new Alien("Alien6");
        Player player7 = new Human("Human7");
        Player player8 = new Alien("Alien8");

        gameStatus.addPlayer(player1);
        gameStatus.addPlayer(player2);
        gameStatus.addPlayer(player3);
        gameStatus.addPlayer(player4);
        gameStatus.addPlayer(player5);
        gameStatus.addPlayer(player6);
        gameStatus.addPlayer(player7);
        gameStatus.addPlayer(player8);

        playersPosition = gameStatus.getPlayersPosition();
        playersPosition.clear();

        sectWithPlayersTest = new LinkedList<SectorCoord>();
        sectWithPlayersTest.add(new SectorCoord('K', 3));
        sectWithPlayersTest.add(new SectorCoord('J', 3));
        sectWithPlayersTest.add(new SectorCoord('J', 4));
        sectWithPlayersTest.add(new SectorCoord('K', 5));
        sectWithPlayersTest.add(new SectorCoord('L', 4));
        sectWithPlayersTest.add(new SectorCoord('L', 3));
        sectWithPlayersTest.add(new SectorCoord('K', 4));

        playersPosition.put(player2, new SectorCoord('K', 3));
        playersPosition.put(player3, new SectorCoord('J', 3));
        playersPosition.put(player4, new SectorCoord('J', 4));
        playersPosition.put(player5, new SectorCoord('K', 5));
        playersPosition.put(player6, new SectorCoord('L', 4));
        playersPosition.put(player7, new SectorCoord('L', 3));
        playersPosition.put(player8, new SectorCoord('K', 4));

    }

    @Test
    public void phaseDecorerTest() {
        assertEquals(gameStatus.getPhaseChanger().getGamePhase(),
                lightCardToUse);
    }

    @Test
    public void lightCMDTest() {
        ((Light) lightCardToUse).light("K", "4");
        LinkedList<SectorCoord> sectToVerify = (LinkedList<SectorCoord>) ((Light) lightCardToUse).sectWithPlayers;
        for (SectorCoord sectTest : sectWithPlayersTest) {
            assertEquals(sectToVerify.removeFirst(), sectTest);
        }

        assertEquals(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getAttackOrDraw());
    }

    @Test
    public void noOneHereTest() {
        ((Light) lightCardToUse).light("L", "9");
        LinkedList<SectorCoord> sectToVerify = (LinkedList<SectorCoord>) ((Light) lightCardToUse).sectWithPlayers;

        assertTrue(sectToVerify.isEmpty());

        assertEquals(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getAttackOrDraw());
    }

}