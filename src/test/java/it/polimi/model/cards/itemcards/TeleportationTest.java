package it.polimi.model.cards.itemcards;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Teleportation;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class TeleportationTest {

    Human player;
    Map<Player, SectorCoord> playersPosition = new HashMap<Player, SectorCoord>();

    GameStatus gameStatus = new GameStatus();
    GameBuilder builder = new GameBuilder();

    @Before
    public void setUp() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
    }

    @Test
    public void testPhaseDecorer() {
        SectorCoord test = new SectorCoord('M', 8);
        playersPosition.put(player, test);
        Card itemCardToUse = new Teleportation();
        ((CardPhaseDecorator) itemCardToUse).setCardDecoredPhase(gameStatus
                .getPhaseChanger().getTurnEnd());
        itemCardToUse.phaseDecorer();
        SectorCoord to = new SectorCoord('L', 8);
        SectorCoord tele = gameStatus.getPlayersPosition().get(player);
        assertEquals(to, tele);
    }

}
