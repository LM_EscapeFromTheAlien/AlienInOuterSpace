package it.polimi.model.cards.itemcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.itemcards.Sedative;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Human;

import org.junit.Before;
import org.junit.Test;

public class SedativeTest {

    Human player;

    GameStatus gameStatus = new GameStatus();
    GameBuilder builder = new GameBuilder();

    private Sedative sedativeCardToUse;

    private GamePhaseContext phaseContext;

    @Before
    public void setUp() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        sedativeCardToUse = new Sedative();
        phaseContext = gameStatus.getPhaseChanger();
    }

    @Test
    public void testPhaseDecorer() {
        ((CardPhaseDecorator) sedativeCardToUse).setCardDecoredPhase(gameStatus
                .getPhaseChanger().getAttackOrDraw());
        sedativeCardToUse.phaseDecorer();
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnEnd());
    }

    @Test
    public void itemConditionToUse() {
        ((CardPhaseDecorator) sedativeCardToUse)
                .setCardDecoredPhase(phaseContext.getAttackOrDraw());
        sedativeCardToUse.phaseDecorer();

        assertTrue(sedativeCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) sedativeCardToUse)
                .setCardDecoredPhase(phaseContext.getAttackOrSkip());
        sedativeCardToUse.phaseDecorer();

        assertFalse(sedativeCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) sedativeCardToUse)
                .setCardDecoredPhase(phaseContext.getDiscardItem());
        sedativeCardToUse.phaseDecorer();

        assertFalse(sedativeCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) sedativeCardToUse)
                .setCardDecoredPhase(phaseContext.getTurnStart());
        sedativeCardToUse.phaseDecorer();

        assertFalse(sedativeCardToUse.itemConditionToUse());
    }

}
