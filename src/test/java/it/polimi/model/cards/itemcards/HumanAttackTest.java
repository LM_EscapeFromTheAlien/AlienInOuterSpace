package it.polimi.model.cards.itemcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.itemcards.HumanAttack;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class HumanAttackTest {

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private GamePhaseContext phaseContext;
    private Player human1;
    private Player alien1;
    private Player human2;
    private Player alien2;
    private HumanAttack humanAtkCardToUse;

    @Before
    public void setUp() {

        human1 = new Human("Human1");
        alien1 = new Alien("Alien2");
        human2 = new Human("Human3");
        alien2 = new Alien("Alien4");

        gameStatus.addPlayer(human1);
        gameStatus.addPlayer(alien1);
        gameStatus.addPlayer(human2);
        gameStatus.addPlayer(alien2);

        builder.modelBuild(gameStatus, "galilei");
        humanAtkCardToUse = new HumanAttack();
        phaseContext = gameStatus.getPhaseChanger();
    }

    @Test
    public void testPhaseDecorer() {
        ((CardPhaseDecorator) humanAtkCardToUse).setCardDecoredPhase(gameStatus
                .getPhaseChanger().getAttackOrDraw());
        humanAtkCardToUse.phaseDecorer();
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnEnd());
    }

    @Test
    public void itemConditionToUse() {
        ((CardPhaseDecorator) humanAtkCardToUse)
                .setCardDecoredPhase(phaseContext.getAttackOrDraw());
        humanAtkCardToUse.phaseDecorer();

        assertTrue(humanAtkCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) humanAtkCardToUse)
                .setCardDecoredPhase(phaseContext.getAttackOrSkip());
        humanAtkCardToUse.phaseDecorer();

        assertTrue(humanAtkCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) humanAtkCardToUse)
                .setCardDecoredPhase(phaseContext.getDiscardItem());
        humanAtkCardToUse.phaseDecorer();

        assertFalse(humanAtkCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) humanAtkCardToUse)
                .setCardDecoredPhase(phaseContext.getTurnStart());
        humanAtkCardToUse.phaseDecorer();

        assertFalse(humanAtkCardToUse.itemConditionToUse());
    }
}
