package it.polimi.model.cards.itemcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.itemcards.Protection;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class ProtectionTest {

    private Human player;
    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private Card protectionCardToUse;
    private Player turnPlayer;

    @Before
    public void setUpEnviroment() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        turnPlayer = gameStatus.getPlayers().peek();
        protectionCardToUse = new Protection();

    }

    @Test
    public void test() {
        GamePhase cardDecoredPhase = gameStatus.getPhaseChanger()
                .getGamePhase();
        ((CardPhaseDecorator) protectionCardToUse)
                .setCardDecoredPhase(cardDecoredPhase);
        ((CardPhaseDecorator) protectionCardToUse).drawEffetc();
        assertTrue(turnPlayer.iAmProtect());
    }

}
