package it.polimi.model.cards.itemcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.itemcards.Adrenaline;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Human;

import org.junit.Before;
import org.junit.Test;

public class AdrenalineTest {
    Human player;

    GameStatus gameStatus = new GameStatus();
    GameBuilder builder = new GameBuilder();
    private GamePhaseContext phaseContext;
    private Adrenaline adrenalineCardToUse;

    @Before
    public void setUp() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        phaseContext = gameStatus.getPhaseChanger();
        adrenalineCardToUse = new Adrenaline();
    }

    @Test
    public void testPhaseDecorer() {

        ((CardPhaseDecorator) adrenalineCardToUse)
                .setCardDecoredPhase(phaseContext.getTurnStart());
        adrenalineCardToUse.phaseDecorer();

        assertEquals(2, player.getMaxfootsteps());
    }

    @Test
    public void itemConditionToUse() {
        ((CardPhaseDecorator) adrenalineCardToUse)
                .setCardDecoredPhase(phaseContext.getTurnStart());
        adrenalineCardToUse.phaseDecorer();

        assertTrue(adrenalineCardToUse.itemConditionToUse());

        ((CardPhaseDecorator) adrenalineCardToUse)
                .setCardDecoredPhase(phaseContext.getAttackOrSkip());
        adrenalineCardToUse.phaseDecorer();

        assertFalse(adrenalineCardToUse.itemConditionToUse());
    }
}
