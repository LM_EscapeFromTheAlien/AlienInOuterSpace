package it.polimi.model.cards.escapehatchcards;

import static org.junit.Assert.*;

import java.util.Map;

import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.escapehatchcards.Red;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.map.SectType;
import it.polimi.model.map.SectorCoord;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class RedTest {
    private Player player;

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();

    @Before
    public void setUp() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        player = new Alien("test");
        gameStatus.addPlayer(player);
        player = new Human("test");
        gameStatus.addPlayer(player);

        builder.modelBuild(gameStatus, "galilei");
    }

    @Test
    public void testGameEndFalse() {
        player = gameStatus.getPlayers().peek();
        SectorCoord escapeCoord = gameStatus.getPlayersPosition().get(player);
        Map<Player, SectorCoord> playersPosition = gameStatus
                .getPlayersPosition();
        Card CardToUse = new Red();
        GamePhase phase = gameStatus.getPhaseChanger().getEscapePhase();
        Map<SectorCoord, SectType> grid = gameStatus.getGridInstance();

        ((CardPhaseDecorator) CardToUse).setCardDecoredPhase(phase);
        CardToUse.phaseDecorer();

        assertFalse(grid.containsKey(escapeCoord));
        assertTrue(playersPosition.containsKey(player));
        assertEquals(gameStatus.getPlayersPosition().get(player), escapeCoord);
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnStart());
    }

    @Test
    public void testGameEndTrue() {

        player = gameStatus.getPlayers().peek();
        SectorCoord escapeCoord = gameStatus.getPlayersPosition().get(player);
        Map<Player, SectorCoord> playersPosition = gameStatus
                .getPlayersPosition();
        Card CardToUse = new Red();
        GamePhase phase = gameStatus.getPhaseChanger().getEscapePhase();
        Map<SectorCoord, SectType> grid = gameStatus.getGridInstance();

        grid.remove(new SectorCoord('B', 13));
        grid.remove(new SectorCoord('V', 2));
        grid.remove(new SectorCoord('V', 13));
        grid.remove(new SectorCoord('B', 2));

        ((CardPhaseDecorator) CardToUse).setCardDecoredPhase(phase);
        CardToUse.phaseDecorer();

        assertFalse(grid.containsKey(escapeCoord));
        assertTrue(playersPosition.containsKey(player));
        assertEquals(gameStatus.getPlayersPosition().get(player), escapeCoord);
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getEndGame());
    }

}
