package it.polimi.model.cards.sectorcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.sectorcards.NoiseInYourSector;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.player.Human;

import org.junit.Before;
import org.junit.Test;

public class NoiseInYourSectorTest {

    private Human player;
    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();
    private Card noiseInYourCardToUse;

    @Before
    public void setUpEnviroment() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        noiseInYourCardToUse = new NoiseInYourSector();
        GamePhase phase = gameStatus.getPhaseChanger().getGamePhase();
        ((CardPhaseDecorator) noiseInYourCardToUse).setCardDecoredPhase(phase);
        noiseInYourCardToUse.phaseDecorer();
    }

    @Test
    public void cardDecorerTest() {
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnEnd());
    }

}
