package it.polimi.model.cards.sectorcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.Card;
import it.polimi.model.cards.sectorcards.NoiseInAnySector;
import it.polimi.model.gamephase.CardPhaseDecorator;
import it.polimi.model.gamephase.GamePhase;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class NoiseInAnySectorTest {

    static Human player;
    static GameStatus gameStatus = new GameStatus();
    static GameBuilder builder = new GameBuilder();
    static Card noiseCardToUse;

    @Before
    public void setUp() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        noiseCardToUse = new NoiseInAnySector();
        builder.modelBuild(gameStatus, "galilei");
    }

    @Test
    public void testPhaseDecorer() {
        GamePhase phase = gameStatus.getPhaseChanger().getGamePhase();
        ((CardPhaseDecorator) noiseCardToUse).setCardDecoredPhase(phase);
        noiseCardToUse.phaseDecorer();
        Player turnPlayer = gameStatus.getPlayers().peek();
        assertEquals(turnPlayer, player);
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), noiseCardToUse);

    }

    @Test
    public void testNoise() {
        GamePhase phase = gameStatus.getPhaseChanger().getGamePhase();
        ((CardPhaseDecorator) noiseCardToUse).setCardDecoredPhase(phase);
        noiseCardToUse.phaseDecorer();
        ((CardPhaseDecorator) noiseCardToUse).noise("M", "8");
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnEnd());
    }

    @Test
    public void testNotNoise() {
        GamePhase phase = gameStatus.getPhaseChanger().getGamePhase();
        ((CardPhaseDecorator) noiseCardToUse).setCardDecoredPhase(phase);
        noiseCardToUse.phaseDecorer();
        ((CardPhaseDecorator) noiseCardToUse).noise("L", "4");
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), noiseCardToUse);
        ((CardPhaseDecorator) noiseCardToUse).noise("L", "6");
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), noiseCardToUse);
        ((CardPhaseDecorator) noiseCardToUse).noise("L", "8");
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), noiseCardToUse);
        ((CardPhaseDecorator) noiseCardToUse).noise("B", "13");
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), noiseCardToUse);
    }

}
