package it.polimi.model.cards.sectorcards;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.GameBuilder;
import it.polimi.model.cards.sectorcards.Silence;
import it.polimi.model.player.Human;

import org.junit.Before;
import org.junit.Test;

public class SilenceTest {

    private Human player;

    private GameStatus gameStatus = new GameStatus();
    private GameBuilder builder = new GameBuilder();

    private Silence silenceCardToUse;

    @Before
    public void setUp() {
        player = new Human("test");
        gameStatus.addPlayer(player);
        builder.modelBuild(gameStatus, "galilei");
        silenceCardToUse = new Silence();
        silenceCardToUse.setCardDecoredPhase(gameStatus.getPhaseChanger()
                .getUseSectCard());
        silenceCardToUse.phaseDecorer();
    }

    @Test
    public void testPhaseDecorer() {
        assertSame(gameStatus.getPhaseChanger().getGamePhase(), gameStatus
                .getPhaseChanger().getTurnEnd());
    }
}
