package it.polimi.model;

import static org.junit.Assert.*;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class GameBuilderTest {

    private GameBuilder builderToTest;
    private GameStatus gameStatus;

    @Before
    public void setUpEnviroment() {

        builderToTest = new GameBuilder();
        gameStatus = new GameStatus();

        Player player1 = new Human("Human1");
        Player player2 = new Alien("Alien2");
        Player player3 = new Human("Human3");
        Player player4 = new Alien("Alien4");

        gameStatus.addPlayer(player1);
        gameStatus.addPlayer(player2);
        gameStatus.addPlayer(player3);
        gameStatus.addPlayer(player4);
    }

    @Test
    public void mapFindTestGalilei() {
        builderToTest.modelBuild(gameStatus, "galilei");
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        assertSame(phaseContext.getTurnStart(), phaseContext.getGamePhase());

    }

    @Test
    public void mapFindTestGalvani() {
        builderToTest.modelBuild(gameStatus, "galvani");
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        assertSame(phaseContext.getTurnStart(), phaseContext.getGamePhase());

    }

    @Test
    public void mapFindTestFermi() {
        builderToTest.modelBuild(gameStatus, "fermi");
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        assertSame(phaseContext.getTurnStart(), phaseContext.getGamePhase());

    }

    @Test
    public void mapNotFindTest() {
        builderToTest.modelBuild(gameStatus, "AAAA");
        GamePhaseContext phaseContext = gameStatus.getPhaseChanger();
        assertSame(phaseContext.getEndGame(), phaseContext.getGamePhase());

    }
}
