package it.polimi.controller;

import static org.junit.Assert.*;
import it.polimi.model.GameStatus;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;
import it.polimi.model.player.Player;

import org.junit.Before;
import org.junit.Test;

public class ControllerGameStartTest {

    private GameStatus status;
    private ActualController actualController;
    private Player firstPlayer;
    private Player secondPlayer;

    @Before
    public void initStartControllerEnviroment() {
        status = new GameStatus();
        actualController = new ActualController(status);
        firstPlayer = new Alien("test");
        secondPlayer = new Human("test");
        status.addPlayer(firstPlayer);
        status.addPlayer(secondPlayer);
    }

    @Test
    public void gameStartControllerTest() {

        actualController.reading("galilei", firstPlayer);
        assertTrue(actualController.controller instanceof ControllerGameRunning);

    }

    @Test
    public void gameStartContrCanIEnterCMDTest() {

        assertTrue(((ControllerGameStart) actualController.controller)
                .canIEnterCmd(firstPlayer));

    }

}
