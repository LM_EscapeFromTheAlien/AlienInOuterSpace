package it.polimi.controller.command;

import static org.junit.Assert.*;
import it.polimi.model.GameBuilder;
import it.polimi.model.GameStatus;
import it.polimi.model.gamephase.GamePhaseContext;
import it.polimi.model.player.Alien;
import it.polimi.model.player.Human;

import java.util.Timer;

import org.junit.Before;
import org.junit.Test;

public class CommandBrokerTest {

    private CommandBroker cmdBroker;
    private GamePhaseContext phaseContext;
    private GameStatus status;
    private Alien firstPlayer;
    private Human secondPlayer;
    private GameBuilder builder;

    @Before
    public void initStartControllerEnviroment() {

        status = new GameStatus();
        status.setTimer(new Timer());
        firstPlayer = new Alien("test");
        secondPlayer = new Human("test");
        status.addPlayer(firstPlayer);
        status.addPlayer(secondPlayer);
        builder = new GameBuilder();

        builder.modelBuild(status, "galilei");

        cmdBroker = new CommandBroker();
        cmdBroker.addCommand();
        phaseContext = new GamePhaseContext(status);
    }

    @Test
    public void gameBrokerECMDTest() {

        assertTrue(cmdBroker.sendThisCommand(phaseContext, "move l;09"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "attack"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "discard 1"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "draw"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "end"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "noise in L;09"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "show cards"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "skip"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "use item 2"));
        assertTrue(cmdBroker.sendThisCommand(phaseContext, "light in L;09"));

    }

    @Test
    public void cmdUknTest() {

        assertFalse(cmdBroker.sendThisCommand(phaseContext, "AAAA"));

    }

}
